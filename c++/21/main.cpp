#include <iostream>

/*
 * MEMORY LEAK: algo queda abandonado en el heap y ya no es accesible
 */

int main() {

    //creando un objeto en el heap
    int* p = new int;
    *p = 25;

    std::cout << *p << std::endl;
    std::cout << p << std::endl;

    //arreglos y punteros
    int scores[50];
    int* punt = scores;
    *punt = 99;
    std::cout << scores[0];


    return 0;
}