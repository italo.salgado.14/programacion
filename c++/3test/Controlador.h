//
// Created by lailapc on 29-07-19.
//
#ifndef CONTROLADOR_H
#define CONTROLADOR_H

#include "DetectorDeRequerimiento.h"
#include "SemaforoDeGiro.h"

class Controlador{
public:
    //--------CONSTR+DESTR-------------
    Controlador(SemaforoDeGiro* semaforodegiro, DetectorDeRequerimiento* sensor);

    //-----------METODOS--------------------
    void manageTraffic();
    void stopmanageTraffic();

    //----------ATRIBUTOS---------------
private:
    SemaforoDeGiro* semaforodegiro;
    DetectorDeRequerimiento* sensor;
    int time_counter;
    bool manage;
};

#endif // CONTROLADOR_H