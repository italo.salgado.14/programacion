//
// Created by lailapc on 29-07-19.
//

#include "DetectorDeRequerimiento.h"
#include <string> // for string class
using namespace std;


//--------CONST-DESTR--------------
DetectorDeRequerimiento::DetectorDeRequerimiento(){
    encendido = false;
}


//--------METODOS--------
bool DetectorDeRequerimiento::isOn(){
    return encendido;
}
void DetectorDeRequerimiento::setOn(){
    encendido = true;
}
void DetectorDeRequerimiento::setOff(){
    encendido = false;
}


string DetectorDeRequerimiento::toString()
{
    if (encendido)
        return "1";
    else
        return "0";
}
