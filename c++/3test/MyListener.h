//
// Created by lailapc on 29-07-19.
//

#ifndef MYLISTENER_H
#define MYLISTENER_H

#include "Listener.h"
#include "DetectorDeRequerimiento.h"
#include <string>
#include <fstream>

class MyListener:public Listener {
//-----------CONST+DESTR-----------------
public:
    MyListener(DetectorDeRequerimiento* bt, string filename);
    ~MyListener();

//----------METODOS------------
    void actionPerformed();
    void closeArchive(); //no me gusta esto, deberia cerrarse con el delete

//-----------ATRIBUTOS----------------
private:
    DetectorDeRequerimiento *boton;
    std::fstream file;
    char linea[8];
};

#endif
