//
// Created by lailapc on 29-07-19.
//

#include "MyListener.h"
#include <iostream>
#include <fstream>
using namespace std;

//----------------CONST+DEST-----------------------
MyListener::MyListener(DetectorDeRequerimiento* bt, string fn){
    boton=bt;

    //Cargar variable file y confirmar
    file.open(fn, ios::in);
    if (file.fail()){
        cout << "Fichero mal ubicado" << endl;
    }
}

MyListener::~MyListener(){
    file.close();
    cout << "Closed File" <<endl;
}

//--------------------METODOS-----------------
void MyListener::actionPerformed() {
    //no haremos nada si estamos al final del archivo
    if (!file.eof()){
        file.getline(linea, sizeof(linea));
        if ((int)linea[0] == 49) {
            boton->setOn();
        }
    }
}

void MyListener::closeArchive(){
    file.close();
    cout << "Closed File" <<endl;
}
