//
// Created by lailapc on 29-07-19.
//

#include "SimuladorEntradas.h"
#include "MyTimer.h"
//#include "MyListener.h"
#include <unistd.h>  // sleep
#include <iostream>   //cout

//-----------CONSTR-DESTR--------
SimuladorEntradas::SimuladorEntradas(DetectorDeRequerimiento* bt, string fn){
    listener = new MyListener(bt, fn);
    timer = new MyTimer(1000,*listener);
    timer->pause();

}

SimuladorEntradas::~SimuladorEntradas(){
    delete listener;
    delete timer;
}

//---------METODOS------------
void SimuladorEntradas::start(){
    timer->resume();
}

void SimuladorEntradas::stop() {
    timer->stop();
}