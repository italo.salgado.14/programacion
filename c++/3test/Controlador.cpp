//
// Created by lailapc on 29-07-19.
//

#include "Controlador.h"
#include <unistd.h>
#include "SemaforoDeGiro.h"
#include "DetectorDeRequerimiento.h"
#include <iostream>
using namespace std;
/*
 * NOTAS:
 * El While(true) es peligrosom en c++, y se desaconseja su uso.
 * Entonces, se a implementado una parada de urgencia para el
 * controlador, dada por la variable manage.
 *
 * Acceso por el paso a referencia:
 * semaforopeaton->toString(); == semaforopeaton.toString();
 */


//---------CONSTRU+DESTR-------------
Controlador::Controlador(SemaforoDeGiro* semaforodegiro, DetectorDeRequerimiento* sensor){
    this->semaforodegiro = semaforodegiro;
    this->sensor = sensor;
    time_counter=0;
    cout <<time_counter <<"   "<< semaforodegiro->toString()<< endl;
    manage=true;
}

//-----------METODOS-----------------
 void Controlador :: manageTraffic(){

    while(manage){
        sleep(1);
        time_counter+=1;
        cout << time_counter << endl;
        if(sensor->isOn()){
            semaforodegiro->turnGreenLightOn();
            cout <<time_counter <<"   "<< semaforodegiro->toString()<< endl;
            sleep(semaforodegiro->getGreenLightTime());
            time_counter+=semaforodegiro->getGreenLightTime();

            semaforodegiro->turnFlashingLightState();
            cout <<time_counter <<"   "<< semaforodegiro->toString()<< endl;
            sleep(semaforodegiro->getFlashingTime());
            time_counter+=semaforodegiro->getFlashingTime();

            semaforodegiro->turnGreenLightOff();
            cout <<time_counter <<"   "<< semaforodegiro->toString()<< endl;
            sensor->setOff();
        }

    }

}

void Controlador::stopmanageTraffic(){
    manage=false;
}