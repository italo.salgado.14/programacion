//
// Created by lailapc on 29-07-19.
//

/*
 * ETAPA 3 - TAREA 3
 * El programa se ejecutara de la siguiente forma:
 * ./TestStage3 <archivo-entrada-sensor>
 * donde el archivo de entrada al sensor esta en la misma carpeta donde el
 * programa se ejecuta.
 * CUIDADO: Clion crea una carpeta llamada "CMakeFiles", que usa como
 * directorio actual.
 * Probar con el archivo introsensor.txt si se desea
 *
 * *****************************************************
 * argc->entero: n de argumentos ingresados - Nombre del
 * progr es el arg 1, argv[0]
 * argv->array: array de puntero a caracteres
 *
 *
 */
#include <iostream>
#include "MyTimer.h"
#include "MyListener.h"
#include "SemaforoDeGiro.h"
#include <unistd.h>
#include <fstream>
#include "Controlador.h"
#include "SimuladorEntradas.h"

using namespace std;

int main( int argc, char *argv[] ){

    string name_file_to_open = "entradasensor.txt";

    int argc, char *argv[]
    if (argc != 2){
        cout << "Numero incorrecto de entradas, por favor, verifique su entrada" << endl;
        exit(0);
    }
    else {
        name_file_to_open = argv[1];
    }


    int duracion_verde = 6;
    int duracion_parpadeo = 2;

    DetectorDeRequerimiento sensor_ind;
    //SemaforoDeGiro semg(duracion_verde, duracion_parpadeo);
    //Controlador ctrl(&semg, &sensor_ind);
    //SimuladorEntradas entradas_autos(&sensor_ind, name_file_to_open);

    //MyListener listener(&sensor_ind, name_file_to_open);
    //MyTimer timera (1000,listener);

    MyListener* listenerP = new MyListener(&sensor_ind, name_file_to_open);
    MyTimer* timer = new MyTimer(1000,*listenerP);


    for(int i=0; i<10 ; i++){
        sleep(1);
    }
    //entradas_autos.start();
    //ctrl.manageTraffic();

    exit(0);
}