//
// Created by lailapc on 29-07-19.
//
#ifndef SIMULADORENTRADAS_H
#define SIMULADORENTRADAS_H


#include "DetectorDeRequerimiento.h"
#include "MyTimer.h"
#include "MyListener.h"
#include <string>
using namespace std;

/*
 * CUIDADO: Los headers "MYLISTENER_H" son muy utiles: evitan
 * error al redefinir las clases.
 */

class SimuladorEntradas {
public:
    SimuladorEntradas(DetectorDeRequerimiento* bt, string fn);
    ~SimuladorEntradas();

    void start();
    void stop();
private:
    MyTimer* timer;
    MyListener* listener;
};

#endif // SIMULADORENTRADAS_H
