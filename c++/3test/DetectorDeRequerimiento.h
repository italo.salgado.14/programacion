//
// Created by lailapc on 29-07-19.
//

/*
 * Notese: aqui deberiamos usar sobrecarga de operadores en algo
 */

#ifndef DETECTORDEREQUERIMIENTO_H
#define DETECTORDEREQUERIMIENTO_H

#include <string> // for string class
using namespace std;

class DetectorDeRequerimiento
{
public:
    DetectorDeRequerimiento();
    bool isOn();
    void setOn();
    void setOff();
    string toString();
private:
    bool encendido;
};

#endif // DETECTORDEREQUERIMIENTO_H
