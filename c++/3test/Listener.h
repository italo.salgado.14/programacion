//
// Created by lailapc on 29-07-19.
//

#if !defined LISTENER_H
#define LISTENER_H
class Listener {
public:
    virtual void actionPerformed()=0;
};
#endif