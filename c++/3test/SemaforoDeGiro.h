//
// Created by lailapc on 29-07-19.
//
/*
 * Interetamos "implementar de forma similar" a implementar una
 * nueva clase para SemaforoDeGiro
 */

#ifndef SEMAFORODEGIRO_H
#define SEMAFORODEGIRO_H

#include <string>

class SemaforoDeGiro {
public:
    //-------CONSTR+DESTR---------------
    SemaforoDeGiro(int t_green, int t_flashing);

    //----------METODOS---------------
    void turnGreenLightOn();
    void turnGreenLightOff();
    void turnFlashingLightState();
    int getGreenLightTime();
    int getFlashingTime();
    std::string toString();

private:
    //------------ATRIBUTOS---------------
    int t_green;
    int t_flashing;
    bool green;
    bool flashing;

};


#endif //INC_3_SEMAFORODEGIRO_H
