#ifndef SEMAFOROP_H
#define SEMAFOROP_H

#include<string>
using namespace std;



/*
 * Notese: aqui deberiamos usar sobrecarga de operadores en algo
 */


class SemaforoP
{
public:
    SemaforoP(int gt, int bt);
    void turnGreenLightOn();
    void turnFlashingState();
    void turnRedLightOn();
    int getGreenTime();
    int getFlashingTime();
    string toString();
private:
    int tiempo_verde;
    int tiempo_yellow;
    bool red;
    bool yellow;
    bool green;
};

#endif // SEMAFOROP_H
