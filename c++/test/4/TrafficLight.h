/*
 * TrafficLight.h
 *
 *  Created on: 17-07-2019
 *      Author: luciano
 *
 *  Esta clase implementa la lógica básica de todos los semaforos de 3
 *  estados, representados por el verde, el amarillo y el rojo. En si estos
 *  colores son arbitrarios, la lógica detrás de la implementación es independiente
 *  de los colores
 */
#include<string> // for string class
using namespace std;

#ifndef TRAFFICLIGHT_H_
#define TRAFFICLIGHT_H_

class TrafficLight {
public:
    //CONST+DESTR
    TrafficLight(int g,int y);
    ~TrafficLight();

    //METODOS
    void turnRedLightOn();
	void turnYellowLightOn();
	void turnGreenLightOn();
	int getGreenTime();
    int getYellowTime();
    int ActualStateLight();


    //ATRIBUTOS
private:
    int tiempo_verde;
    int tiempo_yellow;
    bool red;
	bool yellow;
	bool green;
};

#endif
