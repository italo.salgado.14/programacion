#ifndef SIMULADORENTRADAS_H
#define SIMULADORENTRADAS_H


#include "detectorderequerimiento.h"
#include "MyTimer.h"
#include "MyListener.h"
#include <string>
using namespace std;

/*
 * CUIDADO: Los headers "MYLISTENER_H" son muy utiles: evitan
 * error al redefinir las clases.
 */

class SimuladorEntradas {
public:
    SimuladorEntradas(DetectorDeRequerimiento* boton1, DetectorDeRequerimiento* boton2, string fn);
    void start();
    void stop();
private:
    MyTimer* timer;
    MyListener* listener;
};

#endif // SIMULADORENTRADAS_H
