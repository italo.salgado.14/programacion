#include "simuladorentradas.h"
#include "MyTimer.h"
//#include "MyListener.h"
#include <unistd.h>  // sleep
#include <iostream>   //cout

//-----------CONSTR-DESTR--------
SimuladorEntradas::SimuladorEntradas(DetectorDeRequerimiento* boton1,DetectorDeRequerimiento* boton2,
        string fn){
    listener = new MyListener(boton1, boton2, fn);
    timer = new MyTimer(1000,*listener);
    timer->pause();
}

//---------METODOS------------
void SimuladorEntradas::start(){
    timer->resume();
}

void SimuladorEntradas::stop() {
    timer->stop();
}