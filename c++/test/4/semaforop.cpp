#include "semaforop.h"

SemaforoP::SemaforoP(int gt, int bt){
    tiempo_verde = gt;
    tiempo_yellow= bt;
    red = true;
    yellow = false;
    green = false;
}
void SemaforoP::turnRedLightOn(){
    red = true;
    yellow = false;
    green = false;
}
void SemaforoP::turnFlashingState(){
    red = false;
    yellow = true;
    green = false;
}
void SemaforoP::turnGreenLightOn(){
    red = false;
    yellow = false;
    green = true;
}
int SemaforoP::getGreenTime(){
    return tiempo_verde;
}
int SemaforoP::getFlashingTime(){
    return tiempo_yellow;
}
string SemaforoP::toString(){
    if (red)
        return "R" ; //Rojo
    if (yellow)
        return "P"; //Parpadeo
    else
        return "V"; //Verde
}
