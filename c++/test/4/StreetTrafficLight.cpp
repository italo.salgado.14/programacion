//
// Created by lailapc on 23-07-19.
//

#include "StreetTrafficLight.h"

//--------------CONST+DEST------------
StreetTrafficLight::StreetTrafficLight(int g, int y) : TrafficLight(g, y){
    //aqui tenemos la misma estructura de la clase padre
}

//----------------METODOS----------
string StreetTrafficLight::toString(){
    if (ActualStateLight() == 0)
        return "R" ;
    if (ActualStateLight() == 1)
        return "A";
    if (ActualStateLight() == 2)
        return "V";
    else
        return "E"; //error
}