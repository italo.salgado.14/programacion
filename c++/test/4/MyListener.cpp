#include "MyListener.h"
#include <iostream>
#include <fstream>
#include "string.h"

using namespace std;

//----------------CONST+DEST-----------------------
MyListener::MyListener(DetectorDeRequerimiento* boton1, DetectorDeRequerimiento* boton2,string fn){
    this->boton1=boton1;
    this->boton2=boton2;


    //Cargar variable file y confirmar
    file.open(fn, ios::in);
    if (file.fail()){
        cout << "Fichero mal ubicado" << endl;
    }
}

MyListener::~MyListener(){
    file.close();
    cout << "Closed File" <<endl;
}

//--------------------METODOS-----------------
void MyListener::actionPerformed() {



    //no haremos nada si estamos al final del archivo
    //strcmp + strtok son peligrsos juntos, cuidado al pasarle un puntero
    //a una o la otra!
    if (!file.eof()){
        file.getline(linea, sizeof(linea));


        if ((strcmp(linea, "/n")!=0 and file.eof())==0) {
            cout << linea << endl;
            splited = strtok(linea, " ");
            if (strncmp(splited, "1", 1) == 0) {
                boton1->setOn();
                cout << splited << endl;

            }
            splited = strtok(NULL, " ");

            if (splited != nullptr){
            if (strncmp(splited, "1", 1) == 0) {
                boton2->setOn();
                cout << splited << endl;
            }}
        }
    }

}

void MyListener::closeArchive(){
    file.close();
    cout << "Closed File" <<endl;
}
