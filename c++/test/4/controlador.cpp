//
// Created by lailapc on 29-07-19.
//

#include "controlador.h"
#include <unistd.h>
#include "SemaforoDeGiro.h"
#include "detectorderequerimiento.h"
#include <iostream>
using namespace std;
/*
 * Este controlador está basado en la maquina de estado dada en
 * la documentación.
 *
 * NOTAS:
 * El While(true) es peligrosom en c++, y se desaconseja su uso.
 * Entonces, se a implementado una parada de urgencia para el
 * controlador, dada por la variable manage.
 *
 * Acceso por el paso a referencia:
 * semaforopeaton->toString(); == semaforopeaton.toString();
 */


//---------CONSTRU+DESTR-------------
Controlador::Controlador(StreetTrafficLight* semV1, SemaforoDeGiro* semG1, StreetTrafficLight* semV2,
        DetectorDeRequerimiento* boton1, SemaforoP* semP2, StreetTrafficLight* semV3,
        DetectorDeRequerimiento* boton2, SemaforoDeGiro* semG2, SemaforoP* semP1){
    this->semP1 = semP1;
    this->semG1 = semG1;
    this->semV1 = semV1;
    this->semV2 = semV2;
    this->semP2 = semP2;
    this->boton1 = boton1;
    this->semV3 = semV3;
    this->semG2 = semG2;
    this->boton2 = boton2;
    time_counter=0;

}

//-----------METODOS-----------------
void Controlador :: manageTraffic(){

    while(manage){
        time_counter+=1;

        /*SEMV1*/
        //---etapa en verde
        semV1->turnGreenLightOn();
        this->toString();
        sleep(semV1->getGreenTime());
        time_counter+=semV1->getGreenTime();
        //---etapa en amarillo
        semV1->turnYellowLightOn();
        this->toString();
        sleep(semV1->getYellowTime());
        time_counter+=semV1->getYellowTime();
        //---vuelta a rojo
        semV1->turnRedLightOn();

        /*SEMG1*/
        //---etapa en verde
        semG1->turnGreenLightOn();
        this->toString();
        sleep(semG1->getGreenLightTime());
        time_counter+=semG1->getGreenLightTime();
        //---etapa de parpadeo
        semG1->turnFlashingLightState();
        this->toString();
        sleep(semG1->getFlashingTime());
        time_counter+=semG1->getFlashingTime();
        //---vuelta a apagado
        semG1->turnGreenLightOff();

        /*SEMV2*/
        //---etapa en verde
        semV2->turnGreenLightOn();
        this->toString();
        sleep(semV2->getGreenTime());
        time_counter+=semV2->getGreenTime();
        //---etapa en amarillo
        semV2->turnYellowLightOn();
        this->toString();
        sleep(semV2->getYellowTime());
        time_counter+=semV2->getYellowTime();
        //---vuelta a rojo
        semV2->turnRedLightOn();

        /*semP2*/
        if (boton1->isOn()){
            //---verde
            semP2->turnGreenLightOn();
            this->toString();
            sleep(semP2->getGreenTime());
            time_counter+=semP2->getGreenTime();
            //---parpadeo
            semP2->turnFlashingState();
            this->toString();
            sleep(semP2->getFlashingTime());
            time_counter+=semP2->getFlashingTime();
            //---apagado
            semP2->turnRedLightOn();
        }

        /*SEMV3*/
        //---etapa en verde
        semV3->turnGreenLightOn();
        this->toString();
        sleep(semV3->getGreenTime());
        time_counter+=semV3->getGreenTime();
        //---etapa en amarillo
        semV3->turnYellowLightOn();
        this->toString();
        sleep(semV3->getYellowTime());
        time_counter+=semV3->getYellowTime();
        //---vuelta a rojo
        semV3->turnRedLightOn();

        /*SEMG2 */
        if(boton2->isOn()) {
            //---etapa en verde
            semG2->turnGreenLightOn();
            this->toString();
            sleep(semG2->getGreenLightTime());
            time_counter += semG2->getGreenLightTime();
            //---etapa de parpadeo
            semG2->turnFlashingLightState();
            this->toString();
            sleep(semG2->getFlashingTime());
            time_counter += semG2->getFlashingTime();
            //---vuelta a apagado
            semG2->turnGreenLightOff();
        }

        /*SEMP1*/
        //---etapa en verde
        semP1->turnGreenLightOn();
        this->toString();
        sleep(semP1->getGreenTime());
        time_counter+=semP1->getGreenTime();
        //---etapa flashing
        semP1->turnFlashingState();
        this->toString();
        sleep(semP1->getFlashingTime());
        time_counter+=semP1->getFlashingTime();
        //---vuelta a rojo
        semP1->turnRedLightOn();
        this->toString();

    }

}

void Controlador::toString(){
    cout <<semV1->toString()<<"  "<<semG1->toString()<<"  "
         <<semV2->toString()<< "  "<< semP2->toString() << "  " << semV3->toString() << "  "
         << semG2->toString() << "  " << semP1->toString() << "  " <<time_counter <<endl;
}

void Controlador::stopmanageTraffic(){
    manage = false;
}