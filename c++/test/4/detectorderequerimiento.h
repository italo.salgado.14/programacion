#ifndef DETECTORDEREQUERIMIENTO_H
#define DETECTORDEREQUERIMIENTO_H

#include <string> // for string class
using namespace std;

/*
 * Notese: aqui deberiamos usar sobrecarga de operadores en algo
 */

class DetectorDeRequerimiento
{
public:
    DetectorDeRequerimiento();
    bool isOn();
    void setOn();
    void setOff();
    string toString();
private:
    bool encendido;
};

#endif // DETECTORDEREQUERIMIENTO_H
