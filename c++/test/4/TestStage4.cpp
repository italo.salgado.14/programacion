/*
 * ETAPA 3 - TAREA 3
 * El programa se ejecutara de la siguiente forma:
 * ./TestStage3 <archivo-entrada-sensor>
 * donde el archivo de entrada al sensor esta en la misma carpeta donde el
 * programa se ejecuta.
 * CUIDADO: Clion crea una carpeta llamada "CMakeFiles", que usa como
 * directorio actual.
 * Probar con el archivo introsensor.txt si se desea
 *
 * *****************************************************
 * argc->entero: n de argumentos ingresados - Nombre del
 * progr es el arg 1, argv[0]
 * argv->array: array de puntero a caracteres
 *
 *
 */

#include <iostream>
#include "MyTimer.h"
#include "MyListener.h"
#include "semaforop.h"
#include <unistd.h>
#include <fstream>
#include "controlador.h"
#include "simuladorentradas.h"
#include "StreetTrafficLight.h"
#include "detectorderequerimiento.h"

using namespace std;


int main( int argc, char *argv[] ){

    string filetoread;

    if (argc != 2){
        cout << "Numero incorrecto de entradas, por favor, verifique su entrada" << endl;
        exit(0);
    }
    else {
        filetoread = argv[1];
    }


    //Instancia de cada componente de la calle
    SemaforoP semP1(4,2);
    SemaforoDeGiro semG1(4,1);
    StreetTrafficLight semV1(5,2);
    StreetTrafficLight semV2(5,2);
    SemaforoP semP2(4,2);
    DetectorDeRequerimiento boton1;
    StreetTrafficLight semV3(5,2);
    DetectorDeRequerimiento boton2; //sensor_ind
    SemaforoDeGiro semG2(4,1);

    //Controlador + sim entradas
    Controlador contr(&semV1, &semG1, &semV2, &boton1, &semP2, &semV3, &boton2, &semG2, &semP1);
    SimuladorEntradas entradas_sensores(&boton1, &boton2, filetoread);


    entradas_sensores.start();

    //Cuidado!: el codigo esta atrapado en esta orden, hasta recibir
    //un fin de ejecución.
    contr.manageTraffic();


    exit(0);
}
