//
// Created by lailapc on 29-07-19.
//
#ifndef CONTROLADOR_H
#define CONTROLADOR_H

#include "detectorderequerimiento.h"
#include "SemaforoDeGiro.h"
#include "semaforop.h"
#include "StreetTrafficLight.h"

class Controlador{
public:
    //--------CONSTR+DESTR-------------
    Controlador(StreetTrafficLight* semV1, SemaforoDeGiro* semG1, StreetTrafficLight* semV2,
                DetectorDeRequerimiento* boton1, SemaforoP* semP2, StreetTrafficLight* semV3,
             DetectorDeRequerimiento* boton2, SemaforoDeGiro* semG2, SemaforoP* semP1);

    //-----------METODOS--------------------
    void manageTraffic();
    void stopmanageTraffic();
    void toString();

    //----------ATRIBUTOS---------------
private:
    SemaforoP* semP1;
    SemaforoDeGiro* semG1;
    StreetTrafficLight* semV1;
    StreetTrafficLight* semV2;
    SemaforoP* semP2;
    DetectorDeRequerimiento* boton1;
    StreetTrafficLight* semV3;
    SemaforoDeGiro* semG2;
    DetectorDeRequerimiento* boton2;
    int time_counter, manage;
};

#endif // CONTROLADOR_HNTROLADOR_H