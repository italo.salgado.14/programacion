//
// Created by lailapc on 30-07-19.
//

#include "SemaforoDeGiro.h"

SemaforoDeGiro::SemaforoDeGiro(int t_green, int t_flashing) {
    this->t_green = t_green;
    this->t_flashing = t_flashing;
    green = false;
    flashing = false;
}

void SemaforoDeGiro::turnGreenLightOn() {
    green = true;
    flashing = false;
}

void SemaforoDeGiro::turnFlashingLightState(){
    green= false;
    flashing = true;
}

void SemaforoDeGiro::turnGreenLightOff() {
    green = false;
    flashing = false;
}

int SemaforoDeGiro::getGreenLightTime() {
    return t_green;
}

int SemaforoDeGiro::getFlashingTime() {
    return t_flashing;
}

std::string SemaforoDeGiro::toString() {
    if (green and !flashing){
        return "V"; //Verde
    }
    else if (!green and flashing){
        return "P"; //parpadeo
    }
    else{
        return "A"; //Apagado
    }
}