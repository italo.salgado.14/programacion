#ifndef MYLISTENER_H
#define MYLISTENER_H

#include "Listener.h"
#include "detectorderequerimiento.h"
#include <string>
#include <fstream>

class MyListener:public Listener {
//-----------CONST+DESTR-----------------
public:
   MyListener(DetectorDeRequerimiento* boton1,DetectorDeRequerimiento* boton2, string fn);
   ~MyListener();

//----------METODOS------------
   void actionPerformed();
   void closeArchive(); //no me gusta esto, deberia cerrarse con el delete

//-----------ATRIBUTOS----------------
private:
   DetectorDeRequerimiento *boton1;
    DetectorDeRequerimiento *boton2;
   std::fstream file;
   char linea[8];
    char* splited;
};

#endif