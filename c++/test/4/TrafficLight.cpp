/*
 * TrafficLight.c
 *
 *  Created on: 17-07-2019
 *      Author: luciano
 *
 *  Erratas implementativas: como devolver un char en una función?
 */
#include "TrafficLight.h"
#include <iostream>

#include<string> // for string class
using namespace std;

//----------CONST+DEST------------
// el semaforo partira en verde
TrafficLight::TrafficLight(int g,int y ){
	tiempo_verde = g;
    tiempo_yellow= y;
    red = true;
	yellow = false;
	green = false;
}

TrafficLight::~TrafficLight() {
    cout << "Objeto destruido" << endl;
}

//-----------METODOS--------------
//mutadores
void TrafficLight::turnRedLightOn(){
    red = true;
	yellow = false;
	green = false;
}
void TrafficLight::turnYellowLightOn(){
    red = false;
	yellow = true;
	green = false;
}
void TrafficLight::turnGreenLightOn(){
    red = false;
	yellow = false;
    green = true;
}

//accesores
int TrafficLight::getGreenTime(){
    return tiempo_verde;
}
int TrafficLight::getYellowTime(){
    return tiempo_yellow;
}
int TrafficLight::ActualStateLight() {
    //Al final, las letras son numeros. ocupamos numeros.
    //devolveremos 3 si un semaforo se ha desincronizado.
    //red=0, yellow=1, green=2.
    if (red){
        return 0;
    }
    if (yellow){
        return 1;
    }
    if (green){
        return 2;
    }
    else {
        return 3;
    }
}


