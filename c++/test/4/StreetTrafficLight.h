//
// Created by lailapc on 23-07-19.
//
/*
 * Implementación de interacción. Esta representa una representación
 * tangible de un semaforo, asociado con colores. Hereda de la clase
 * TrafficLight.
 */

#ifndef STREETTRAFFICLIGHT_H
#define STREETTRAFFICLIGHT_H


#include "TrafficLight.h"

class StreetTrafficLight : public TrafficLight {
public:
    //CONST
    StreetTrafficLight(int g, int y);

    //METODOS
    string toString();
};


#endif
