#include "MyTimer.h"
#include "MyListener.h"
#include <unistd.h>  // sleep
#include <iostream>   //cout

/*
 * ERROR DEL THREAD
 * Cambios para todos los compiladores:
 * 1-Encender la flag de thread en el CMake:
 * SET(CMAKE_CXX_FLAGS -pthread)
 * 2-Agregar la libreria <pthread.h> en Mytimer.cpp, no todos los
 * compiladores tienen esto por std.
 *
 * https://stackoverflow.com/questions/45601811/undefined-reference-to-pthread-clion
 *
 * NOTAS DEL TIMER:
 * en los programas utilizar solo uno
 *
 */

using namespace std;

int main () {
   //MyListener listener;
   //MyTimer timer (1000, listener);   //instancia de MyTimer.

   
    MyListener* listenerP = new MyListener();
    MyTimer* timerP = new  MyTimer(100, *listenerP);


    for(int i=0; i<10 ; i++){
        sleep(1);
    }

    timerP->stop();
   //timer.stop();
   exit(0);
}
