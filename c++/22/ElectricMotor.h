//
// Created by lailapc on 23-07-19.
//

// ElectricMotor.h: interface for the CElectricMotor class.
//


#if !defined(AFX_ELECTRICMOTOR_H)
#define AFX_ELECTRICMOTOR_H


#include "motor.h"

class CElectricMotor : public CMotor {
public:
    CElectricMotor();
    CElectricMotor(const string & id, double volts);

    void Display() const;
    void Input();
    void set_Voltage(double volts);
    double get_Voltage() const;

private:
    double m_nVoltage;
};

#endif

