//
// Created by lailapc on 23-07-19.
//

#ifndef MOTOR_H
#define MOTOR_H


#include <string>
#include <iostream>
using namespace std;

class CMotor {
public:
    //Constructores
    CMotor() { }
    CMotor( const string & id );

    //Metodos
    string get_ID() const;
    void set_ID(const string & s);
    void Display() const; // Display a motor on the console.
    void Input(); // Input a motor from the user.

private:
    string m_sID;       // the motor identification number
};


#endif
