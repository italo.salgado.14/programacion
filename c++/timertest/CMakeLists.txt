cmake_minimum_required(VERSION 3.14)
project(timertest)

set(CMAKE_CXX_STANDARD 14)

add_executable(timertest main.cpp)