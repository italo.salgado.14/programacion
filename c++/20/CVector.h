//
// Created by lailapc on 15-07-19.
//

/*
 * http://profesores.elo.utfsm.cl/%7Eagv/elo329/miscellaneous/preprocessor.pdf
 * Todo esto es para preprocesar el asunto una sola vez
 *
 * Se crea un archivo de encabezado “clase.h”, en él podemos la definición
 * de la clase, es decir los atributos y prototipos de métodos.
 *
 * El símbolo :: permite al compilador saber que estamos definiendo la
 * función Point de la clase Point. Este también es conocido como
 * operador de resolución de alcance.
 */


//#ifndef INC_20_CVECTOR_H
//#define INC_20_CVECTOR_H

#ifndef CVECTOR_H
#define CVECTOR_H


class CVector {
public:
    int x,y;
    CVector () {};
    CVector (int,int);
    CVector operator + (const CVector &) const;
};


#endif
//#endif //INC_20_CVECTOR_H
