//
// Created by lailapc on 15-07-19.
//

/*
 * En otro archivo “clase.cpp” ponemos la implementación de cada
 * método. En éste debemos incluir el archivo de encabezado “clase.h”
 *
 * El símbolo :: permite al compilador saber que estamos definiendo la
 * función Point de la clase Point. Este también es conocido como
 * operador de resolución de alcance.
 */

#include "CVector.h"

CVector::CVector (int a, int b) {
    x = a;
    y = b;
}

CVector CVector::operator+ (const CVector &param) const {
    CVector temp;
    temp.x = x + param.x;
    temp.y = y + param.y;
    return temp;
}

