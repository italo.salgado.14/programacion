/*
 *
 * DEFINICIONES
 * -Un “accesor” es un método que retorna un valor desde su objeto,
 * pero no cambia el objeto (sus atributos). Permite acceder a los
 * atributos del objeto.
 * -Un mutador es un método que modifica su objeto
 * -Un constructor es un método con el mismo nombre de la clase que
 * se ejecuta tan pronto como una instancia de la clase es creada.
 * -Un destructor es un método el mismo nombre de la clase y una
 * virgulilla "~" antepuesta. Ej.: ~Automobil()
 *
 */

#include <iostream>
#include "CVector.h"
#include "CRectangle.h"
using namespace std;

int main () {
    CRectangle rect (3,4);
    CRectangle rectb;
    cout << "rect area: " << rect.area() << endl;
    cout << "rectb area: " << rectb.area() << endl;

    cout << endl;

    CVector a (3,5);
    CVector b (1,2);
    CVector c;
    c = a + b;
    cout <<"Mi vector C es: "<< c.x << "," << c.y << endl;
    return 0;
}
