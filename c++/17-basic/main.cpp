/*
 * NOCIONES BASICAS SOBRE C++
 * Italo Salgado
 * V1.2
 *
 * Sobre lectura y escritura de archivos: cuidado donde tu ide personal hecha los compilaodos. Esta wea
 * de CLion tira las weas en la carpeta "CMakefiles", pero usa como directorio
 * raiz donde este el cpp (en este caso el main). Puede comprobarse con la
 * función de creado de archivos
 *
 *CUIDADO: DECLARE MAS ARRIBA LAS FUNCIONES SI QUEIRE QUE LAS RECONOZCA TODO EL CÓDIGO!
 */

//archivos de encabezado <..> lugares standares definidos en el compilador
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <float.h>

#include <iostream>
#include <fstream>

#include <cstring>
#include <sstream>


using namespace std;

//DECLARATIONFUNCTIONS
void func_aux(fstream* archivo);

//END-DECLARATIONFUNCTIONS


int test_of_size(){
    std::cout << "Tamaño de los tipos de Datos en C y C++" << std::endl;
    std::cout << "char size: " << sizeof(char) << std::endl;
    std::cout << "uint8_t size: " << sizeof(uint8_t) << std::endl; //entero sin signo de 8 bits, de 0 a 255
    std::cout << "bool size: " << sizeof(bool) << std::endl;
    std::cout << "uint16_t size: " <<sizeof(uint16_t) << std::endl; //entero sin signo de 16bits
    std::cout << "int size: "<<sizeof(int) << std::endl;
    std::cout << "float size: "<<sizeof(float) << std::endl;

    return 0;
}

/*
 * Entrada y salida de archivos: ejemplos, lectura y otras maldades
 */

void func_aux(fstream& archivo) { //usar & o no usarle no le afecto, por que?

    std::fstream* myPtoFile = &archivo;
    char lectura[128];
    while(!archivo.eof()){
        myPtoFile->getline(lectura, sizeof(lectura));
        cout << lectura << endl;
    }
    archivo.close();
}
int test_input(){

    char linea[128];
    std::fstream fin;

    fin.open("test.txt"); //test contiene 3 ints
    while(!fin.eof()){
        fin.getline(linea, sizeof(linea));
        cout << linea << endl;
    }
    fin.close();


    std::fstream fichero;

    fichero.open("input_test.txt", ios::in);
    if(fichero.fail()){
        cout << "el fichero no esta ubicado donde corresponde" << endl;
    }
    else {
        func_aux(fichero);
    }
    return 0;
}






/*
 * Un stream es una abstracción para referirse a cualquier flujo de
 * datos entre una fuente y un destinatario
 */
int test_stream(){
    //creacion de fichero
    char cadena[128];
    ofstream fs("nombre.txt");

    fs << "alejandro magno era un dios" << endl;
    fs.close();


    //lectura del fichero
    ifstream fe("nombre.txt");
    fe.getline(cadena, 128);


    return 0;
}





/*
 * char *strcpy(char *s1, const char *s2)
 * Copia la cadena apuintada por s2 a la apuntada ṕor s1;
 *
 * casteo:convertir tipos.
 */
int test_stream2(){

    struct human_Imperator {
        char nombre[32];
        int edad;
        float altura;
    };

    human_Imperator alejandro{};
    human_Imperator alejandro_copy{};

    ofstream fsalida("grandes_luchadores.dat", ios::out | ios::binary);

    strcpy(alejandro.nombre, "Alejandro Magno");
    alejandro.edad = 26;
    alejandro.altura = 1.68;

    fsalida.write(reinterpret_cast<char *>(&alejandro), sizeof(human_Imperator));
    fsalida.close();

    ifstream fentrada("prueba.dat", ios::in | ios::binary);

    fentrada.read(reinterpret_cast<char *>(&alejandro_copy), sizeof(human_Imperator));
    cout << alejandro_copy.nombre <<endl;
    cout << alejandro_copy.edad<<endl;
    cout << alejandro_copy.altura<<endl;
    fentrada.close();

    return 0;
}

int test_pointer(){
    int miVar = 5;
    // VARIABLE---------------------miVar----------------
    // VALOR--------|           |     5       |         |
    // DIRECCION-------1775---------1776--------1777-----

    int* pVar;
    pVar = &miVar;
    // *: valor de variable a la que se apunta
    // &: direccion de memoria de la variable

    // VARIABLE-------pVar---
    // VALOR-------|  1776  |
    // DIRECCION-----3571----
    cout << "Valor miVar:"<< miVar <<"  Direccion mem:"<< &miVar <<endl;
    cout << "Direccion de la variable pVar:" << &pVar <<endl;
    cout << "Contenido de pVar:" << pVar << endl;
    cout << "Valor del contenido apuntado:" << *pVar <<endl;

    return 0;
}



//Ejemplo de manejo de punteros:
void swapenC(int* px, int* py){
    int tmp = *px;
    *px=*py;
    *py=tmp;
}

void swapenCplusplus(int& x, int& y){
    int tmp=x;
    x=y;
    y=tmp;
}






int main() {

    cout <<"Pruebas implementadas: test_size, test_input, test_stream, test_stream2."<< endl;
    cout <<"Ingrese lo que deseas saber: "<< endl;

    string peticion;
    cin >> peticion;

    if (peticion == "test_size"){
        test_of_size();
    }
    else if (peticion == "test_input"){
        test_input();
    }
    else if(peticion == "test_stream"){
        test_stream();
    }
    else if(peticion == "test_stream2"){
        test_stream2();
    }
    else if(peticion == "test_pointer"){
        test_pointer();
    }
    else {
        cout <<"No se ha ingresado nada valido de la lista."<<endl;
    }

    return 0;
}