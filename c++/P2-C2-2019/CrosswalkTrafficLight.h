//
// Created by lailapc on 09-09-19.
//

#ifndef P2_C2_2019_CROSSWALKTRAFFICLIGHT_H
#define P2_C2_2019_CROSSWALKTRAFFICLIGHT_H

#include "StreetTrafficLight.h"
class CrosswalkTrafficLight: public TrafficLight {
public:
    CrosswalkTrafficLight (ostream &, int &time, int ft, int tt);
    virtual void printState ();
};
#endif //P2_C2_2019_CROSSWALKTRAFFICLIGHT_H
