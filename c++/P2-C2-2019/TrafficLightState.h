#ifndef TRAFFIC_LIGHT_STATE
#define TRAFFIC_LIGHT_STATE
enum class TrafficLightState {STOP, TRANSITION, FOLLOW};
#endif
