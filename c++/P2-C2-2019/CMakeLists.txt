cmake_minimum_required(VERSION 3.14)
project(P2_C2_2019)

set(CMAKE_CXX_STANDARD 14)

add_executable(P2_C2_2019 P2.cpp StreetTrafficLight.h StreetTrafficLight.cpp TrafficLight.h TrafficLight.cpp TrafficLightState.h CrosswalkTrafficLight.cpp CrosswalkTrafficLight.h)