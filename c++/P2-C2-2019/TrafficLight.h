#ifndef TRAFFIC_LIGHT_H
#define TRAFFIC_LIGHT_H
#include "TrafficLightState.h"
#include <ostream>
using namespace std;
class TrafficLight {
public:
   TrafficLight (ostream &, int &, int ft, int tt);
   void turnStop();
   void turnTransition();
   void turnFollow();
   int getFollowTime();
   int getTransitionTime();
   TrafficLightState getState();
   virtual void printState()=0;

protected:
   ostream &os;
   int time;
private:
   int followTime;
   int transitionTime;
   TrafficLightState state;
};
#endif
