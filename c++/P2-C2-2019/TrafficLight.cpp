#include <unistd.h>
#include <iostream>
#include "TrafficLight.h"
#include "TrafficLightState.h"


TrafficLight::TrafficLight (ostream &_os, int &t, int ft, int tt): os(_os), time(t) {
      followTime = ft;
      transitionTime = tt;
      state = TrafficLightState::STOP;
      time = t; //asi todos conocen t (?)
      //cout << time << endl;
}
void TrafficLight::turnStop() {
      //cout << time - transitionTime - followTime <<endl;
      state = TrafficLightState::STOP;
      printState();
      sleep(time - transitionTime - followTime);

}
void TrafficLight::turnTransition() {
      state = TrafficLightState::TRANSITION;
      printState();
      sleep(transitionTime);
}
void TrafficLight::turnFollow() {
      state = TrafficLightState::FOLLOW;
      printState();
      sleep(followTime);
}
int TrafficLight::getFollowTime() {
      return followTime;
}
int TrafficLight::getTransitionTime() {
      return transitionTime;
}
TrafficLightState TrafficLight::getState() {
      return state;
}

