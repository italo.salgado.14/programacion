#include "StreetTrafficLight.h"
#include <iostream>   //cout
using namespace std;


int main (int argc, char * argv[]) {
   int nCiclos=atoi(argv[1]);
   int redTime=atoi(argv[2]);
   int time=8;  // tiempo en segundos  moddd


   StreetTrafficLight sCalle(cout, time, 4, 2);
   for (int n=0; n < nCiclos; n++) {
       sCalle.turnFollow();
       time+=sCalle.getFollowTime();
       sCalle.turnTransition();
       time+=sCalle.getTransitionTime();
       sCalle.turnStop();
       time+=redTime;
   }   
}
