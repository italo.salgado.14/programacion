#include "StreetTrafficLight.h"
StreetTrafficLight::StreetTrafficLight (ostream & _os, int &t, int ft, int tt):
   TrafficLight(_os, t, ft, tt) {
}  
void StreetTrafficLight::printState () {
   switch (getState()) {
      case TrafficLightState::STOP: os << time << "\tR" << endl;
                 break;
      case TrafficLightState::TRANSITION: os << time << "\tA" << endl;
                 break;
      case TrafficLightState::FOLLOW: os << time << "\tV" << endl;
                 break;
   }        
}

