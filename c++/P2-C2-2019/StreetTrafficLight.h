#ifndef STREET_TRAFFIC_LIGHT_H
#define STREET_TRAFFIC_LIGHT_H
#include "TrafficLight.h"
using namespace std;
class StreetTrafficLight: public TrafficLight {
public:
    StreetTrafficLight (ostream &, int &time, int ft, int tt);
    virtual void printState ();
};
#endif
