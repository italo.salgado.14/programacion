//
// Created by lailapc on 09-09-19.
//
#include "CrosswalkTrafficLight.h"
CrosswalkTrafficLight::CrosswalkTrafficLight (ostream & _os, int &t, int ft, int tt):
        TrafficLight(_os, t, ft, tt) {
}
void CrosswalkTrafficLight::printState () {
    switch (getState()) {
        case TrafficLightState::STOP: os << time << "\tR" << endl;
            break;
        case TrafficLightState::TRANSITION: os << time << "\tR/V" << endl;
            break;
        case TrafficLightState::FOLLOW: os << time << "\tV" << endl;
            break;
    }
}