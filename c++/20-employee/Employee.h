//
// Created by lailapc on 15-07-19.
//

#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#include <string>
using namespace std;

typedef struct {
    int year_date;
    int month_date;
    int day_date;
} datetime;

class Employee {
private:
    string name;
    double salary;
    datetime hireDay;

public:
    Employee(string n, double s, int year, int month, int day)
    string getName();
    double getSalary();

};


#endif //INC_20_EMPLOYEE_EMPLOYEE_H
