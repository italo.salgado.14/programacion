/*
 *Al ser C++ una ampliación del lenguaje C, es necesario agregar nuevas
 * palabras reservadas. Éstas palabras reservadas están en un “namespace”
 * (espacio de nombres). Para ser mas específicos, las palabras
 * reservadas cout y cin están el namespace std (standard).
 *
 */


#include <iostream>
#include <string.h>

//aui se agregan los nombres reservados de c++
using namespace std;

int main() {
    std::cout << "Ejemplo clase 19" << std::endl;

    //EJ1
    const int *a;  // no puedo cambiar el contenido apuntado a
    int* const b;  // no puedo cambiar b, puntero fijo



    //EJ2
    int N = 25;
    int & rN = N;  // referencia a N

    /* similar a puntero en semántica,pero con sintaxis normal*/
    rN = 36;
    std::cout << N << std::endl;         // "36" displayed


    //EJ3: CASTEO
    int n = (int)3.5;
    int w = int(3.5);
    bool isOK = bool(15);
    char ch = char(86); //simbolo en ascii
    string s = string("123");

    //int x = int("123"); //error
    //string s = string(3.5); //error
    double pi = 3.1416;
    char *p = (char*)&pi; //asi se accede a pi bite por bite

    return 0;
}