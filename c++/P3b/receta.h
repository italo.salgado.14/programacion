//
// Created by lailapc on 09-09-19.
//
#ifndef RECETA_H
#define RECETA_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;

class receta {
public:
    receta(string nombre);
    ~receta();
    void leer();
    static void crear();

private:
    fstream archivo;
    string nombre;
    vector<ingrediente> ingredientes;
    string instrucciones;
};

receta::receta(string nombre) {
    string aux;
    archivo.open (nombre+".receta");
    this->nombre = nombre;
    getline(archivo, nombre);
    getline(archivo, aux); //
    if (aux == "ingredientes:") {
        getline(archivo, aux); //
        while(aux != "instrucciones:") {
            ingredientes.push_back(aux);
            getline(archivo, aux); //
        }
        getline(archivo, instrucciones); //
    }
}

receta::~receta() {
    archivo.close();
}

void receta::leer() {
    cout << "Receta " + nombre << endl << endl;
    cout << "Ingredientes: " << endl;
    for(int i=0; i<ingredientes.size(); i++) {
        cout << ingredientes[i] << endl;
    }
    cout << endl;
    cout << "Instrucciones:" << endl << instrucciones << endl;
}

void receta::crear() {
    ofstream archivo;
    string nombre;

    cout << "Indique el nombre de la receta:" << endl;
    getline(cin, nombre);
    archivo.open(nombre+".receta");
    archivo << nombre << endl;

    cout << endl << "Ingrese ingredientes: (vacio para terminar)" << endl;
    string ingrediente;
    archivo << "ingredientes:" << endl;
    while(true) {
        getline(cin, ingrediente);
        if (!ingrediente.empty())
            archivo << ingrediente << endl;
        else
            break;
    }
    cout << "Ingrese instrucciones: (una Ãºnica entrada de texto)" << endl;
    string instrucciones;
    getline(cin, instrucciones);
    archivo << "instrucciones:" << endl << instrucciones << endl;
    archivo.close();
}

class ingrediente {

};
#endif // RECETA_H