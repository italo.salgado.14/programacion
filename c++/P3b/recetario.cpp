//
// Created by lailapc on 09-09-19.
//

#include "receta.h"
#include <string>

using namespace std;

int main() {

    string opcion = "";
    receta *r;
    string nombre;

    while(true) {
        cout << endl << "Indique quÃ© desea realizar:" << endl;
        cout << "\t0: aÃ±adir una receta." << endl;
        cout << "\t1: buscar una receta." << endl;
        cout << "\t*: otra opciÃ³n termina el programa." << endl << endl;

        getline(cin,opcion);

        switch(atoi(opcion.c_str())) {
            case 0:
                receta::crear();
                break;
            case 1:
                cout << "Indique el nombre de la receta:" << endl;
                getline(cin,nombre);
                r = new receta(nombre);
                r->leer();
                delete r;
                break;
            default:
                cout << "Terminando ejecuciÃ³n."<< endl;
                return 0;
        }
        opcion = "";
    }

}