#include <iostream>
#include "RegistroDesplazamiento.h"
using namespace std;

int main (void){
    RegistroDesplazamiento reg1(16); // crea registro de 16 bits. Su valor inicial es 0.
    RegistroDesplazamiento reg2(8); // crea registro de 8 bits.
    //reg1 << 1 // desplaza el registro hacia la izquierda ingresando un 1.
    //reg2 = 0xAD; // carga valores lógicos correspondientes al registro desde bit menos significativo hasta el tamaño del menor.
    //reg1 = reg2; // carga los bits de reg2 en reg1, desde bit menos significativo hasta el tamaño del menor.
    //reg2 >> 2; // desplaza el registro en 2 posiciones hacia la derecha ingresando cada vez el bit de signo (el más izquierdo).
    //reg1++; // incrementa el valor del registro el cual es interpretado como un número binario.
    //cout << reg1;
    //cout << reg2[3] // muestra valor lógico del 4° bit, índice 0 es el menos significativo.
}