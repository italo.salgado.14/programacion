#ifndef RECETA_H
#define RECETA_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;

class receta {
public:
    receta(string nombre);
    ~receta();
    void leer();
    static void crear();

private:
    fstream archivo;
    string nombre;
    vector<string> ingredientes;
    string instrucciones;
};

receta::receta(string nombre) {
    string aux;
    try {
        archivo.open(nombre + ".receta");
        archivo.exceptions ( ifstream::eofbit | ifstream::failbit | ifstream::badbit ); //exep
        this->nombre = nombre;
        getline(archivo, nombre);
        getline(archivo, aux); //
        if (aux == "ingredientes:") {
            getline(archivo, aux); //
            while (aux != "instrucciones:") {
                ingredientes.push_back(aux);
                getline(archivo, aux); //
            }
            getline(archivo, instrucciones); //
        }
    }catch(std::exception const& e){
        cout << "Hubo un error al leer su receta" << nombre << endl;
    }

}


receta::~receta() {
    archivo.close();
}

void receta::leer() {
    cout << "Receta " + nombre << endl << endl;
    cout << "Ingredientes: " << endl;
    for(int i=0; i<ingredientes.size(); i++) {
        cout << ingredientes[i] << endl;
    }
    cout << endl;
    cout << "Instrucciones:" << endl << instrucciones << endl;
}

void receta::crear() {
    ofstream archivo;
    string nombre;

    cout << "Indique el nombre de la receta:" << endl;
    getline(cin, nombre);
    try {
        archivo.open(nombre + ".receta");
        archivo.exceptions ( ifstream::eofbit | ifstream::failbit | ifstream::badbit ); //exepc
        archivo << nombre << endl;

        cout << endl << "Ingrese ingredientes: (vacio para terminar)" << endl;
        string ingrediente;
        archivo << "ingredientes:" << endl;
        while (true) {
            getline(cin, ingrediente);
            if (!ingrediente.empty())
                archivo << ingrediente << endl;
            else
                break;
        }
        cout << "Ingrese instrucciones: (una Ãºnica entrada de texto)" << endl;
        string instrucciones;
        getline(cin, instrucciones);
        archivo << "instrucciones:" << endl << instrucciones << endl;
        archivo.close();
    }catch(std::exception const& e){
        cout << "Hubo un error al abrir la receta" << nombre << endl;
    }
}


#endif // RECETA_H
