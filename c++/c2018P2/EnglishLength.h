//
// Created by lailapc on 08-09-19.
//

#ifndef C2018P2_ENGLISHLENGTH_H
#define C2018P2_ENGLISHLENGTH_H

#include <iostream>
using namespace std;



class EnglishLength {
public:
    EnglishLength();
    int getLength();
    friend ostream & operator<< (ostream&  , EnglishLength);

protected:
    //int valueFoot;
    int valueInch;
};


#endif //C2018P2_ENGLISHLENGTH_H
