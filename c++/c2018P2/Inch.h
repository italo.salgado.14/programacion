    //
// Created by lailapc on 08-09-19.
//

#ifndef C2018P2_INCH_H
#define C2018P2_INCH_H


#include "EnglishLength.h"
//#include "Foot.h"
#include <iostream>

class Inch : public EnglishLength{
public:
    Inch(int);
    Inch();
    void operator += (EnglishLength);
    friend ostream & operator<< (ostream&  , Inch);

private:

};


#endif //C2018P2_INCH_H
