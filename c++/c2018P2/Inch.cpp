//
// Created by lailapc on 08-09-19.
//

#include "Inch.h"
Inch::Inch(int valueInch) {
    this->valueInch = valueInch;
}

Inch::Inch() {
    this->valueInch=0;
}

void Inch::operator+=(EnglishLength a) {
    valueInch = valueInch + (a.getLength());

}

ostream & operator<< (ostream& os, Inch el){
    os << el.valueInch <<" [inch]";
    return os;
}