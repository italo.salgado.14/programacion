//
// Created by lailapc on 08-09-19.
//

#ifndef C2018P2_FOOT_H
#define C2018P2_FOOT_H

#include "EnglishLength.h"
//#include "Inch.h"

class Foot: public EnglishLength {
public:
    Foot();
    void operator=(int);
    Foot operator + (EnglishLength);

private:

};


#endif //C2018P2_FOOT_H
