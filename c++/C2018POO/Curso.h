//
// Created by lailapc on 08-09-19.
//

#ifndef CURSO_H
#define CURSO_H
#include <iostream>
#include <vector>
#include <string>
class Curso {
public:
// Obtiene promedio de notas
    float get_notProm();
// Obtiene promedio de asistencia
    float get_asiProm();
// Consulta y escribe asistencia
    void set_asistencia();
// Añade estudiante
    void add_estudiante(std::string name);
// Permite ingresar notas
// En cada ingreso se crear un nuevo
  //  'vector<float>' y se consulta
// por las notas de los estudiantes
  //          existentes en el vector 'estudiantes'
    void set_notas();
private:
    std::vector< std::vector<float> > notas;
    std::vector<float> asistencia;
    std::vector<std::string> estudiantes;
};
#endif // CURSO_H