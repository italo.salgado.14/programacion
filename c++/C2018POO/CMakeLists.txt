cmake_minimum_required(VERSION 3.14)
project(C2018POO)

set(CMAKE_CXX_STANDARD 14)

add_executable(C2018POO main.cpp Curso.cpp Curso.h)