
#ifndef REGISTRO_DESPLAZAMIENTO
#define REGISTRO_DESPLAZAMIENTO

#include <ostream>
using namespace std;
class RegistroDesplazamiento {
public:
    RegistroDesplazamiento(int n);
    RegistroDesplazamiento(const RegistroDesplazamiento &r);
    const RegistroDesplazamiento& operator << (int b);
    const RegistroDesplazamiento& operator = (int i);
    const RegistroDesplazamiento& operator = (const RegistroDesplazamiento& r);
    const RegistroDesplazamiento& operator >> (int i);
    const RegistroDesplazamiento& operator ++ (int);
    int& operator [] (int i);
    friend ostream& operator << (ostream &os, const RegistroDesplazamiento &r);
    ~RegistroDesplazamiento();
private:
    int size;
    int * pr; // no es eficiente en almacenamiento, pero OK en este caso.
};
#endif