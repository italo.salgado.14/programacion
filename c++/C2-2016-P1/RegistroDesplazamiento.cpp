/* Archivo “RegistroDesplazamiento.cpp” */
#include "RegistroDesplazamiento.h"
using namespace std;

RegistroDesplazamiento::RegistroDesplazamiento(int i): size(i){
    pr = new int[i];
}
RegistroDesplazamiento::~RegistroDesplazamiento(){
    delete [] pr;
}
RegistroDesplazamiento::RegistroDesplazamiento(const RegistroDesplazamiento& r):size(r.size){
    pr = new int[size];
    for (int i=0; i<size; i++)
        pr[i] = r.pr[i];
}
const RegistroDesplazamiento& RegistroDesplazamiento::operator<<(int b){
    for (int i=0; i<size­1; i++)
        pr[i+1] = pr[i];
    pr[0]=b;
    return *this;
}
const RegistroDesplazamiento & RegistroDesplazamiento::operator=(int v){
    for (int i=0; i<size; i++){
        pr[i] = v%2;
        v/=2;
    }
    return *this;
}
const RegistroDesplazamiento & RegistroDesplazamiento::operator=(const RegistroDesplazamiento & r){
    int min = size < r.size? size:r.size;
    for (int i=0; i<min; i++)
        pr[i] = r.pr[i];
    return *this;
}
const RegistroDesplazamiento & RegistroDesplazamiento::operator>>(int j){
    for (int i=0; i<size­j; i++)
        pr[i] = pr[i+j];
    for (int i=size­j; i<size­1; i++)
        pr[i]=pr[size­1];
    return *this;
}
const RegistroDesplazamiento & RegistroDesplazamiento::operator++(int j){
    int c=1,s;
    for (int i=0; i<size­1; i++){
        s = pr[i] || c ? 1:0;
        c = pr[i] && c ? 1:0;
        pr[i]=s;
    }
    return *this;
}
int& RegistroDesplazamiento::operator[](int i){
    return pr[i];
}
ostream& operator << (ostream &os, const RegistroDesplazamiento &r){
    for (int i=r.size­1; i>=0; i­­)
        os << r.pr[i];
    return os;
}