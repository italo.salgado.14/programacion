import java.awt.*;
import java.awt.geom.*;
//import java.util.*;
import java.awt.event.*;
import javax.swing.*;
/**
   This class represents a possible traffic light view.
   In this case, it corresponds to a pedestrian crosswalk traffic light 
   (red, green, and green blinking light)
+++++++++++++


 public abstract void setFrame(double x,double y,double w, double h)
 Sets the location and size of the framing rectangle of this Shape to the specified rectangular values.
 ------
 x: la coordenada X de la esquina superior izquierda de la forma rectangular especificada
 y - la coordenada Y de la esquina superior izquierda de la forma rectangular especificada
 w - el ancho de la forma rectangular especificada
 h - la altura de la forma rectangular especificada

 public Rectangle2D getFrame()
 Returns the framing Rectangle2D that defines the overall shape of this object.
--------
 Returns:
 a Rectangle2D, specified in double coordinates

 public abstract void fill(Shape s)
 Rellena el interior de una forma utilizando la configuración del contexto Graphics2D. Los atributos de
 representación aplicados incluyen Clip, Transform, Paint, and Composite.
--------
 Parámetros:
 s - la forma a rellenar


*/
public class CrosswalkTrafficLight extends TrafficLight implements ActionListener, View{

    /**
     * CONSTRUCTOR
     */
    public CrosswalkTrafficLight (int ft, int tt, double posX, double posY) {
      super(ft, tt); //llamamos al constructor superior, TrafficLight
      this.posX = posX;
      red_view = new Ellipse2D.Double(origen_x + posX, origen_y + posY, DIAMETER, DIAMETER);
      green_view = new Ellipse2D.Double(origen_x + posX, origen_y +DIAMETER +posY, DIAMETER, DIAMETER);

      cabinet = red_view.getFrame();
      cabinet = cabinet.createUnion(green_view.getFrame());

      timer = new Timer(ON_OFF_TIME, this); //timer con coneccion al listener parpadeo
      green_on=false; 
   }

    /**
     * METODOS
     */
   public void paint_view (Graphics2D g2d) {
      g2d.setColor(Color.BLACK);
      g2d.fill(cabinet); //pintar negro el cuadrado

      switch (getState()) {
         case STOP: if (timer.isRunning()) timer.stop(); //apagamos el timer de parpadeo
                    g2d.setColor(Color.RED);  //steamos el pinteo en rojo
                    g2d.fill(red_view); //pintamos la luzRoja
                    g2d.setColor(Color.GRAY);  //seteamos el pinteo en plomo
                    g2d.fill(green_view); //pintamos la luzVerde
                    break;
         case TRANSITION: if (!timer.isRunning()) timer.start(); //encendemos el timer de parpadeo
                          g2d.setColor(Color.GRAY);
                          g2d.fill(red_view);
                          if(green_on)  //variable en el listener que cambia cada 500ms
                             g2d.setColor(Color.GREEN); 
                          g2d.fill(green_view);
                          break;
         case FOLLOW: g2d.setColor(Color.GRAY);
                      g2d.fill(red_view);
                      g2d.setColor(Color.GREEN); 
                      g2d.fill(green_view);
      }        
   }

   // aqui parpadeamos la luz, que lo activa el timer de parpadeo
   public void actionPerformed (ActionEvent event) {
      green_on = !green_on;
      panel.repaint();
   }
   private Ellipse2D red_view, green_view;
   private Rectangle2D cabinet;
   private Timer timer;
   private boolean green_on;
   private int origen_x=100;
   private int origen_y=50;
   private static int DIAMETER=50;
   private static int ON_OFF_TIME = 500; //[ms]
   private double posX;
   private double posY;
}