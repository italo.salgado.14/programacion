/**
   @version 1.00 2019-04-18
   @author Agustín J. González
*/

import java.awt.*;
import javax.swing.*;
import java.util.*;

/**
 *  main -> frame
 */

public class TwoTrafficLightsExample {  
   public static void main(String[] args) {
      final MyOwnPanel myPanel = new MyOwnPanel();
      CrosswalkTrafficLight mattaCrosswalk = new CrosswalkTrafficLight(6,4,0,0);
      myPanel.addView(mattaCrosswalk);
      
      StreetTrafficLight mattaTrafficLight = new StreetTrafficLight(8,3,0,0);
      myPanel.addView(mattaTrafficLight);

      //extra
      StreetTrafficLight placeresTrafficLight = new StreetTrafficLight(2,2, 200,0);
      myPanel.addView(placeresTrafficLight);

      Controller controller = new Controller(mattaTrafficLight, mattaCrosswalk, placeresTrafficLight);
            
      SwingUtilities.invokeLater(new Runnable() { // implementación Swing recomendada
         public void run() {
            MyOwnFrame frame = new MyOwnFrame(myPanel);  //CREA EL FRAME Y LE ENTREGA EL PANEL
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
         } // run ends
      });
      
      controller.manageTraffic();
   }
}

/**
   A frame with a panel for a simple graphics object
 Jframe usa por std el Border Layout
*/
class MyOwnFrame extends JFrame  {
   public MyOwnFrame(JPanel panel) {
      setTitle("Two Traffic Lights Example");
      setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);

      getContentPane().add(panel);// add panel to frame
   }

   public static final int DEFAULT_WIDTH = 450;
   public static final int DEFAULT_HEIGHT = 600;
}





/**
   A panel for a simple graphics object.
*/
class MyOwnPanel extends JPanel  {
   /**
    * CONSTRUCTOR
    */
   public MyOwnPanel() {  
      views = new ArrayList<View> ();
      setFocusable(true); //se utiliza principalmente para activar / desactivar el evento de enfoque de la vista
   }

   /**
    * METODOS
   For each call, the panel sets itself within
   the view being added to the panel.
   */
   public void addView(View v) {  
      views.add(v);
      v.setPanel(this);
   }
   public void paintComponent(Graphics g)   {  
      super.paintComponent(g); //esto pinta lo que yo no pinte en el fonod, debe estar aqui siempre!
      Graphics2D g2 = (Graphics2D) g; // For historical reasons, this method
      for (View v: views){     // has Graphics as parameter, but now an intance
         v.paint_view(g2);   // of a subclass of Graphics, instance of Graphics2D,
         System.out.println(views);
         System.out.println(" ");
      }                      // is passed as argument.
   }

   /**
    * ATRIBUTOS
    */
   private ArrayList<View> views;
}
