public class Controller {
   public Controller(TrafficLight m, TrafficLight p, TrafficLight pp) {
      matta = m;
      pedestrian = p;
      placeres = pp;
   }
   public void manageTraffic(){
      try {
          while (true) {
            matta.turnFollow();
            Thread.sleep(matta.getFollowTime()*1000);
            matta.turnTransition();
            Thread.sleep(matta.getTransitionTime()*1000);
            matta.turnStop();

            pedestrian.turnFollow();
            Thread.sleep(pedestrian.getFollowTime()*1000);
            pedestrian.turnTransition();
            Thread.sleep(pedestrian.getTransitionTime()*1000);
            pedestrian.turnStop();

            //extra
            placeres.turnFollow();
            Thread.sleep(placeres.getFollowTime()*1000);
            placeres.turnTransition();
            Thread.sleep(placeres.getTransitionTime()*1000);
            placeres.turnStop();
         }   
     } catch (InterruptedException e){
        System.out.println(e);
     }
   }
   private TrafficLight pedestrian, matta, placeres;
}
