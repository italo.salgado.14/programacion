import java.awt.Graphics2D;
import javax.swing.JPanel;
/**
  Any View must know how to paint itself.
  Any View must be displayed on a panel, so its panel
  must be set.
  To update a view, its panel must be repainted,
  this is achieved with repaint method.

 Cualquier vista debe saber cómo pintarse.
 Cualquier vista debe mostrarse en un panel, por lo que su panel
 se debe establecer.
 Para actualizar una vista, su panel debe ser repintado,
 Esto se logra con el método de repintado.
*/
public interface View {
   void paint_view(Graphics2D g2d);
   void repaint();
   void setPanel(JPanel panel);
}