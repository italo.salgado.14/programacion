// Programa Java para leer datos de varios tipos usando la clase Scanner
import java.util.Scanner;
public class ScannerDemo
{
    public static void main(String[] args)
    {
        // Declarar el objeto e inicializar con
        // el objeto de entrada estándar predefinido
        Scanner sc = new Scanner(System.in);

        // entrada de una cadena (linea) completa
        System.out.println("Nombre:");
        String name = sc.nextLine();

        // entrada de un solo caracter
        System.out.println("Genero:");
        char gender = sc.next().charAt(0);

        // Entrada de entero completo
        System.out.println("Edad:");
        int age = sc.nextInt();

        //entrada numero long
        System.out.println("Numero telefono:");
        long mobileNo = sc.nextLong();

        //entrada de numero double
        System.out.println("Promedio Notas:");
        double average = sc.nextDouble();

        // Imprima los valores para verificar si la entrada
        // fue obtenida correctamente.
        System.out.println("Nombre: "+name);
        System.out.println("Género: "+gender);
        System.out.println("Edad: "+age);
        System.out.println("Teléfono: "+mobileNo);
        System.out.println("Promedio: "+average);
    }
}