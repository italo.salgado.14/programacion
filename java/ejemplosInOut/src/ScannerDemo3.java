import java.util.*;

public class ScannerDemo3 {
    public static void main(String[] args) {

        String s = "Hello World! 3 + 3.0 = 6.0 true ";
        Long l = 13964599874l;
        s = s + l;

        // create a new scanner with the specified String Object
        Scanner scanner = new Scanner(s);

        // find the next long token and print it
        // loop for the whole scanner
        while (scanner.hasNext()) {

            // if no long is found, print "Not Found:" and the token
            System.out.println("Not Found :" + scanner.next());

            // if the next is a long, print found and the long
            if (scanner.hasNextLong()) {
                System.out.println("Found :" + scanner.nextLong());
            }
        }

        // close the scanner
        scanner.close();
    }
}