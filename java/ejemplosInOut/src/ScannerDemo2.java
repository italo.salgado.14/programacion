// Programa Java para leer algunos valores usando la clase Scanner
// e imprimir su media
import java.util.Scanner;

/**
 * Leeremos la entrada hasta tener algo distinto a numeros.
 */
public class ScannerDemo2
{
    public static void main(String[] args)
    {
        // Declarar un objeto e inicializar con
        // el objeto de entrada estándar predefinido
        Scanner sc = new Scanner(System.in);

        // Inicializar la suma y el recuento
        // de los elementos de entrada
        int sum = 0, count = 0;

        System.out.println("Ingresa:");
        // Comprueba si un valor int está disponible: leerá hasta
        //ue se entrege un dato NOint
        while (sc.hasNextInt()){
            // Lee un valor int
            int num = sc.nextInt();
            sum += num;
            count++;
        }

        int mean = sum / count;
        System.out.println("La media es: " + mean);
    }
}