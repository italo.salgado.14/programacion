import javax.swing.*;
import java.awt.event.*;

public class CountDownTimer {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                CountDownFrame frame = new CountDownFrame();
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setVisible(true);
            }
        });
    }
}

class CountDownFrame extends JFrame {
    CountDownFrame() {
        JPanel panel = new CountDownPanel();
        getContentPane().add(panel);
        pack();
    }
}

class CountDownPanel extends JPanel implements ActionListener {
    private Timer tick;
    private JButton startStop = new JButton("Start");
    private JButton reset = new JButton("Reset");
    private int t = 60;
    private JLabel count = new JLabel("" + t);

    CountDownPanel() {
        startStop.addActionListener(this);
        reset.addActionListener(this);
        tick = new Timer(1000, new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                t--;
                count.setText("" + t);
            }
        });
        add(startStop);
        add(reset);
        add(count);
    }

    public void actionPerformed(ActionEvent event) {
        String command = event.getActionCommand();
        if (command.equals("Start")) {
            tick.start();
            startStop.setText("Stop");
        }
        if (command.equals("Stop")) {
            tick.stop();
            startStop.setText("Start");
        }
        if (command.equals("Reset")) {
            tick.stop();
            startStop.setText("Start");
            t = 60;
            count.setText("" + t);
        }
    }
}