/* P2.java */
import java.awt.*;
import javax.swing.*;
import java.util.*;

public class P2 {  
   public static void main(String[] args) {
      SwingUtilities.invokeLater(new Runnable() { 
         public void run() {
            MyOwnPanel tlPanel = new MyOwnPanel();

            CrosswalkTrafficLight crosswalk = new CrosswalkTrafficLight(6,4);
            tlPanel.setTrafficLight(crosswalk);

            MyOwnFrame frame = new MyOwnFrame();
            frame.add(tlPanel, BorderLayout.CENTER);

            MyButton button = new MyButton(crosswalk);
            frame.add(button, BorderLayout.SOUTH);

            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
         } 
      });
   }
}
class MyOwnFrame extends JFrame  {
   public MyOwnFrame() {
      setTitle("P2 Certamen ELO329 1s19");
      setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
   }
   public static final int DEFAULT_WIDTH = 150;
   public static final int DEFAULT_HEIGHT = 200;  
}
class MyOwnPanel extends JPanel  { 
   public MyOwnPanel() {  
      setFocusable(true);
   }
   public void setTrafficLight(CrosswalkTrafficLight light) {  
      tl = light;
      tl.setPanel(this);
   }
   public void paintComponent(Graphics g)   {  
      super.paintComponent(g);
      Graphics2D g2 = (Graphics2D)g; 
      tl.paint_view(g2);
   }
   private CrosswalkTrafficLight tl;
}
