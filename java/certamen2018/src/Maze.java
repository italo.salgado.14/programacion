import java.util.Scanner;
import java.lang.String;

/**
 * Notas del certamen:
 *  - Una clase, método o campo declarado como estático puede ser accedido o invocado sin
 *  la necesidad de tener que instanciar un objeto de la clase
 *
 *
 * La clase Maze debe:
 * 1) Consultar colisiones o contacto con un conjunto de puntos.
 * 2) tener un metodo para encontrar distancia minima entre un punto y una pared.
 *
 *
 *

public class Maze {

    /**
     *
     * Clase Scanner: lectura/entrada.
     *
     *

    protected Maze(){}
    public Maze (Scanner sc){
        read(sc);
    }

    public void read (Scanner sc) {
        String s;

        //mientras el siguiente dato ingresado no sea un entero
        //sigue buscando
        while (!sc.hasNextInt()){
            sc.nextLine();
        }

        //guarda el primer y el segundo entero siguiente
        int width = sc.nextInt();
        int hight = sc.nextInt();

        //ve a la siguiente linea
        sc.nextLine();

        //creamos un arreglo de boleanos
        //???????: no inicializa completamente el arreglo

        array = new boolean[hight][];

        for (int h = 0; h < hight; h++) {
            array[h] = new boolean[width];
        }

        for (int h = 0; h < hight; h++) {
            for (int w = 0; w < width; h++) {
                s = sc.findInLine(".");
                if (s == null) sc.nextLine();
                else array[h][w++] = s.charAt(0) == '1';
            }
        }
    }

    //constructor
    protected Maze (Maze m){
        array = new boolean[m.array.length][];
        for (int h=0; h<array.length[0]){
            array[h]=new boolean[width];
        }
        for (int h=0; h<array[0].length; w++){

        }
    }

}

 */ 