package notHelloWorld;

import javax.swing.*;
import java.awt.*;

/*
* Los frames estan estan diseñados para ser contenedores de componentes.
*
* Todos los dibujos en java deben pasar por un objeto Graphics
*
* NUNCA hay que llamar a paintComponent. Este es llamado automáticamente por
* la parte de las aplicaciones donde yo quiero que se pinte. Cuando ocurre esta wea?
* Cuando resizeo una ventana, o minimizo una ventana, cuando la restauro, etc.
 */

public class NotHelloWorld {
    public static void main(String[] args){
        EventQueue.invokeLater( ()-> {
            JFrame frame = new NotHelloWorldFrame();
            frame.setTitle("NotHelloWorld");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);

        });
    }
}

/**
* Aqui definimos el frame que contiene el menssage del panel
 */
class NotHelloWorldFrame extends JFrame{
    public NotHelloWorldFrame(){
        add( new NotHelloWorldComponent() );
        pack(); //rezicea la ventana, es como setSize pero sizea lo preferido para cada componente
    }
}
/**
 * Un componente que muestra el mensaje. Cada vez que una ventana necesite
 * ser repintada, por cualquier razon, el evento notificará al componente. Esto
 * causara que los metodos paintComponent de todos los componentes sean ejecutados.
 */
class NotHelloWorldComponent extends JComponent{
    public static final int MESSAGE_X=75;
    public static final int MESSAGE_Y=100;

    private static final int DEFAULT_WIDTH = 300;
    private static final int DEFAULT_HEIGHT = 200;

    public void paintComponent(Graphics g){
        g.drawString("Not a Hello, World programn", MESSAGE_X, MESSAGE_Y);
    }

    //aqui le decimos qlas dimensiones del texto
    public Dimension getPreferredSize() {
        return new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }
}