package CreatingaFrame;

import java.awt.*;
import javax.swing.*; //paquete de extension a java, por la x


public class SimpleFrameTest {
    public static void main(String[] args){
        EventQueue.invokeLater(() -> {

            /*
            * Todos los componentes de Swing deben ser configurados en el
            * EVENT DISPATCH THREAD. Pasara los eventos como clicks y keystrokes
             */
            SimpleFrame frame = new SimpleFrame();

            //aqui le decimos al programa que hacer al salir
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            frame.setVisible(true);
        });
    }
}

class SimpleFrame extends JFrame {
    private static final int DEFAULT_WIDTH = 300;
    private static final int DEFAULT_HEIGHT = 200;

    public SimpleFrame(){
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT); //por default es de 0x0
    }
}
