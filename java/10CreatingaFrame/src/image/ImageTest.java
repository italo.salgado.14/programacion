package image;

import javax.swing.*;
import java.awt.*;

public class ImageTest {
    public static void main(String[] args){
        EventQueue.invokeLater( ()-> {
            JFrame frame = new ImageFrame();
            frame.setTitle("TestofImage");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
        });
    }
}

/**
 * Un frame con un componente-imagen
 */
class ImageFrame extends JFrame{
    public ImageFrame(){
        add (new ImageComponent() );
        pack();
    }
}


class ImageComponent extends JComponent{
    private static final int DEFAULT_WIDTH=300;
    private static final int DEFAULT_HEIGHT=200;
    private Image img;

    public ImageComponent(){
        img = new ImageIcon("src/image/blue-ball.gif").getImage();
    }

    public void paintComponent(Graphics g){
        if (img == null){
            return;
        }

        int imageWidth = img.getWidth(this);
        int imageHeight = img.getHeight(this);

        //dibujar la imagen en la parte de arriba a la isquierda
        g.drawImage(img, 0, 0, null);

        //repetimos la imagen por todo el componente
        for (int i=0; i*imageWidth <= getWidth(); i++){
            for (int j=0; j*imageHeight <= getHeight(); j++){
                if (i+j>0){
                    g.copyArea(0,0,imageWidth, imageHeight,i*imageWidth,j*imageHeight);
                }
            }
        }
    }

    public Dimension getPreferredSize(){
        return new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }
}