package SizedFrame;

import java.awt.*;
import javax.swing.*;

public class SizedFrameTest {
    public static void main(String[] args){
        EventQueue.invokeLater( ()-> {
           JFrame frame = new SizedFrame();
           frame.setTitle("SizedFrame");
           frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
           frame.setVisible(true); //si no lo seteamos visible, no pasa nada
        });
    }
}

class SizedFrame extends JFrame{
    public SizedFrame(){
        //dimensiones
        Toolkit kit = Toolkit.getDefaultToolkit();
        Dimension screenSize = kit.getScreenSize();
        int screenHeight = screenSize.height;
        int screenWidth = screenSize.width;

        //seeteo del tamaño del frame
        setSize(screenWidth/2 , screenHeight/2);
        setLocationByPlatform(true);

        //seteo de icono para el frame
        Image img= new ImageIcon("src/SizedFrame/icon.gif").getImage();
        setIconImage(img);
    }
}