package font;

import javax.swing.*;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

public class FontTest {
    public static void main(String[] args){
        EventQueue.invokeLater( ()-> {
            JFrame frame = new FontFrame();
            frame.setTitle("FontTest");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
        });
    }
}

/**
 * Un frame con un componente mensaje de texto
 */
class FontFrame extends JFrame{
    public FontFrame(){
        add(new FontComponent() );
        pack();
    }
}

/**
 * Un compontente que muestra un texto centrado en un recuadro
 */
class FontComponent extends JComponent{
    private static int DEFAULT_WIDTH=300;
    private static int DEFAULT_HEIGHT=200;

    public void paintComponent(Graphics g){
        Graphics2D g2 = (Graphics2D) g;
        String msg= "hello world";

        // Creamos un objeto Font
        //1-name: El nombre del tipo de letra
        //2-style: El estilo de la letra; Font.PLAIN, Font.ITALIC, etc
        //3-size
        Font f = new Font("Serif",Font.BOLD,36);

        g2.setFont(f);

        //medida de el tamaño del mensaje
        FontRenderContext context = g2.getFontRenderContext();
        Rectangle2D bounds = f.getStringBounds(msg,context);

        //setear el (x,y) = arriba-isquierda
        double x = (getWidth() - bounds.getWidth() )/2;
        double y = (getHeight() - bounds.getHeight())/2;

        //
        double ascent = -bounds.getY();
        double baseY = y + ascent;

        //dibujamos el mensaje
        g2.drawString(msg, (int)x, (int)baseY);
        g2.setPaint(Color.LIGHT_GRAY);

        //dibujar la lineabase
        g2.draw(new Line2D.Double(x, baseY, x+bounds.getWidth(), baseY));

        //dibujamos el rectangulo que encierra
        Rectangle2D rect = new Rectangle2D.Double(x, y, bounds.getWidth(), bounds.getHeight() );
        g2.draw(rect);
    }

    public Dimension getPreferredSize(){
        return new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }
}