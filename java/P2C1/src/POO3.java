import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class POO3 {
	public static void main (String[] args) {
		Comunidad vecinos = new Comunidad();
		vecinos.add("Francisco", 19); // Nombre y edad
		vecinos.add("Jorge", 25); 
		vecinos.add("Camila", 19);
		System.out.println("La edad de Camila es "+vecinos.getAge("Camila"));
		vecinos.listarOrdenados();
	}
}

//la ide me dijo ue no fuese publica
class Persona {
	public Persona(String nombre, int edad ){
		this.edad=edad;
		this.nombre=nombre;
	}

	public String getName(){
		return nombre;
	}
	public int getAge(){
		return edad;
	}

	String nombre;
	int edad;
}

class Comunidad {
	public Comunidad(){
		views = new ArrayList<>();
	}

	public void add(String name, int edad){
		Persona personaNueva = new Persona(name, edad);
		views.add(personaNueva);
	}

	public int getAge(String nombrebus){
		for (Persona p: views){
			if (p.getName().equals(nombrebus) ){
				return p.getAge();
			}
		}
		return -1;
	}
	public void listarOrdenados(){
		Collections.sort(views, new Comparator() {
			@Override
			public int compare(Object o, Object t1) {
				return 0;
			}

			@Override
			public int compare(Persona p1, Persona p2) {
				return new Integer(p1.getAge()).compareTo(new Integer(p2.getAge()));
			}
		});
	}
	private ArrayList<Persona> views;
}

/*
class MyOwnPanel extends JPanel  {

public MyOwnPanel() {
	views = new ArrayList<View> ();
	setFocusable(true); //se utiliza principalmente para activar / desactivar el evento de enfoque de la vista
}

	public void addView(View v) {
		views.add(v);
		v.setPanel(this);
	}
	public void paintComponent(Graphics g)   {
		super.paintComponent(g); //esto pinta lo que yo no pinte en el fonod, debe estar aqui siempre!
		Graphics2D g2 = (Graphics2D) g; // For historical reasons, this method
		for (View v: views){     // has Graphics as parameter, but now an intance
			v.paint_view(g2);   // of a subclass of Graphics, instance of Graphics2D,
			System.out.println(views);
			System.out.println(" ");
		}                      // is passed as argument.
	}


	private ArrayList<View> views;
}
*/
