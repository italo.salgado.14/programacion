/*
 * A simple hashmap
 */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h> 

enum nodes {MOTOR, BMS};
enum var_motor {MOTOR_SPEED};
enum var_bms {BMS_VOLTAGE, BMS_TEMPERATURE};

/*********STRUCTS-AND-DEFINITIONS************/
typedef struct _element {
    void *addr;
    size_t len;
} element_t;

typedef struct _node {
    uint32_t id;
    element_t *elements[];
} node_t;

/**************VAR***********************/
float motor_speed = 0.0;
uint16_t bms_voltage = 60;
float bms_temperature = 0.0;

/***************DESIGN-HASH*********************/
//motor
element_t e_motor_speed = {
    &motor_speed, sizeof(motor_speed) 
};
node_t n_motor = {MOTOR, &e_motor_speed };

//bms
element_t e_bms[2] = {
    &bms_voltage, sizeof(bms_voltage), 
    &bms_temperature, sizeof(bms_temperature)
};
node_t n_bms = { BMS, {&e_bms[0], &e_bms[1]}
};

//all_nodes
node_t *allnodes[] = {&n_bms, &n_motor};

int main()
{
    uint8_t a = allnodes[0]->elements[0]->len;    

    printf("BMS Voltage:\t%d\n", *( (uint16_t *) allnodes[0]->elements[0]->addr) );
    printf("node BMS:\t%d\n",  ( allnodes[0]->id)) ;
    printf("Size of variable:%zu ---- The len of data stored: %td \n", sizeof(allnodes[0]->elements[0]->len), allnodes[0]->elements[0]->len);
    printf("Len of bms:\t%u\n",  a) ;

    return 0;
}