cmake_minimum_required(VERSION 3.19)
project(taller5 C)

set(CMAKE_C_STANDARD 99)

add_executable(taller5 main.c myBST.c myBST.h)