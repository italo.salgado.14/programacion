/*
 * @file    : main_BST.c
 * @author	: Ioannis Vourkas
 * @date	: 09/06/2020
 * @brief   : Template de código sobre ABB para Taller 04 - ELO 320
 */

#include <stdlib.h>
#include <stdio.h>

#include "myBST.h"

void myfunc(Object* obj){
    if(obj != NULL){
        myfunc(obj->right);
        printf("Num: %d\n", obj->number);
        myfunc(obj->left);
    }
    return;
}

int main (){
    Object* BST_root = NULL; //Puntero al nodo raíz del ABB
    Object* temp = NULL;     //Puntero auxiliar

    /*EJEMPLOS de uso de las funciones disponibles*/

    /*Crear el ABB mostraro el la diapositiva 25: Clase 10*/
    BST_insertObject(&BST_root,15);

    BST_insertObject(&BST_root,18);
    BST_insertObject(&BST_root,17);
    BST_insertObject(&BST_root,20);

    BST_insertObject(&BST_root, 6);
    BST_insertObject(&BST_root, 3);
    BST_insertObject(&BST_root, 2);
    BST_insertObject(&BST_root, 4);

    BST_insertObject(&BST_root, 7);
    BST_insertObject(&BST_root,13);
    BST_insertObject(&BST_root, 9);

    /*Intentar ingreasar valor repetido*/
    BST_insertObject(&BST_root, 9);
    puts("---------------------------\n");

    /*  */
    printf("------------en orden-------------\n");
    BST_traverseInOrder(BST_root);
    printf("------------mayor menor-------------\n");
    myfunc(BST_root);
    printf("------------mayor menor-------------\n");

    /*Buscar un número en el árbol*/
    int num = 13;
    temp = BST_searchObject(BST_root, num);
    if(temp != NULL){
        printf("Se encontró %d en el ABB!\n", temp->number);
    } else {
        printf("No existe el %d en el ABB!\n", num);
    }
    puts("\n");

    /*Buscar el nodo con el menor valor*/
    temp = BST_searchMinValue(BST_root);
    printf("El menor valor en el ABB es el %d\n", temp->number);
    temp = BST_searchMaxValue(BST_root);
    printf("El menor valor en el ABB es el %d\n", temp->number);
    printf("--------------------------------\n");

    BST_traverseInOrder(BST_root);

    num = 9;
    temp = BST_searchParent (BST_root, num);
    if (temp != NULL){
        printf("El nodo padre del nodo con valor %d tiene valor %d\n", num, temp->number);
    }
    else {
        printf("El nodo padre del nodo con valor %d es NULL\n", num);
    }

    num = 13;
    temp = BST_searchObject(BST_root, num);
    temp = BST_searchSuccessor (temp, BST_root);
    if (temp != NULL){
        printf("El nodo sucesor del nodo con valor %d tiene valor %d\n", num, temp->number);
    }
    else {
        printf("El nodo sucesor del nodo con valor %d es NULL\n", num);
    }

    BST_deleteTree(BST_root);

    return EXIT_SUCCESS;
}
