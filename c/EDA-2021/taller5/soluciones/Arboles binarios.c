#include <stdio.h>
#include <stdlib.h>

struct node
{
    int data; //nodo, con un entero como dato
    struct node *right_child; // hijo derecho
    struct node *left_child; // hijo izquierdo
};

struct node* search(struct node *root, int x) //busca un elemento particular x
{
    if(root==NULL || root->data==x) //si root->data es x entonces hemos encontrado el elemento
        return root;
    else if(x>root->data) // x es mayor, por lo tanto buscaremos por la drecha
        return search(root->right_child, x);
    else //x es menor que data, por lo tanto buscaremos por la izquierda
        return search(root->left_child,x);
}

struct node* find_minimum(struct node *root)//esta función encuentra el mínimo 
{
    if(root == NULL)
        return NULL;
    else if(root->left_child != NULL) // el nodo con el valor mínimo, no tendrá un hijo izquierdo
        return find_minimum(root->left_child); // el nodo más a la izquierda será el mínimo
    return root;
}


struct node* new_node(int x) //función para crear un nodo
{
    struct node *p;
    p = malloc(sizeof(struct node));
    p->data = x;
    p->left_child = NULL;
    p->right_child = NULL;

    return p;
}

/*
struct node* successor(struct node *n) {
    if ( n->right_child != NULL )
        return find_minimum(n->right_child);
    struct node *p = n->parent; // ojo, a este "parent" lo tienen que encontrar (hint: usar "en orden")
        while ((p != NULL) && (n == p->right_child)) {
            n = p;
            p = p->parent;
        }
    return p;
}*/



struct node* insert(struct node *root, int x)//función (recursiva) para insertar un nodo
{
    //buscamos el lugar para insertar el nodo
    if(root==NULL)
        return new_node(x);
    else if(x>root->data) // x es mayor, tiene que ir por la derecha
        root->right_child = insert(root->right_child, x);
    else // x es menor, debemos insertar por la izquierda
        root->left_child = insert(root->left_child,x);
    return root;
}

struct node* delete(struct node *root, int x) //función para eliminar un nodo (buscamos recursivamente)
{
    //buscamos el elemento a eliminar
    if(root==NULL)
        return NULL;
    if (x>root->data)
        root->right_child = delete(root->right_child, x);
    else if(x<root->data)
        root->left_child = delete(root->left_child, x);
    else
    {
        //no tiene hijos
        if(root->left_child==NULL && root->right_child==NULL)
        {
            free(root);
            return NULL;
        }

        //tiene un hijo
        else if(root->left_child==NULL || root->right_child==NULL)
        {
            struct node *temp;
            if(root->left_child==NULL)
                temp = root->right_child;
            else
                temp = root->left_child;
            free(root);
            return temp;
        }

        //tiene dos hijos
        else
        {
            struct node *temp = find_minimum(root->right_child);
            root->data = temp->data;
            root->right_child = delete(root->right_child, temp->data);
        }
    }
    return root;
}

void inorder(struct node *root) //búsqueda en orden
{
    if(root!=NULL) // chequeamos que la raíz no sea nula
    {
        inorder(root->left_child); // visitamos hijo izquierdo
        printf(" %d ", root->data); // imprimimos dato
        inorder(root->right_child);// visitamos hijo derecho
    }
}

int main()
{
    /*
                   20
                 /    \
                /      \
               5       30
             /   \     /\
            /     \   /  \
           1      15 25  40
                /          \
               /            \
              9             45
            /   \          /
           /     \        /
          7      12      42
    */
    struct node *root;
    root = new_node(20);
    insert(root,5);
    insert(root,1);
    insert(root,15);
    insert(root,9);
    insert(root,7);
    insert(root,12);
    insert(root,30);
    insert(root,25);
    insert(root,40);
    insert(root, 45);
    insert(root, 42);

    inorder(root);
    printf("\n");

    root = delete(root, 1);
    /*
                   20
                 /    \
                /      \
               5       30
                 \     /\
                  \   /  \
                  15 25   40
                /           \
               /             \
              9              45
            /   \           /
           /     \         /
          7      12       42
    */

    root = delete(root, 40);
    /*
                   20
                 /    \
                /      \
               5       30
                 \     /\
                  \   /  \
                  15 25  45
                 /       / 
                /       /   
               9       42    
             /   \          
            /     \        
           7      12      
    */

    root = delete(root, 45);
    /*
                   20
                 /    \
                /      \
               5       30
                 \     /\
                  \   /  \
                  15 25  42
                 /          
                /            
               9            
             /   \          
            /     \        
           7      12      
    */
    root = delete(root, 9);
    inorder(root);
    /*
                   20
                 /    \
                /      \
               5       30
                 \     /\
                  \   /  \
                  15 25  42
                 /          
                /            
               12            
             /             
            /             
           7            
    */
    printf("\n");

    return 0;
}

