/*
 * @file    : myBST.c
 * @author  : Ioannis Vourkas
 * @date    : 09/06/2020
 * @brief   : Template de código sobre ABB para Taller 04 - ELO 320
 */

#include <stdio.h>
#include <stdlib.h>
#include "myBST.h"

Object* getNewObject (int num, Object* leftObject, Object* rightObject) { // funcion general para ingresar elementos a ABB
  Object* new_object = (Object*) malloc(sizeof (Object));
  if (new_object == NULL) {
    fprintf (stderr, "Error creating a new object with malloc.\n"); 
  }
  else {
    /*Actualizo el dato y los punteros left/right del nuevo objeto*/
    new_object->number = num;
    new_object->left = leftObject;
    new_object->right = rightObject;
  }
  
  return new_object;
}

Object* BST_insertObject (Object** objectRef, int num){
    // si paso como referencia mi raiz
  Object* newObject = *objectRef;
  if(*objectRef == NULL){
    /*Si he llegado a la posición adecuada, creo el Objeto*/
    *objectRef = getNewObject(num, NULL, NULL);
    newObject = *objectRef;// actualizo la direccion de memoria de mi raiz
  }
  //de lo contrario, evaluo si debo buscar izquierda o derecha
  else if(num < (*objectRef)->number){
    newObject = BST_insertObject(&((*objectRef)->left), num); // avazo por la izq
  }
  else if(num > (*objectRef)->number){
    newObject = BST_insertObject(&((*objectRef)->right), num);// avanzo por la der
  }
  else {
    /*Avisar cuando se intenta agregar un valor repetido y retornar NULL*/
    fprintf (stderr, "Error with insertion. Number %d already in ABB.\n", num);
    newObject = NULL;
    // Como puedo agregar valores repetidos? actualizo el valor o que
  }
  return newObject;
}

Object* BST_removeObject (Object** objectRef, int num){
  //Llenar con su código
    if(*objectRef == NULL){
        return *objectRef;
    }
    else if((*objectRef)->number > num){ // si numero es menor que number , ir por izq
        (*objectRef)->left = BST_removeObject(&(*objectRef)->left, num);
    }
    else if((*objectRef)->number < num){
        (*objectRef)->right = BST_removeObject(&(*objectRef)->right, num);
    }
    // si no es mayor, ni menor , ni null -> encontramos nodo!
    else{
        // si no tiene hijos
        if((*objectRef)->left == NULL && (*objectRef)->right == NULL){
            free(*objectRef);
            return NULL;
        }
        //si tiene 1 hijo
        else if((*objectRef)->left == NULL || (*objectRef)->right == NULL){
            Object *temp;
            if((*objectRef)->left == NULL){
                temp = (*objectRef)->right; // le paso el hijo derecho
            }
            else{
                temp = (*objectRef)->left; // le paso el hijo izquierdo
            }
            free(*objectRef);
            return temp; // retorno el nodo del hijo que quedó
        }
        // si tiene 2 hijos
        //RECORDAR: siempr el de la derecha es mayor que su padre pero menor que los de otro nodo en el mismo nivel
        else{ // necesito enlazar los huerfanos a uun nuevo padre
            Object * temp = BST_searchMinValue((*objectRef)->right);
            // reemplazo el valor del hijo mayor en el padre
            (*objectRef)->number = temp ->number;
            // tegno que borrar el hijo derecho y hacer free -> recursion sobre el hijo derecho
            (*objectRef)->right = BST_removeObject(&(*objectRef)->right, temp->number);
            // al usar recursividad, queda atrapado en los demas casos
        }

    }


  
  return *objectRef;
}

Object* BST_searchObject (Object* obj, int num){
  /*Mientras encuentro objetos validos (no NULL), chequear*/
  while(obj != NULL){
    if(num == obj->number){
      break;
    }
    if(num < obj->number){
      obj = obj->left;
    }
    else {
      obj = obj->right;
    }
  }
  return obj;
}

Object* BST_searchParent (Object* treeRoot, int num){
  /*Mientras encuentro objetos validos (no NULL), chequear*/
  Object* cursor = treeRoot;
  Object* parent = NULL;
  while(cursor != NULL){
    if(cursor->left != NULL){
      if(num == (cursor->left->number)){
        parent = cursor;
        break;
      }
    }
    if(cursor->right != NULL){
      if(num == (cursor->right->number)){
        parent = cursor;
        break;
      }
    }
    if(num < cursor->number){
      cursor = cursor->left;
    }
    else {
      cursor = cursor->right;
    }
  }
  return parent;
}

Object* BST_searchMinValue (Object* obj){
  Object* min = NULL;
  if(obj == NULL){
    fprintf (stderr, "Error with BST_getMinValue. Object argument is NULL.\n");
  }
  /*Si el nodo no tiene hijo iquierdo, tiene el menor valor en el ABB*/
  else if (obj->left == NULL){
    min = obj;
  }
  /*Mientras existen nodos con hijos izquierdos, seguir buscando*/
  else {
    min = BST_searchMinValue(obj->left);
  }
  return min;
}

Object* BST_searchMaxValue (Object* obj){
  Object* max = NULL;
  //Llenar con su código
  if(obj == NULL){
      fprintf (stderr, "Error with BST_getMaxValue. Object argument is NULL.\n");
  }
  // si el nodo no tiene hijo derecho, tiene el mayor valor en ABB
  else if(obj->right == NULL){
      max = obj;
  }
  // si existe nodo derecho, seguir buscando
  else {
      max = BST_searchMaxValue(obj->right);
  }
  return max;
}

Object* BST_searchSuccessor (Object* obj, Object* treeRoot){
  // sucesor de nodo x: es el nodo con el valor minimo mas grande que el del nodo x en el arbol
  Object* successor = NULL;
  if(obj->right != NULL){ //
    successor = BST_searchMinValue(obj->right);
  }

  else {
    //Llenar con su código
    // si el obj es el mayor num -> retorna null pq no tiene sucesor
    if(obj->number == BST_searchMaxValue(treeRoot)->number){
        return NULL;
    }
    // IDEA: subir y preguntar si tiene hijo derecho y si tiene, recurcion
    Object *parent = BST_searchParent(treeRoot,obj->number);
    while(parent->number < obj->number){ // busco hasta que encuentro un parent mayor
        parent = BST_searchParent(treeRoot,parent->number);
    }
    successor = parent;
    }
  return successor;
}

void BST_traverseInOrder(Object* obj){
  if(obj != NULL){
    BST_traverseInOrder(obj->left);
    printf("Num: %d\n", obj->number);
    BST_traverseInOrder(obj->right);
  }
}

void BST_traversePreOrder(Object* obj){
  //Llenar con su código
  if(obj != NULL){
      printf("Num: %d\n",obj->number);
      BST_traversePreOrder(obj->left);
      BST_traversePreOrder(obj->right);
  }
}

void BST_traversePostOrder(Object* obj){
  //Llenar con su código
  if(obj){
      BST_traversePostOrder(obj->left);
      printf("Num: %d\n",obj->number);
  }
}

void BST_deleteTree(Object* obj){
  // Aquí los nodos del ABB se visitan en post-orden
  if(obj != NULL){
    BST_deleteTree(obj->left);
    BST_deleteTree(obj->right);
    free(obj);
  }
  return;
}


