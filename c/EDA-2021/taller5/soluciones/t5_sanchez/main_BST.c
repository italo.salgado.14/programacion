/*
 * @file    : main_BST.c
 * @author	: Ioannis Vourkas
 * @date	: 09/06/2020
 * @brief   : Template de código sobre ABB para Taller 04 - ELO 320
 */

#include <stdlib.h>
#include <stdio.h>

#include "myBST.h"

int main (){
    Object* BST_root = NULL; //Puntero al nodo raíz del ABB
    Object* temp = NULL;     //Puntero auxiliar
  
  /*EJEMPLOS de uso de las funciones disponibles*/
    
  /*Crear el ABB mostraro el la diapositiva 25: Clase 10*/
  BST_insertObject(&BST_root,15);
  
  BST_insertObject(&BST_root,18);
  BST_insertObject(&BST_root,17);
  BST_insertObject(&BST_root,20);
  BST_insertObject(&BST_root,19);
  BST_insertObject(&BST_root,60);

  
  BST_insertObject(&BST_root, 6);
  BST_insertObject(&BST_root, 3);
  BST_insertObject(&BST_root, 2);
  BST_insertObject(&BST_root, 4);
  
  BST_insertObject(&BST_root, 7);
  BST_insertObject(&BST_root,13);
  BST_insertObject(&BST_root, 9);
  BST_insertObject(&BST_root,14);
  
  /*Intentar ingreasar valor repetido*/
  BST_insertObject(&BST_root, 9);
  puts("\n");
  
  /*Buscar un número en el árbol*/
  int num = 13;
  temp = BST_searchObject(BST_root, num);
  if(temp != NULL){
    printf("Se encontro %d en el ABB!\n", temp->number);
  } else {
    printf("No existe el %d en el ABB!\n", num);
  }
  puts("\n");
  
  /*Buscar el nodo con el menor valor*/
  temp = BST_searchMinValue(BST_root);
  printf("El menor valor en el ABB es el %d\n", temp->number);

  /*Buscar el nodo con el mayor valor*/
  temp = BST_searchMaxValue(BST_root);
  printf("El mayor valor en el ABB es el %d\n", temp->number);


    BST_traverseInOrder(BST_root);


  num = 4;
  temp = BST_searchParent (BST_root, num);
  if (temp != NULL){
    printf("El nodo padre del nodo con valor %d tiene valor %d\n", num, temp->number);
  }
  else {
    printf("El nodo padre del nodo con valor %d es NULL\n", num);
  }


  num = 60;
  temp = BST_searchObject(BST_root, num);
  temp = BST_searchSuccessor (temp, BST_root);
  if (temp != NULL){
    printf("El nodo sucesor del nodo con valor %d tiene valor %d\n", num, temp->number);
  }
  else {
    printf("El nodo sucesor del nodo con valor %d es NULL\n", num);
  }

  // max value prueba
    printf("\nmin value partiendo de 6\n");
    temp = BST_searchMinValue(BST_searchObject(BST_root,6));
    printf("valor min: %d \n", temp->number);


    // prueba de remove
    printf("\nTraverse in ordeer\n");
    BST_traverseInOrder(BST_root);

    printf("\ntraverse pre order\n");
    BST_traversePreOrder(BST_root);

    printf("\ntraverse post order\n");
    BST_traversePostOrder(BST_root);


    printf("\nremove 20...\n");
    BST_removeObject(&BST_root,20);
    BST_traverseInOrder(BST_root);

    printf("\nremove 3...\n");
    BST_removeObject(&BST_root,3);
    BST_traverseInOrder(BST_root);

    printf("\nremove 18...\n");
    BST_removeObject(&BST_root,18);
    BST_traverseInOrder(BST_root);

    printf("\nremove 9...\n");
    BST_removeObject(&BST_root,9);
    BST_traverseInOrder(BST_root);

  BST_deleteTree(BST_root);
  
  return EXIT_SUCCESS;
}

