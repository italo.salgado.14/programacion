Nombre: Italo Salgado
ROL: 201473051-9

Compilacion:
>> gcc  -o prog main.c
>> ./prog

Errata: siempre leerá un archivo llamado: notas-EDA-C1.txt

Etapas:
S1: Primera sección. Corroborar que se traduce. Corroborar que la tabla se crea.

S2: Lectura del archivo de datos. Se muestra el dato, la encriptación y la desencriptación.

S3: Se muestra la creación de la lista enlasada. Creacion de la lista leyendo el archivo con Tokens.

S4: se printean los datos pedidos en el enunciado.

S5: El arbol ABB. El objetivo es transformar los apellidos a numeros, e ingresar con ese numero el dato
a el arbol binario. Esta sección estará comentada, dadas las reglas sugeridas. Si la pregunta no esta
terminada debe ir comentada.

Avance:
P1 Completa
P2 Completa
P3 Parcial.