//
// Created by laila on 25-06-21.
//
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include "stack.h"

#define NLEN 30

//agrega el elemento item al stack st
int push(Stack* st, char* nombre, char* apellido, int notas){
    int status = 0;
    if(isFull(st)){ //corroborar que no este lleno
        //expulsaremos un mensaje de error (fprintf) que dira que no se logro
        fprintf (stderr, "Could not push item. Stack is Full.\n");
    }
    else{

        //rellenamos los datos correspodnientes
        Student* Item = (Student*) malloc(sizeof(Student));
        strcpy(Item->nombre, nombre);
        strcpy(Item->apellido, apellido);
        Item->nota = notas;

        if (isEmpty(st)){
            st->TopStudent = Item;
            st->TopStudent->next = NULL;
            st->TopStudent->previous = NULL;

        }
        else{
            st->TopStudent->next = Item; //ahora el ultimo sera el Item creado
            Item->previous = st->TopStudent; //El item creado tendra como previo el que antes era el top
            st->TopStudent = Item;  //ahora el stack apunta al Item

        }

        st->top++; //el top avanza uno en su tamaño
        status = 1; //todo salio ok

    }
    return status;
}

//quita el primero elemento del stack y returna puntero donde este
Student* pop(Stack* st){
    Student* result;
    if(isEmpty(st)){ //si esta vacio el stack retornamos
        result = NULL; //un numero gigante negativo predefinido
    }
    else{
        if (st->top == 1){ //es el ultimo de la lista
            result = st->TopStudent;
            st->TopStudent = NULL; //no liberamos el objeto

            st->top = st->top - 1;
        }
        else{
            result = st->TopStudent; //retornamos el valor actual del top
            st->TopStudent = st->TopStudent->previous; //hacemos retroceder el top
            st->top = st->top - 1;

            st->TopStudent->next = NULL; //no existe siguiente porque lo quitamos
            result->previous = NULL; //result ya no tienen ningun nexo con la lista
        }


    }
    return result; //el elemento no es free(), se entrega un puntero a él
}

//indica si el stack st está lleno
int isFull (Stack* st){
    int result = 0;
    // Control de puntero NULL
    if(st == NULL){
        fprintf (stderr, "NULL pointer passed to isFull().\n");
    }
    else{
        if( st->capacity <= st->top ){ //si sobrepasamos la capacidad de el stack
            result = 1; //esta lleno
        }
        //else st->capacity > st->top podremos ingresar el dato
    }
    return result;
}

//indica si el stack st está vacio
int isEmpty(Stack* st){
    int result = 0;
    if(st == NULL){ //control
        fprintf (stderr, "NULL pointer passed to isEmpty().\n");
        result = 1;
    }
    else{
        //como en la teoria, se hace apuntar a -1 el puntero si no tenemos ningún
        //elemento
        if(st->top == 0){
            result = 1; //avisamos que esta vacio el stack
        }
    }
    return result;
}


//resetea el stack st al estado inicial
Stack* getNewStack (unsigned int size){
    Stack* newStack = NULL; //instanciamos un puntero para el nuevo stack
    if (size > 0){ //Procedemos solo si size > 0
        newStack = (Stack*) malloc(sizeof(Stack) + size*sizeof(int));

        if(newStack == NULL){ //Control si malloc no funcionó
            fprintf (stderr, "Error creating a new Stack with malloc.\n");
        }
        else{
            newStack->top = 0; //inicializamos el stack sin que tenga nada -> -1
            newStack->capacity = size; //la capacidad será size, para siempre
            newStack->TopStudent = NULL; //nadie es el top
        }
    }

    return newStack; //retornamos el puntero al stack con la memoria ya reservada
}


//elimina el stack y libera la memoria reservada
int stackDelete(Stack** st){
    int status = 0;
    Student* aux;
    if(*st == NULL){ //control
        fprintf (stderr, "NULL pointer passed to stackDelete().\n");
    }
    else{
        while( (*st)->TopStudent != NULL){ //free de cada Student
            aux = (*st)->TopStudent->previous;
            free((*st)->TopStudent);
            (*st)->TopStudent = aux;
        }

        free(*st); //free del stack

        status = 1; //todo ok. Puede reutilizar/elimnar el puntero
    }
    return status;
}

