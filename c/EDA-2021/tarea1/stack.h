//
// Created by laila on 25-06-21.
//

#ifndef TAREA1_STACK_H
#define TAREA1_STACK_H

#define NLEN 30


typedef struct node {
    char nombre[NLEN];
    char apellido[NLEN];
    int nota;
    struct node* previous;
    struct node* next;
} Student;

typedef struct St {
    int top; //el size
    unsigned int capacity;
    Student* TopStudent;
} Stack;

/**
 * @brief   : indica si el stack st está lleno
 * @param st: dirección de struct Stack que maneja el Stack
 * @return  : retorna 0 para false y 1 para true
 */
int isFull (Stack* st);


/**
 * @brief   : indica si el stack st está vacio
 * @param st: dirección de struct Stack que maneja el Stack
 * @return  : retorna 0 para false y 1 para true
 */
int isEmpty(Stack* st);


/**
 * @brief       : genera un nuevo stack de capacidad size
 * @param size  : el valor correspondiente a la capacidad del Stack
 * @return      : el puntero al Struct que maneja el Stack, o NULL si algo falló
 */
Stack* getNewStack (unsigned int size);


/**
 * @brief   : elimina el stack y libera la memoria reservada
 * @param st: dirección de struct Stack que maneja el Stack a eliminar
 * @return  : retorna 0 si el stack indicado no existe (NULL) o 1 en c.o.c.
 */
int stackDelete(Stack** st);

/**
 * @brief       : agrega el elemento item al stack st
 * @param st    : dirección de struct Stack que maneja el Stack
 * @param item  : el número a agregar al stack
 * @return      : retorna 0 si algo falló, o 1 en c.o.c.
 */
int push(Stack* st, char* nombre, char* apellido, int notas);

/**
 * @brief   : quita el primero elemento del stack.
 * @param st: dirección de struct Stack que maneja el Stack.
 * @return  : retorna objeto Student con valores del pop. (y elminiando lo que popie, evidentemente )
 */
Student* pop(Stack* st);


/*************************************************/




#endif //TAREA1_STACK_H
