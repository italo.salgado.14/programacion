#ifndef MYBST_H
#define MYBST_H


//Estructura que describe los nodos de un ABB
typedef struct Node{
    unsigned long number;
    Student* NodeStudent; //aqui dejamos el alumno asociado a el number transformado por la tabla hash
    struct Node* left;
    struct Node* right;
}Object;

/**
 * @brief            : crea un nuevo objeto con los datos pasados como argumentos
 * @param num        : número a ingresar a la estructura
 * @param leftObject : el valor a asignar al puntero left del nuevo objeto
 * @param rightObject: el valor a asignar al puntero right del nuevo objeto
 * @return           : retorna un puntero al nuevo objeto creado o NULL si no se pudo crear
 */
Object* getNewObjectABB ( unsigned long num, Object* leftObject, Object* rightObject, Student* NodeStudent);

/**
 * @brief           : agrega un nuevo objeto con número num al ABB que empieza en *objectRef
 * @param objectRef : la dirección del puntero a la raíz del subarbol ABB donde agregar el num
 * @param num       : el número a ingresar al nuevo objeto que entra al ABB
 * @return          : retorna el puntero al nuevo objeto, o NULL si no se pudo crear
 */
Object* BST_insertObject (Object** objectRef,  unsigned long num, Student* NodeStudent);

/**
 * @brief           : quita el nodo con numero num del ABB
 * @param objectRef : la dirección del puntero donde empieza el ABB
 * @param num       : el numero del nodo a eliminar del ABB
 * @return          : el puntero al nodo removido del ABB
 */
Object* BST_removeObject (Object** objectRef, unsigned long num);

/**
 * @brief       : busca un objeto en el ABB
 * @param obj   : el puntero al nodo donde empieza el ABB
 * @param num   : el número del nodo a buscar
 * @return      : el puntero an nodo en el ABB que tiene el valor num, o NULL si no existe
 */
Object* BST_searchObject (Object* obj, unsigned long num);

/**
 * @brief           : busca en nodo padre del nodo con numero num en el ABB que empieza en treeRoot
 * @param treeRoot  : el puntero al nodo donde empieza el ABB
 * @param num       : el numero del nodo cuyo nodo padre estamos buscando
 * @return          : el puntero al nodo padre, o NULL si el nodo no existe o si se trata del padre del nodo raiz
 */
Object* BST_searchParent (Object* treeRoot, unsigned long num);

/**
 * @brief       : busca el nodo con el valor mínimo en el ABB
 * @param obj   : el puntero al nodo donde empieza el ABB
 * @return      : retorna el puntero al nodo con el valor mínimo en el ABB, o NULL si el ABB no existe
 */
Object* BST_searchMinValue (Object* obj);

/**
 * @brief       : busca el nodo con el valor maximo en el ABB
 * @param obj   : el puntero al nodo donde empieza el ABB
 * @return      : retorna el puntero al nodo con el valor maximo en el ABB, o NULL si el ABB no existe
 */
Object* BST_searchMaxValue (Object* obj);

/**
 * @brief           : busca en nodo sucesor del nodo obj en el ABB que empieza en treeRoot
 * @param obj       : el nodo cuyo nodo sucesor estamos buscando
 * @param treeRoot  : el nodo raiz del ABB
 * @return          : el puntero al nodo sucesor, o NULL cuando este no existe
 */
Object* BST_searchSuccessor (Object* obj, Object* treeRoot);

/**
 * @brief       : recorrer el ABB en orden y imprimir los valores
 * @param obj   : el puntero al nodo donde empieza el ABB
 */
void BST_traverseInOrder(Object* obj);

/**
 * @brief
 * @param obj
 */
void BST_traversePreOrder(Object* obj);

/**
 * @brief
 * @param obj
 */
void BST_traversePostOrder(Object* obj);

/**
 * @brief       : elimina el arbol completo
 * @param obj   : el puntero al nodo donde empieza el ABB
 */
void BST_deleteTree(Object* obj);

/*Agregar funciones para:
 * - Eliminar el ABB usando BST_removeObject de forma iterativa
 * - Recorrer de minimo a maximo usando BST_searchMinValue y luego BST_searchSuccessor
 * - Encontrar el antecesor: BST_searchPredeccessor
 * - Recorrer de maximo a mínimo usando BST_searchMaxValue y luego BST_searchPredeccessor*/

#endif	// MYBST_H
