#include <stdio.h>
#include <stdlib.h>
#include "myABB.h"

//crea un nuevo objeto con los datos pasados como argumentos
Object* getNewObjectABB ( unsigned long num, Object* leftObject, Object* rightObject, Student* NodeStudent) {
    Object* new_object = (Object*) malloc(sizeof (Object)); //un nuevo nodo
    if (new_object == NULL) { //si NO creamos el nodo correctamente
        fprintf (stderr, "Error creating a new object with malloc.\n");
    }
    else {
        /*Actualizo el dato y los punteros left/right del nuevo objeto*/
        new_object->number = num; //su dato asociado
        new_object->left = leftObject;
        new_object->right = rightObject;
        new_object->NodeStudent = NodeStudent;
    }

    return new_object;
}

//agrega un nuevo objeto con número num al ABB que empieza en *objectRef
Object* BST_insertObject (Object** objectRef,  unsigned long num, Student* NodeStudent){
    Object* newObject = *objectRef; //creamos un nuevo nodo

    //si hemos llegado al nodo donde vamos a ingresar el dato
    if(*objectRef == NULL){
        //creamos un nodo, guardamos su dirección en la direccion de el nodo vacio detectado
        *objectRef = getNewObjectABB(num, NULL, NULL, NodeStudent);
        newObject = *objectRef; //hacemos que el puntero creado apunte a el objeto creado
    }

        //si el num es menor a el valor del nodo actual
    else if(num < (*objectRef)->number){
        newObject = BST_insertObject(&((*objectRef)->left), num, NodeStudent); //nos vamos a la isquierda
    }

        //si el num es mayor a el valor del nodo actual
    else if(num > (*objectRef)->number){
        newObject = BST_insertObject(&((*objectRef)->right), num, NodeStudent);
    }

    else {  // es to es como  if(num == (*objectRef)->number)
        /*Avisar cuando se intenta agregar un valor repetido y retornar NULL*/
        fprintf (stderr, "Error with insertion. Number %lu already in ABB.\n", num);
        newObject = NULL;
    }
    return newObject;
}

/*------------------Auxiliar de BST_removeObject-----------*/
Object* find_min(Object* node){
    if (node == NULL){
        return NULL;
    }
    else if (node->left != NULL){ //nodo con el valor minimo no tendra un hijo isquierdo
        return find_min(node->left);
    }
    return node;
}

//quita el nodo con numero num del ABB
Object* BST_removeObject (Object** objectRef, unsigned long num){
    //Llenar con su código
    //CORROBORAR QUE NO ESTE VACIO
    if ( (*objectRef) == NULL){
        return NULL; //no tiene sentido eliminar algo: retornamos
    }

    //BUSQUEDA
    if ( num > (*objectRef)->number ){ //si num es mayor al nodo actual
        (*objectRef)->left = BST_removeObject( &(*objectRef)->left, num ); //nos vamos a buscar un menor
    }
    else if ( num < (*objectRef)->number ){ //si num es menor al nodo actual
        (*objectRef)->right = BST_removeObject( &(*objectRef)->right, num ); //nos vamos a buscar un mayor
    }
    else{ //ENCUENTRO EL NODO
        //el nodo no tiene hijos
        if ( (*objectRef)->left == NULL && (*objectRef)->right == NULL ){
            free( (*objectRef) );
            return NULL;
        }
            //el nodo tiene solo un hijo: el derecho
        else if ( (*objectRef)->left == NULL && (*objectRef)->right != NULL ){
            Object* temp = NULL;
            temp = (*objectRef)->right;
            free( (*objectRef) );
            return temp;
        }
            //el nodo tiene un solo hijo: el isquierdo
        else if ( (*objectRef)->left != NULL && (*objectRef)->right == NULL ){
            Object* temp = NULL;
            temp = (*objectRef)->left;
            free( (*objectRef) );
            return temp;
        }
            //el nodo tiene los 2 hijos
        else {
            Object* temp = find_min( (*objectRef)->right );
            (*objectRef)->number = temp->number;
            (*objectRef)->right = BST_removeObject(&(*objectRef)->right, temp->number );
        }
    }

    return (*objectRef);
}
/*------------------Fin BST_removeObject-----------*/

//busca un objeto en el ABB
Object* BST_searchObject (Object* obj, unsigned long num){
    /*Mientras encuentro objetos validos (no NULL), chequear*/
    while(obj != NULL){
        if(num == obj->number){ //si el num es igual al numero guardado en el nodo actual
            break; //salimos del if
        }
        if(num < obj->number){ //si el num es menor a el numero guardado en el nodo actual
            obj = obj->left; //vamos a la isquierda
        }
        else { //if(num > obj->number)
            obj = obj->right; //vamos la derecha
        }
    }
    return obj;//retornamos el puntero en la posición final saliendo del break
}

//busca el nodo padre del nodo con numero num en el ABB que empieza en treeRoot
Object* BST_searchParent (Object* treeRoot, unsigned long num){
    /*Mientras encuentro objetos validos (no NULL), chequear*/
    Object* cursor = treeRoot;
    Object* parent = NULL;
    while(cursor != NULL){ //mientras el cursos pueda seguir avanzando
        if(cursor->left != NULL){ //si el nodo actual tiene un hijo isquierdo (menor)
            if(num == (cursor->left->number)){ // si el hijo isquierdo tiene el valor buscado
                parent = cursor; //retornamos ese cursor
                break;
            }
        }
        if(cursor->right != NULL){ //si el nodo actual tiene hijo derecho (mayor)
            if(num == (cursor->right->number)){ //si el hijo derecho tuviese el valor buscado
                parent = cursor; //retorna ese cursor
                break;
            }
        }
        if(num < cursor->number){ //si el numero que busco es menor a el nodo en el que estoy
            cursor = cursor->left; //avanzo a la isquierda
        }
        else { //si el numero que busco es mayor a el numero de el nodo en el que estoy
            cursor = cursor->right; //avanzo a la derecha
        }
    }
    return parent;
}

//busca el nodo con el valor mínimo en el ABB
Object* BST_searchMinValue (Object* obj){
    Object* min = NULL;
    if(obj == NULL){
        fprintf (stderr, "Error with BST_getMinValue. Object argument is NULL.\n");
    }
        /*Si el nodo no tiene hijo iquierdo, tiene el menor valor en el ABB*/
    else if (obj->left == NULL){
        min = obj;
    }
        /*Mientras existen nodos con hijos izquierdos, seguir buscando*/
    else {
        min = BST_searchMinValue(obj->left);
    }
    return min;
}

/*---------Init-searchMax-----------------*/
Object* find_max(Object* node){
    if (node == NULL){
        return NULL;
    }
    else if (node->right != NULL){ //nodo con el valor minimo no tendra un hijo isquierdo
        return find_max(node->right);
    }
    return node;
}

//busca el nodo con el valor maximo en el ABB
Object* BST_searchMaxValue (Object* obj){
    Object* max = NULL;
    //Llenar con su código
    max = find_max(obj);

    return max;
}
/* ----------End-MaxValue------------------------ */

//busca en nodo sucesor del nodo obj en el ABB que empieza en treeRoot
//esta funcion se divide en dos casos marcados:
//el primero: cuando al nodo al quele buscamos el sucesor no tiene NULL en su hijo derecho. esto
//nos dice que allí estará si o si su sucesor, si lo posee. ese sucesor será el minimo que se encuentre
//en ese tree.
//el segundo: no tenemos hijo derecho. esto se traduce en buscar sistematicamente en los padres el
//sucesor, mientras el valor de mi nodo sea mayor al de los padres. El primer padre con valor mayor
//sera el sucesor.
Object* BST_searchSuccessor (Object* obj, Object* treeRoot){
    Object* successor = NULL;
    if(obj->right != NULL){ //si el hijo de la derecha no es NULL
        successor = BST_searchMinValue(obj->right);  //entonces si o si estará allI el succesor y sera el menor
    }
    else { //si, por otro lado, mi hijo derecho esta vacio, debemos buscarle
        //Llenar con su código
        if (obj->number == BST_searchMaxValue(treeRoot)->number){ //si obj es el mayor en el treeroot
            return NULL; //jodimos, nada que retornar
        }
        else{ //si obj no es el mayor, existira el sucesor en el treeroot
            Object* parent = BST_searchParent(treeRoot, obj->number); //buscamos el padre de obj
            while (parent->number < obj->number){ //mientras el padre sea menor que nuestro nodo
                parent = BST_searchParent(treeRoot, parent->number); //buscamos el padre del padre
            }
            successor = parent; //una vez lo encontramos, lo retornamos
        }
    }
    return successor;
}

//recorrer el ABB en orden y imprimir los valores
void BST_traverseInOrder(Object* obj){
    if(obj != NULL){
        BST_traverseInOrder(obj->left);
        printf("Num: %lu\n", obj->number);
        BST_traverseInOrder(obj->right);
    }
    return;
}


//elimina el arbol completo
void BST_deleteTree(Object* obj){
    // Aquí los nodos del ABB se visitan en post-orden
    if(obj != NULL){
        BST_deleteTree(obj->left);
        BST_deleteTree(obj->right);
        free(obj);
    }
}



