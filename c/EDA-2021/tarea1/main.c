/*
 * @file    : main.c
 * @author	: Italo Salgado
 * @date	: 21/06/2021
 * @brief   : Codigo main() de la tarea 1 de EDA 1-2021
 */


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include "stack.c"
#include "myABB.c"


/* -----------------THE DOCS---------------- */
/*
 * El codigo ASCII: https://elcodigoascii.com.ar/codigos-ascii/comillas-simples-apostrofes-codigo-ascii-39.html
 * Cambiar de lower a uppercase: https://www.geeksforgeeks.org/toupper-function-in-c/
 * Uso de strlen: https://www.geeksforgeeks.org/difference-strlen-sizeof-string-c-reviewed/
 * Leer archivos en C: https://www.learnc.net/c-tutorial/c-read-text-file/
 * Tokenisación: https://stackoverflow.com/questions/18421310/reading-a-file-line-by-line-and-splitting-the-string-into-tokens-in-c
 * /

/*------------- DEF GLOBALS---------------- */
#define stackSIZE 50

/*-------- THE FUNCTIONS---------------- */
void initCripto(char [2][26]);
void printCripto(char arr[2][26]);
void initEncryptionTable(char arr[2][26]);
char* encrypt(char* str, char table[2][26]);
char* decrypt(char* str, char table[2][26]);
void printear_aprobados_P2(Stack *stack, char crypt[2][26]);
unsigned long hash(unsigned char *str);


int main() {
    srand(time(NULL));   // Initialization, should only be called once.

    /* -----------THE TEST CODE------------- */
    printf("\n-------------S1------------------\n");

    char crypto[2][26]; //Filas, Columnas
    initEncryptionTable(crypto);
    printCripto(crypto);

    char* nombre = "Yorgos,Seferis,54\0";
    printf("%s\n", nombre);
    char* result = NULL;
    char* result2 = NULL;
    result = encrypt(nombre, crypto);
    printf("Mi palabra encriptada es: %s \n", result);
    result2 =  decrypt(result, crypto);
    printf("Mi palabra desencriptada es: %s \n", result2);
    free(result);
    free(result2);

    /*-------------- READ THE FILE----------------- */
    printf("\n-------------S2------------------\n");


    printf("La encriptacion del archivo sistematicamente es:");
    char *filename = "notas-EDA-C1.txt";
    FILE *fp = fopen(filename, "r");

    if (fp == NULL)
    {
        printf("Error: could not open file %s", filename);
        return 1;
    }

    // reading line by line, max 256 bytes
    const unsigned MAX_LENGTH = 256;
    char buffer[MAX_LENGTH];
    char *auxResult = (char *) malloc(256);

    while (fgets(buffer, MAX_LENGTH, fp)) {
        printf("%s ", buffer);
        auxResult = encrypt(buffer, crypto);
        printf("%s ", auxResult);
        auxResult = decrypt(auxResult, crypto);
        printf("%s ", auxResult);

    }

    free(auxResult);
    // close the file
    fclose(fp);

    /*----------------STACK----------------------*/
    printf("\n-------------S3------------------\n");

    //las pruebas básicas
    Stack* stack1 = getNewStack(stackSIZE);
    int flag = push(stack1, "italo", "salgado", 65 );
    flag = push(stack1, "Augustus", "Caesar", 97 );

    Student* act_stud = pop(stack1);
    printf("nombre: %s\n", act_stud->nombre);
    free(act_stud);



    // la construccion de la lista enlasada: releer el archivo
    Stack* stack2 = getNewStack(stackSIZE);

    //char *filename = "notas-EDA-C1.txt";
    fp = fopen(filename, "r");

    if (fp == NULL)
    {
        printf("Error: could not open file %s", filename);
        return 1;
    }

    char line[MAX_LENGTH], line2[MAX_LENGTH];

    printf("Los datos insertados en el stack son:\n");
    while (fgets(line, (int) MAX_LENGTH, fp)) {
        const char* val1 = strtok(line, ",");
        const char* val2 = strtok(NULL, ",");
        const char* val3 = strtok(NULL, ",");

        printf("%s - ", val1);
        printf("%s - ", val2);
        printf("%i - ", atoi(val3));
        printf("%s - ", encrypt( (char*) val1, crypto));
        printf("%s - ", encrypt( (char*) val2, crypto));
        printf("%i\n", atoi(val3));

        push(stack2, encrypt( (char*) val1, crypto), encrypt( (char*) val2, crypto),  atoi(val3)); //la lista enlasada con los datoa agregados
    }

    // close the file
    fclose(fp);

    printf("\n-------------S4------------------\n");
    printear_aprobados_P2(stack2, crypto);




    /*---------------ABB----------------------*/
    printf("\n-------------S5------------------\n");

    printf("Ver myABB. Aqui hay PseudoCodigo.\n");

    //el plan es traducir las palabras a numeros a traves de una tabla hash simplificada.
    //Asi podremos ubicar el nombre asociandolo a un valor. Vea myABB.h y myABB.c
    //Este approach es muy útil porque podemos reciclar el código del taller 5

    /*

     while ( i < num_datos_lista ){
        recorrer la lista doblemente enlasada segun i
        num_apellido = hash (apellido);
        BST_insertObject (myBST,  num_apellido, Actual_Student);
    }

     */

    //se elimina el stack en memoria, como se solicita en el enunciado
    stackDelete(&stack2);
    stackDelete(&stack1);
    /*--------------------- el fin--------------- */
    return 0;

}


/**
 * @brief       : encriptador
 * @param str : el texto de entrada a encriptar
 * @param table : la tabla con las encriptaciones
 * @return      : puntero donde estará guardado el resultado de la operación
 */
char* encrypt(char* str, char table[2][26]){

    //atención: no sabemos si el puntero es const o no.
    //entonces deberemos retornar la variable
    char* result = (char*) malloc(strlen(str) + 1);

    short int i = 0, j = 0;

    while ( i >= 0  &&  str[i] != '\0'){
        j = 0;
        //printf("letra procesada actual: %c\n", str[i]);
        while (j < 28){
            if ( toupper(str[i]) == table[0][j]){
                //printf("asignacion: %c,%c\n", table[0][j], str[i]);
                result[i] = table[1][j];
                j = 28; //salir, ya se ha encontrado
            }
            //printf("j es ahora: %u\n", j);
            j++;
        }
        if (j == 28){
            result[i] = str[i];
        }
        //printf("i es ahora: %u\n", i);
        i++;
    }
    result[strlen(str)] = '\0';
    return result;
}


/**
 * @brief       : desencriptar un string
 * @param str : string a desencriptar
 * @param table : tabla de encriptacion
 * @return      : retorna el número aleatorio en el rango especificado, o 0 si limit = 0
 */
char* decrypt(char* str, char table[2][26]){

    //atención: no sabemos si el puntero es const o no.
    //entonces deberemos retornar la variable
    //char* result = (char*) malloc(strlen(str));
    char* result = (char*) malloc(strlen(str) + 1);

    short int i = 0, j = 0;

    while ( i >= 0  &&  str[i] != '\0'){
        j = 0;
        //printf("letra procesada actual: %c\n", str[i]);
        while (j < 28){
            if ( toupper(str[i]) == table[1][j]){
                //printf("asignacion: %c,%c\n", table[0][j], str[i]);
                result[i] = table[0][j];
                j = 28; //salir, ya se ha encontrado
            }
            //printf("j es ahora: %u\n", j);
            j++;
        }
        if (j == 28){
            result[i] = str[i];
        }
        //printf("i es ahora: %u\n", i);
        i++;
    }
    result[strlen(str)] = '\0';
    return result;
}

/**
 * @brief       : genera un número entero aleatorio en el rango de 1 a limit
 * @param limit : el limite superior del rango de valores a crear aleatoriamente
 * @return      : retorna el número aleatorio en el rango especificado, o 0 si limit = 0
 */
static unsigned int getRandomNumber(unsigned int limit){
    unsigned int result = 0;
    if (limit != 0){
        result = rand() % limit + 1;
    }
    return result;
}


/**
 * @brief       : inicializador del array de 2x26
 * @param arr : Siempre un array de char de 2x26
 * @return      : NA
 */
void initEncryptionTable(char arr[2][26]){
    int i = 0;

    //rellenamos la seccion de letras constantes
    while ( i < 26 ){
        arr[0][i] =   (char) i + 17 +'0';
        i++;
    }

    //rellenamos la seccion de letras aleatorias
    i = 0;
    short int used_char[26]; //A,B,C,D,E,...,X,Y,Z

    //un arreglo que indica si el numero ya fue usado o no (esto usara memoria
    //pero ahorrará tiempo de busqueda de la letra
    while (i < 26){
        used_char[i] = 0; //0 al 25
        i++;
    }

    i = 0;
    while ( i < 26 ){
        int r = (int) getRandomNumber(25); //1 al 25
        while( (used_char[r] == 1) || (arr[0][i] == r) ){
            r = r+1;
            r = r%26;
        }

        arr[1][i] = (char) r + 17 + '0'; //aqui deberia ser +65
        used_char[r] = 1;

        i++;
    }
    //return;
}


/**
 * @brief       : printear el array traductor de encriptacion de 2x26
 * @param arr : Siempre un array de char de 2x26
 * @return      : NA
 */
void printCripto(char arr[2][26]){
    int i = 0;
    while (i < 26){
        printf("%c ", arr[0][i]);
        i++;
    }
    printf("\n");
    i = 0;
    while (i < 26){
        printf("%c ", arr[1][i]);
        i++;
    }
    printf("\n");
    //return;
}


/**
 * @brief       : printear alumnos aprobados. Luego, printear alumno con la mayor nota
 * @param stack : Puntero al stack
 * @param crypt : Matriz de encriptacion
 * @return      : NA
 */
void printear_aprobados_P2(Stack *stack, char crypt[2][26]){
    Student *actualTop;
    Student *best;
    actualTop = stack->TopStudent;
    best = actualTop;
    printf("Los alumnos que han aprobado son los siguientes:\n");
    while(actualTop != NULL){ //no sea el primero, buscamos
        if(actualTop->nota >54){ //un aprobado
            if(actualTop->nota >= best->nota){ //si es el mejor entre los mejores
                best = actualTop; //lo guardamos
            }
            printf("Name: %s-", decrypt(actualTop->nombre,crypt));
            printf("Apellido:%s-", decrypt(actualTop->apellido,crypt));
            printf("Nota: %d  -  ",actualTop->nota);
            printf("NameEnc:%s-", actualTop->nombre,crypt);
            printf("ApellidoEnc:%s-", actualTop->apellido,crypt);
            printf("NotaEnc:%d\n",actualTop->nota);
        }
        actualTop = actualTop->previous;
    }
    printf("Mejor Nota: %s %s, con un %d\n",
           decrypt(best->nombre,crypt), decrypt(best->apellido,crypt), best->nota);
}

/**
 * @brief       : tabla hash. De string a numeros
 * @param str : Un String de entrada
 * @return      : Un numero de salida a usar en la tabla
 */
unsigned long hash(unsigned char *str)
{
    unsigned long hash = 5381;
    int c;

    while (c = *str++)
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

    return hash;
}