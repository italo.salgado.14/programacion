#include <stdio.h>
#include <malloc.h>

int main() {

    float pi=3.1416;
    float* ppi = &pi;
    printf("direccion de pi: %u\n", ppi);
    printf("direccion de ppi: %u\n", &ppi);

    float* mpi=(float*) malloc (sizeof (float));
    *mpi = pi;
    printf("direccion de mpi: %u\n", mpi);
    free(mpi);

    return 0;
}
