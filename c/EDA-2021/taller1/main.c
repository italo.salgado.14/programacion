/* 
 * @file    : template_Taller1.c
 * @author  : Ioannis Vourkas
 * @date    : 23/04/2020
 * @brief   : Template de código para Taller 1 - ELO 320
 */

#include <stdio.h>
#include <stdlib.h>
#include "calculadora_Taller1.h"

#define SIZE 15

int array[SIZE];

int main(){
    //Mensaje de bienvenida
    puts("Inicio del programa de Taller 1\n");

    //instanciar variables
    float res_sum;
    float res_resta = 6;

    unsigned int op1 = 2, op2 = 3, res_multi = 0;
    unsigned int D = 9, d = 2, co, re;

    //operaciones
    sumar(3,3, &res_sum);
    restar(&res_resta, 3);
    multiplicar(op1, op2, &res_multi);
    dividir(D, d, &co, &re);

    /*Mostrar ejemplos de su implementación*/
    float f1, *pf = NULL;
    //printf("sumar() retornó %d\n",sumar(1.3, -4.5, pf));
    //printf("sumar() retornó %d\n",sumar(1.3, -4.5, &f1));
    //printf("El resultado de sumar(%f, %f) es %f\n",1.3, -4.5, f1);

    printf("restar() retornó %f\n", res_resta);
    printf("multiplicar() retornó %u\n", res_multi);
    printf("division() retornó %u, %u\n", co, re);
    //puts("-------------------------\n");

    initArray(array, SIZE, 500);
    printArray(array, SIZE);

    sortArray(array, SIZE);
    printArray(array, SIZE);

    int min, max;
    minmaxArray(array, SIZE, )

    //Mensaje de salida
    puts("Termino del programa de Taller 1");

    return 0;
}