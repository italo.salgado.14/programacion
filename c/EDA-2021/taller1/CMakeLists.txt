cmake_minimum_required(VERSION 3.19)
project(taller1 C)

set(CMAKE_C_STANDARD 99)

add_executable(taller1 main.c calculadora_Taller1.c calculadora_Taller1.h)