//
// Created by laila on 29-05-21.
//

/*
 * @file    : calculadora_Taller1.c
 * @author  : Ioannis Vourkas
 * @date    : 23/04/2020
 * @brief   : Template de código para Taller 1 - ELO 320
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "calculadora_Taller1.h"

//FUNCIONES EXTRAS

void swap(int *xp, int *yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

// A function to implement bubble sort
void bubbleSort(int arr[], int n)
{
    int i, j;
    for (i = 0; i < n-1; i++)

        // Last i elements are already in place
        for (j = 0; j < n-i-1; j++)
            if (arr[j] > arr[j+1])
                swap(&arr[j], &arr[j+1]);
}


//FUNCIONES PEDIDAS
int sumar( float op1,float op2, float *res){
    /*Ejemplo*/
    //if (res == NULL) return -1;

    *res = op1 + op2;

    return 0;
}
int usumar( unsigned int op1, unsigned int op2, unsigned int *res){
    /*Ejemplo*/
    //if (res == NULL) return -1;

    *res = op1 + op2;

    return 0;
}

int restar(float* minu, float subs){
    /*Llenar con su código*/
    *minu = *minu - subs;

    return 0;
}
int urestar(unsigned int* minu, unsigned int subs){
    /*Llenar con su código*/
    *minu = *minu - subs;

    return 0;
}

int multiplicar(unsigned int op1, unsigned int op2, unsigned int* res){
    /* Llenar con su código SIN usar op1*op2
     * En lugar de eso, utilice la función sumar()
     * para sumar op1 a si mismo op2 veces
     */
    unsigned int temp = 0;
    for (unsigned int i=0; i <= op1; i++){
        usumar( op1, temp, &temp);
    }

    *res = temp;
    return 0;
}

int dividir(unsigned int D, unsigned int d, unsigned int* co, unsigned int* re){
    /* Llenar con su código SIN usar D/d
     * En lugar de eso, utilice la función restar() para restar
     * d de D continuamente incrementando co hasta encontrar D < 0.
     * El resto de la división re es entonces D + d.
     */
    unsigned int temp = 0, result_resta;
    while (D >= d){
        temp++;
        result_resta = D;
        urestar(&result_resta, d);
        D = result_resta;
    }

    *co = temp;
    *re = D;

    return 0;
}

int initArray (int array[], unsigned int size, unsigned int limit){
    /* Llenar con su código. Leer aqui para uso de rand()
     * https://www.cplusplus.com/reference/cstdlib/rand/
     */
    srand (time(NULL));
    int v1 = rand() % limit;

    for (int i=0; i < size; i++){
        array[i]=v1;
        v1 = rand() % limit;
    }

    return 0;
}

int sortArray (int array[], unsigned int size){
    /* Llenar con su código. Busque en WWW
     * por el algoritmo "Bubble sort"
     */
    bubbleSort(array, (int)size);
    return 0;
}

int printArray (int array[], unsigned int size){
    /*Llenar con su código*/
    printf("Array:");
    for (int i=0; i < size; i++){
        printf("-%i", array[i]);
    }
    printf("\n");

    return 0;
}

int copyArray(const int src[], int dst[], unsigned int size){
    /*Llenar con su código*/
    for (int i=0; i < size; i++){
        dst[i] = src[i];
    }

    return 0;
}

int minmaxArray (int array[], unsigned int size, int* min, int* max){
    /*Llenar con su código. Aprovechen de la funcion sortArray()
     * Si ordenan el array, el valor minimo queda en posicion [0]
     * y el valor máximo queda en posicion [size-1]. Use un arreglo
     * local tempArray para este trabajo, SIN ordenar el arreglo array.
     */
    int temp[size];
    copyArray(array, temp, size);
    sortArray(temp, size);
    temp[0] = *min;
    temp[size] = *max;
    return 0;
}

