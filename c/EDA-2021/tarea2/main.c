/*
 * Italo Saqlgado
 * V1.0
 * 24/jul/21
 *
 * Nota 1: el programa no tiene ennes ni tildes a drede.
 * Nota 2: explicacion tarea https://www.youtube.com/watch?v=hkSrXPOr0IM
 *
 * ETAPA 1
 *  Arreglos dinamicos en C:
 * https://www.delftstack.com/es/howto/c/c-array-of-structs/
 * http://www.mathcs.emory.edu/~cheung/Courses/255/Syllabus/2-C-adv-data/dyn-array.html
 *
 * Los numeros aleatorios en C:
 * https://www.aprenderaprogramar.com/index.php?option=com_content&view=article&id=917:generar-numeros-o-secuencias-aleatorios-en-c-intervalos-srand-y-rand-time-null-randmax-cu00525f&catid=82&Itemid=210
 *
 * ETAPA2:
 * Uso de scanf para obtener mas de 1 parametro
 * https://stackoverflow.com/questions/1412513/getting-multiple-values-with-scanf
 *
 * Referencia para Heap:
 * https://gist.github.com/nachinius/490c1561a5bfe015ff18d8d4ac22cb84
 * https://d-michail.github.io/assets/teaching/data-structures/033_BinaryHeapImplementation.en.pdf
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "heapify.c"

struct node {
    int data;
    int left;
    int right;
};


void reset(struct node* myPointer) {
    if (myPointer) {
        free(myPointer);
        myPointer = NULL;
    }
}


int main(int argc, char** argv)
{
    /*--------------------- ETAPA 1-------------------- */
    //reservar la memoria dinamicamente
    int number_of_numbers = 1000;
    struct node* myArray = malloc(number_of_numbers * sizeof(struct node));

    //generacion del numero aleatorio
    srand (time(NULL));
    int N = 1000000; //desde 0 hasta N se generara el numero
    int random_number = rand() % (N+1);

    //rellenar los datos correspondientes
    int counter = 0;
    for (counter = 0; counter < number_of_numbers; counter++){
        myArray[counter].data = random_number;
        myArray[counter].left = -1;
        myArray[counter].right = -1;
        random_number = rand() % (N+1);
    }

    //printear los numeros generados aleatoriamente
    //for (counter = 0; counter < number_of_numbers; counter++){
     //   printf("datos (%i, %i, %i) \n", myArray[counter].myArray, myArray[counter].left, myArray[counter].right);
    //}

    /*----------Etapa 2-----------*/

    //captura de los subelementos de interés,
    int min1, max1, min2, max2, out = 0;
    printf("Ingrese los 4 numeros separados por espacios:");
    scanf( "%i %i %i %i", &min1, &max1, &min2, &max2);

    //identificar 2 zonas dentro del arreglo original
    while(out == 0){
        if( (0 <= min1) && min1 < max1 && max1 < min2 && min2 < max2 && (max2 <= 999)){
            out = 1;
        }
        else{
            printf("No ha ingresado correctamente los parámetros. Reintentelo:");
            scanf( "%i %i %i %i", &min1, &max1, &min2, &max2);
        }
    }


    //borrar la memoria reservada
    reset(myArray);
    return (0);
}
