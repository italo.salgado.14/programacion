//
// Created by laila on 25-07-21.
//

/*
 * COMENTARIOS
 * - Cuidado al usar en otro lugar la variable minheap, puede dejar la tendalada de errores
 */

#ifndef HEAP_EXAMPLE_HEAPIFY_H
#define HEAP_EXAMPLE_HEAPIFY_H

/* def de los tipos */
typedef int key_type;
typedef struct minheap* minheap;

/*funciones a definir*/
/**
 * @brief       : instancia un minheap, de tamano maximo 64
 * @param arr : NA
 * @return      : NA
 */
minheap minheap_create();


/**
 * @brief       : printear el array traductor de encriptacion de 2x26
 * @param arr : Siempre un array de char de 2x26
 * @return      : NA
 */
minheap minheap_heapify(const key_type* array, int n);

/**
 * @brief       : printear el array traductor de encriptacion de 2x26
 * @param arr : Siempre un array de char de 2x26
 * @return      : NA
 */
void minheap_destroy(minheap);

/**
 * @brief       : printear el array traductor de encriptacion de 2x26
 * @param arr : Siempre un array de char de 2x26
 * @return      : NA
 */
int minheap_findmin(minheap);

/**
 * @brief       : printear el array traductor de encriptacion de 2x26
 * @param arr : Siempre un array de char de 2x26
 * @return      : NA
 */
void minheap_insert(minheap, key_type);

/**
 * @brief       : printear el array traductor de encriptacion de 2x26
 * @param arr : Siempre un array de char de 2x26
 * @return      : NA
 */
void minheap_deletemin(minheap);

/**
 * @brief       : printear el array traductor de encriptacion de 2x26
 * @param arr : Siempre un array de char de 2x26
 * @return      : NA
 */
int minheap_is_empty(minheap);

/**
 * @brief       : printear el array traductor de encriptacion de 2x26
 * @param arr : Siempre un array de char de 2x26
 * @return      : NA
 */
int minheap_size(minheap);

/**
 * @brief       : printear el array traductor de encriptacion de 2x26
 * @param arr : Siempre un array de char de 2x26
 * @return      : NA
 */
void minheap_clear(minheap);

/**
 * @brief       : printear el array traductor de encriptacion de 2x26
 * @param arr : Siempre un array de char de 2x26
 * @return      : NA
 */
void heapsort(int *array, int n);

#endif //HEAP_EXAMPLE_HEAPIFY_H
