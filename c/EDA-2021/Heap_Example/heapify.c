//
// Created by laila on 25-07-21.
//
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "heapify.h"

/*
 * El tipo de dato minheap, que contiene los parametros y la informacion del heap
 */
struct minheap {
    key_type* array; //array is the array for the keys, un (int*)
    int max_size; //max_size+1 is the array size
    int cur_size; //cur_size is the position of the last array element which is used
};


minheap minheap_create() {
    minheap h = (minheap) malloc(sizeof(struct minheap));
    if (h == NULL) { //existe suficiente memoria?
        fprintf(stderr, "Not enough memory!\n");
        abort();
    }
    h->max_size = 64;
    h->cur_size = 0; //posicion del ultimo elemento usado, es un cursor
    h->array = (key_type*) malloc( sizeof(key_type)*(h->max_size+1)); //el tamano es (num_entradas + 1)
    if (h->array == NULL) { //existe suficiente memoria?
        fprintf(stderr, "Not enough memory!\n");
        abort();
    }

    return h;
}


void minheap_destroy(minheap h) {
    assert(h); //control
    free(h->array);
    free(h);
}

static void minheap_double_capacity(minheap h) {
    // create double the array
    int new_max_size = 2 * h->max_size;
    key_type* new_array = (key_type*) malloc( sizeof(key_type)*(new_max_size+1)); //crear nuevo array
    if (new_array == NULL) { //existe suficiente memoria?
        fprintf(stderr, "Not enough memory!\n");
        abort();
    }
    /* copy old elements to new array */
    for(int i = 1; i <= h->cur_size; i++) {
        new_array[i] = h->array[i];
    }
    /* free old array and place new in position */
    free(h->array);
    h->array = new_array;
    h->max_size = new_max_size;
}


/*
 * el clasico swap de toda la vida
 */
static void minheap_swap(minheap h, int i, int j) {
    assert (h && i >=1 && i <= h->cur_size && j >= 1 && j <= h->cur_size); //todo ok?
    key_type tmp = h->array[i];
    h->array[i] = h->array[j]; //todo ok, estos son punteros
    h->array[j] = tmp;
}

/*
 * Las dos funciones principales para realizar un minHeapify
 *
 * RECUERDE: if "yo soy k"
 * my parent is k/2,
 * nodo de la isquierda es 2k,
 * nodo de la derecha es 2k+1
 */
static void minheap_fixup(minheap h, int k) {
    assert(h && k >= 1 && k <= h->cur_size);
    while (k>1 && h->array[k] < h->array[k/2]) { //mientras yo(k) <= mi padre(k)
        minheap_swap(h, k/2, k); //lo intercambiamos por el padre
        k /= 2; //k = k/2 : ahora lo ejecutamos en el padre(k)
    }
}
static void minheap_fixdown(minheap h, int k) {
    assert(h);
    while (2*k <= h->cur_size) { //mientras mi nodo isq (2k) <= cursorHeap
        int j = 2*k; //guardamos al nodo isquierdo (2k) en j
        if (j < h->cur_size && h->array[j+1] < h->array[j]) //si hijoisq < cursorH y hijoder < hijoisq (o sea si el arbol esta mal como minHeap)
            j++; //voy al hijo de la derecha
        if (h->array[k] <= h->array[j]) //si yo nodo(k) <= hijoder nodo(2k +1)
            break; //salimos del while
        minheap_swap(h, k, j);
        k = j; //ahora "yo" soy el nodo hijoder (2k+1)
    }
}


void minheap_insert(minheap h, key_type key) {
    assert(h);
    // make sure there is space
    if (h->cur_size == h->max_size) //si no existe capacidad, doblamos el largo del array dinámicamente
        minheap_double_capacity(h);
    // add at the bottom, as a leaf
    h->array[++h->cur_size] = key;
    // fix its position
    minheap_fixup(h, h->cur_size);
}


int minheap_findmin(minheap h) {
    if (minheap_is_empty(h)) {
        fprintf(stderr, "Heap is empty!\n");
        abort();
    }
// min is always in first position
    return h->array[1];
}


void minheap_deletemin(minheap h) {
    if (minheap_is_empty(h)) {
        fprintf(stderr, "Heap is empty!\n");
        abort();
    }
    // swap first with last
    minheap_swap(h, 1, h->cur_size);
    // delete last
    h->cur_size--;
    // fixdown first
    minheap_fixdown(h, 1);
}


int minheap_size(minheap h) {
    assert(h);
    return h->cur_size;
}

int minheap_is_empty(minheap h) {
    assert(h);
    return h->cur_size <= 0;
}


void minheap_clear(minheap h) {
    assert(h);
    h->cur_size = 0;
}


minheap minheap_heapify(const key_type* array, int n) {
    assert(array && n > 0);
    minheap h = (minheap) malloc(sizeof(struct minheap)); //reservar memoria para el heap
    if (h == NULL) { //memoria suficiente?
        fprintf(stderr, "Not enough memory!\n");
        abort();
    }
    h->max_size = n; //tamaño del heap creado
    h->cur_size = 0; //el cursor parte en el primer elemento
    h->array = (key_type*) malloc(sizeof(key_type)*(h->max_size+1)); //reservar mem para n elementos
    if (h->array == NULL) {
        fprintf(stderr, "Not enough memory!\n");
        abort();
    }
    h->cur_size = n; //ponemos el cursor en el elemento n (el ultimo)
    for(int k = 0; k < n; k++) //recorremos desde el 0 hasta el ultimo
        h->array[k+1] = array[k]; //guardar el elemento k+1 en mi array k (una copia total)
    for(int k = (h->max_size+1)/2; k > 0; k--) //procesar desde la mitad hacia adelante, o sea los nodos finales
        minheap_fixdown(h, k); //realizar fixdown, que realiza el foramto del minHeap para el nodo dado (tira hacia arriba los menores)
    return h;
}


void heapsort(int *array, int n) {
    minheap h = minheap_heapify(array, n);
    int i = 0;
    while(!minheap_is_empty(h)) {
        array[i++] = minheap_findmin(h);
        minheap_deletemin(h);
    }
    minheap_destroy(h);
}