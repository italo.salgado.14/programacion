#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "heapify.c"

#define SIZE 40
#define MAX_NUMBER 100

/*Funciones Auxiliares de Muestra*/
void print_hip(minheap h){
    for (int k=0; k < h->max_size; k++ ){
        printf("%i ",h->array[k]);
    }
    printf("\n");
}


int main(int argc, char **argv) {
    /*------------- primera parte----------------*/
    int i;
    srand(time(NULL));
    minheap h = minheap_create(); //crear minHeap estandar
    for(i = 0; i < 40; i++)
        minheap_insert(h, rand() % 1000); //inserto en el minHeap aleatoriamente 40 numeros

    //El minHeap esta armado. Corroboramos printenadolo
    print_hip(h);
    while(!minheap_is_empty(h)) {
        printf("%4d", minheap_findmin(h));
        minheap_deletemin(h);
    }
    minheap_destroy(h);

    /*-------------------- segunda parte --------------------*/
    printf("\n");

    //instanciar el arreglo dinámico
    srand(time(NULL));
    int array[SIZE];
    for(int i = 0; i < SIZE; i++) {
        array[i] = rand() % MAX_NUMBER;
    }

    //realizar algoritmo heapsort
    heapsort(array, SIZE);
    for(int i = 1; i < SIZE; i++) {
        assert(array[i - 1] <= array[i]);
        printf("%i ", array[i]);
    }

    return 0;

}