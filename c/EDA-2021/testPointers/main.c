#include <stdio.h>

int main() {

    unsigned int arr[5][2] = {{0,0}, {1,2}, {2,4}, {3,6}, {4,8}};
    unsigned int (*p)[2];
    p=arr;


    printf("--------PrimeraPrueba---------\n");
    printf("arr :%u\n", arr );
    printf("*arr :%u\n", *arr );
    printf("(*arr+1) :%u\n", (*arr+1) ); //avanza 4 bytes
    printf("(*arr+2) :%u\n", (*arr+2) ); //avanza 8 bytes
    printf("-------------1-------------\n");

    printf("arr+1 :%u\n", arr+1 ); //avanza 8 bytes
    printf("arr+2 :%u\n", arr+2 ); //avanza 16 bytes
    printf("*(arr+1)+1 :%u\n", *(arr+1)+1 ); //avanza 4+8 bytes
    printf("**arr :%u\n", **arr); //valor de la direccion arr[0,0]
    printf("-------------2-------------\n");

    printf("(**arr+1) :%u\n", (**arr+1)); //valor arr[0,0]+1
    printf("*(arr+1) :%u\n", *(arr+1) ); //avanza 8 bytes
    printf("*(arr+2) :%u\n", *(arr+2) ); //avanza 16 bytes
    printf("------------3--------------\n");

    printf("p :%u\n", p ); //igual a imprimir arr
    printf("*p :%u\n", *p ); //igual a imprimir p o arr


    printf("_____________OtraPrueba____________\n");
    int** ppA;
    int* pA;
    int A;

    A=3;
    pA = &A;
    ppA = &pA;

    printf("A: %u\n", A );
    printf("pA: %u\n", pA );
    printf("ppA: %u\n", ppA );

    printf("------------------------\n");

    printf("(*ppA): %u\n", (*ppA) );
    printf("(**ppA): %u\n", (**ppA) );


    return 0;
}
