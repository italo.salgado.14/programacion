/*
 * @file    : stack.c
 * @author	: Ioannis Vourkas
 * @date	: 28/05/2020
 * @brief   : Soluciones de código para Taller 3 - ELO 320
 */

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

#include "stack.h"

int isFull (Stack* st){
  int result = 0;
  // Control de puntero NULL
  if(st == NULL){
    fprintf (stderr, "NULL pointer passed to isFull().\n");
  }
  else{
    // esta lleno cuando top+1 = capacidad
    if((st->top + 1) == st->capacity){
      result = 1;
    }
  }
  return result;
}

int isEmpty(Stack* st){
  int result = 0;
  if(st == NULL){
    fprintf (stderr, "NULL pointer passed to isEmpty().\n");
    result = 1;
  }
  else{
    //esta vacío cuando top=-1
    if(st->top == -1){
      result = 1;
    }
  }
  return result;
}

int sizeOfStack(Stack* st){
  int result = 0;
  if(st == NULL){
    fprintf (stderr, "NULL pointer passed to sizeOfStack().\n");
  }
  else{
    //el numero de elementos en el stack es igual a top+1
    result = st->top + 1;
  }
  return result;
}

Stack* getNewStack (unsigned int size){
  Stack* newStack = NULL;
  //Procedemos solo si size > 0
  if (size > 0){
    /*presten atención aquí cómo se reserva memoria para el struct + para el array*/
    newStack = (Stack*) malloc(sizeof(Stack) + size*sizeof(int));
    //Control si malloc no funcionó
    if(newStack == NULL){
      fprintf (stderr, "Error creating a new Stack with malloc.\n");
    }
    else{
      newStack->top = -1;
      newStack->capacity = size;
    }
  }
    
  return newStack;
}

int stackReset(Stack* st){
  int status = 0;
  if(st == NULL){
    fprintf (stderr, "NULL pointer passed to stackReset().\n");
  }
  else{
    //reseteamos el stack simplemente haciendo top=-1
    st->top = -1;
    status = 1;
  }
  
  return status;
}

int stackDelete(Stack** st){
  int status = 0;
  if(*st == NULL){
    fprintf (stderr, "NULL pointer passed to stackDelete().\n");
  }
  else{
    //liberamos la memoria reservada y dejamos el puntero a NULL
    /*es por eso que necesitamos aquí usar doble puntero*/
    free(*st);
    *st = NULL;
    status = 1;
  }
  return status;
}

int peak(Stack* st){
  int result;
  /*Si el stack está vacio, retornamos a propósito
   * el mínimo número negativo que se puede representar
   * con una variable int*/
  if(isEmpty(st)){
    result = INT_MIN;
  }
  else {
    result = st->array[st->top];
  }
  return result;
}

int push(Stack* st, int item){
  int status = 0;
  if(isFull(st)){
    fprintf (stderr, "Could not push item %d. Stack is Full.\n", item);
  }
  else{
    st->top++;
    st->array[st->top] = item;
    status = 1;
  }
  return status;
}

int pop(Stack* st){
  int result;
  if(isEmpty(st)){
    result = INT_MIN;
  }
  else{
    result = st->array[st->top];
    st->top--;
  }
  return result;
}

int stackPrint(Stack* st){
  Stack* st_temp = NULL; //un stack temporal
  int result = 0;
  //Procedemos solo si st no esta vacia; isEmpty() también controla si st = NULL
  if(!isEmpty(st)){
    st_temp = getNewStack(st->capacity);
    /*Imprimir mientras vamos sacando y guardando a st_temp*/
    while(!isEmpty(st)){
      int st_top = st->top;
      int tmp = pop(st);
      push(st_temp, tmp);
      printf("Número: %d en posición %d\n", tmp, st_top);
    }
    printf("\n");
    /*volvemos a dejar todo en el orden en el que estaba*/
    while(!isEmpty(st_temp)){
      int tmp = pop(st_temp);
      push(st, tmp);
    }
    result = 1;
  }
  
  return result;
}

int stackPrintRecursive(Stack* st){
  int result = 0;
  //Procedemos solo si st no esta vacia; isEmpty() también controla si st = NULL
  if(!isEmpty(st)){
    int st_top = st->top;
    int tmp = pop(st);
    printf("Número: %d en posición %d\n", tmp, st_top);
    stackPrintRecursive(st);
    push(st, tmp);
    result = 1;
  }
  return result;
}

Stack* stackCopy(Stack* st){
  Stack* st_copy = NULL; //el Stack a retornar
  Stack* st_temp = NULL; //otro stack temporal
  if(st == NULL){
    fprintf (stderr, "NULL pointer passed to stackCopy().\n");
  }
  else{
    st_copy = getNewStack(st->capacity);
    st_temp = getNewStack(st->capacity);
    /*Mover datos de st a st_temp*/
    while(!isEmpty(st)){
      int tmp = pop(st);
      push(st_temp, tmp);
    }
    /*Dejar st tal cual estaba antes y al mismo tiempo llenar su copia*/
    while(!isEmpty(st_temp)){
      int tmp = pop(st_temp);
      push(st, tmp);
      push(st_copy, tmp);
    }
    //Antes de retornar, eliminamos el stack temporal
    stackDelete(&st_temp);
  }
  return st_copy;
}

int stackInvert(Stack* st){
  //rellenar
  /* HOMEWORK: Traten de implementarla también de forma recursiva!
   * SIN CREAR COPIA del stack */
}

int pushSumOf2Pops (Stack* st){
  //Ejemplo a seguir
  int result = 0;
  if(st == NULL){
    fprintf (stderr, "NULL pointer passed to pushSumOf2Pops().\n");
  }
  else{
    /*Procedemos si el stack tiene al menos 2 elementos*/
    if((!isEmpty(st)) && (sizeOfStack(st) >= 2)){
      /*Fijense en la siguiente linea de código*/
      int temp = pop(st) + pop(st);
      push(st, temp);
      result = 1;
    }
    else{
      fprintf (stderr, "No hay suficientes elementos para completar pushSumOf2Pops().\n");
    }
  }
  
  return result;
}

int pushSubOf2Pops (Stack* st){
  // Función similar a la anterior
  int result = 0;
  if(st == NULL){
    fprintf (stderr, "NULL pointer passed to pushSubOf2Pops().\n");
  }
  else{
    /*Procedemos si el stack tiene al menos 2 elementos*/
    if((!isEmpty(st)) && (sizeOfStack(st) >= 2)){
      /*Fijense en la siguiente linea de código*/
      int temp = pop(st) - pop(st);
      push(st, temp);
      result = 1;
    }
    else{
      fprintf (stderr, "No hay suficientes elementos para completar pushSubOf2Pops().\n");
    }
  }
  
  return result;
}

int pushMulOf2Pops (Stack* st){
  // Función similar a la anterior
  int result = 0;
  if(st == NULL){
    fprintf (stderr, "NULL pointer passed to pushMulOf2Pops().\n");
  }
  else{
    /*Procedemos si el stack tiene al menos 2 elementos*/
    if((!isEmpty(st)) && (sizeOfStack(st) >= 2)){
      /*Fijense en la siguiente linea de código*/
      int temp = pop(st) * pop(st);
      push(st, temp);
      result = 1;
    }
    else{
      fprintf (stderr, "No hay suficientes elementos para completar pushMulOf2Pops().\n");
    }
  }
  
  return result;
}

int pushDivOf2Pops (Stack* st){
  // Función similar a la anterior
  int result = 0;
  if(st == NULL){
    fprintf (stderr, "NULL pointer passed to pushDivOf2Pops().\n");
  }
  else{
    /*Procedemos si el stack tiene al menos 2 elementos*/
    if((!isEmpty(st)) && (sizeOfStack(st) >= 2)){
      /*Fijense en la siguiente linea de código*/
      int temp = pop(st) / pop(st);
      push(st, temp);
      result = 1;
    }
    else{
      fprintf (stderr, "No hay suficientes elementos para completar pushDivOf2Pops().\n");
    }
  }
  
  return result;
}