/*
 * @file    : stack_main.c
 * @author	: Ioannis Vourkas
 * @date	: 28/05/2020
 * @brief   : Soluciones de código para Taller 3 - ELO 320
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "stack.h"

#define stackSIZE 10

/*genera un número entero aleatorio en el rango de 1 a max*/
static unsigned int getRandomNumber(unsigned int max);

int main(){
  /*Inicializar random seed una sola vez*/
  srand (time(NULL));
  
  /*Generar 1 stack de capacidad stackSIZE*/
  Stack* stack1 = getNewStack(stackSIZE);
  Stack* stack2 = NULL;
  
  /*Llenar el Stack1 con números aleatorios*/
  /*Agregar numeros nuevos hasta que se llene*/
  while(!isFull(stack1)){
    int temp = getRandomNumber(100);
    push(stack1, temp);
  }
  
  /*Chequear si el stack está lleno
   *utilizando la siguiente sintaxis: (condicion) ? (valor para true) : (valor para false)
   * que se llama "ternary operator" (operador ternario)*/
  printf("El Stack está lleno?: %s\n\n" , isFull(stack1)?"Si":"No");
  
  /*Intentar agregar otro número a propósito --> No se puede completar la operación*/
  push(stack1, 33);
  
  /*Imprimir el valor en la cima del stack SIN sacarlo*/
  printf("En la cima de stack1 está el número %d\n\n", peak(stack1));
  
  /*Copiar Stack 1 a Stack 2*/
  stack2 = stackCopy(stack1);
  
  /*Imprimir los dos stack*/
  stackPrint(stack1);
  stackPrint(stack2);
  
  /*Repetimos usando la funcion recursiva para imprimir*/
  stackPrintRecursive(stack1);
  
  /*Resetear el Stack1, agregar nuevos números y visualizar contenido*/
  stackReset(stack2);
  push(stack2,3);
  push(stack2,5);
  stackPrint(stack2);
  
  /*Calcular la expresión: "[(3 * 7) + 14 - (30 / 2)] - 15" con stack1 */
  /*Prueben con otras expresiones como esta utilizando estas funciones*/
  stackReset(stack1);
  push(stack1, 15);
  push(stack1,  2);
  push(stack1, 30);
  pushDivOf2Pops(stack1); // almacenar (30 / 2)
  stackPrint(stack1);
  
  push(stack1,  3);
  push(stack1,  7);
  pushMulOf2Pops(stack1); // almacenar (3 * 7)
  stackPrint(stack1);
  
  push(stack1, 14);
  pushSumOf2Pops(stack1); // almacenar (21 + 14)
  stackPrint(stack1);
  pushSubOf2Pops(stack1); // almacenar (35 - 15)
  pushSubOf2Pops(stack1); // almacenar (20 - 15)
  stackPrint(stack1);     // el resultado es 5
  
  /*Eliminar ambos stack*/
  stackDelete(&stack1);
  stackDelete(&stack2);
  
  return EXIT_SUCCESS;
}

/**
 * @brief       : genera un número entero aleatorio en el rango de 1 a limit 
 * @param limit : el limite superior del rango de valores a crear aleatoriamente
 * @return      : retorna el número aleatorio en el rango especificado, o 0 si limit = 0
 */
static unsigned int getRandomNumber(unsigned int limit){
  int result = 0;
  if (limit != 0){
    result = rand() % limit + 1;
  }
  return result;
}