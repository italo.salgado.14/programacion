/*
 * @file    : stack.h
 * @author  : Ioannis Vourkas
 * @date    : 28/05/2020
 * @brief   : Template de código para Taller 3 - ELO 320
 */

#ifndef STACK_H
#define STACK_H

// Una estructura para administrar un Stack de variables int
//basado en un array. alternativamente se podrian usar punteros (aunque el
//arreglo es un puntero :S)
typedef struct St {
    int top; //el size
    unsigned int capacity;
    int array[]; // esto se llama "flexible array member" de un struct
}Stack;

/**
 * @brief   : indica si el stack st está lleno
 * @param st: dirección de struct Stack que maneja el Stack
 * @return  : retorna 0 para false y 1 para true
 */
int isFull (Stack* st);

/**
 * @brief   : indica si el stack st está vacio
 * @param st: dirección de struct Stack que maneja el Stack
 * @return  : retorna 0 para false y 1 para true
 */
int isEmpty(Stack* st);

/**
 * @brief   : indica el numero de elementos actualmente en el stack
 * @param st: dirección de struct Stack que maneja el Stack
 * @return  : retorna el numero de elementos actualmente en el stack
 */
int sizeOfStack(Stack* st);

/**
 * @brief       : genera un nuevo stack de capacidad size
 * @param size  : el valor correspondiente a la capacidad del Stack
 * @return      : el puntero al Struct que maneja el Stack, o NULL si algo falló
 */
Stack* getNewStack (unsigned int size);

/**
 * @brief   : resetea el stack st al estado inicial
 * @param st: dirección de struct Stack que maneja el Stack
 * @return  : retorna 0 si el stack indicado no existe (NULL) o 1 en c.o.c.
 */
int stackReset(Stack* st);

/**
 * @brief   : elimina el stack y libera la memoria reservada
 * @param st: dirección de struct Stack que maneja el Stack a eliminar
 * @return  : retorna 0 si el stack indicado no existe (NULL) o 1 en c.o.c.
 */
int stackDelete(Stack** st);

/**
 * @brief   : indica el valor almacenado en la cima del stack sin eliminarlo
 * @param st: dirección de struct Stack que maneja el Stack
 * @return  : retorna el valor en la cima, o INT_MIN si la lista está vacia o si no existe
 */
int peak(Stack* st);

/**
 * @brief       : agrega el elemento item al stack st
 * @param st    : dirección de struct Stack que maneja el Stack
 * @param item  : el número a agregar al stack
 * @return      : retorna 0 si algo falló, o 1 en c.o.c.
 */
int push(Stack* st, int item);

/**
 * @brief   : quita el primero elemento del stack
 * @param st: dirección de struct Stack que maneja el Stack
 * @return  : retorna el valor en la cima, o INT_MIN si el stacj esta vacio
 */
int pop(Stack* st);

/**
 * @brief   : imprime los contenidos del stack completo sin modificarlo
 * @param st: dirección de struct Stack que maneja el Stack a imprimir
 * @return  : retorna 0 si el stack está vacio o no existe (NULL), y 1 en c.o.c.
 */
int stackPrint(Stack* st);

/**
 * @brief   : genera una copia del stack st
 * @param st: dirección de struct Stack que maneja el Stack a copiar
 * @return  : retorna la dirección del nuevo stack (la copia) o NULL si algo falló
 */
Stack* stackCopy(Stack* st);

/**
 * @brief   : invierte los contenidos del stack
 * @param st: dirección de struct Stack que maneja el Stack a invertir
 * @return  : retorna 0 si el stack está vacio o no existe (NULL), y 1 en c.o.c.
 */
int stackInvert(Stack* st);

/**
 * @brief   : realiza "pop() + pop()" y hace push() el resultado
 * @param st: dirección de struct Stack que maneja el Stack
 * @return  : retorna 0 si no se completa la operación, o 1 en c.o.c.
 */
int pushSumOf2Pops (Stack* st);

/**
 * @brief   : realiza "pop() - pop()" y hace push() el resultado
 * @param st: dirección de struct Stack que maneja el Stack
 * @return  : retorna 0 si no se completa la operación, o 1 en c.o.c.
 */
int pushSubOf2Pops (Stack* st);

/**
 * @brief   : realiza "pop() * pop()" y hace push() el resultado
 * @param st: dirección de struct Stack que maneja el Stack
 * @return  : retorna 0 si no se completa la operación, o 1 en c.o.c.
 */
int pushMulOf2Pops (Stack* st);

/**
 * @brief   : realiza "pop() / pop()" y hace push() el resultado
 * @param st: dirección de struct Stack que maneja el Stack
 * @return  : retorna 0 si no se completa la operación, o 1 en c.o.c.
 */
int pushDivOf2Pops (Stack* st);

#endif	// STACK_H

