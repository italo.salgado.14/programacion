/*
 * @file    : stack_main.c
 * @author	: Ioannis Vourkas
 * @date	: 28/05/2020
 * @brief   : Template de código para Taller 3 - ELO 320
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "stack.h"

#define stackSIZE 10

/*genera un número entero aleatorio en el rango de 1 a max*/
static unsigned int getRandomNumber(unsigned int max);

int main(){
    /*Inicializar random seed una sola vez*/
    srand (time(NULL));

    /*Generar 2 stacks de la misma capacidad*/
    Stack* stack1 = getNewStack(stackSIZE);
    Stack* stack2 = getNewStack(stackSIZE);

    /*Llenar el Stack1 con números aleatorios*/
    /*Agregar numeros nuevos hasta que se llene*/
    printf("Agregar numeros nuevos hasta que se llene stack 1\n");
    while(!isFull(stack1)){
        int temp = getRandomNumber(100);
        push(stack1, temp);
    }
    stackPrint(stack1);

    /*Chequear si el stack está lleno
     *utilizando la siguiente sintaxis:
     * (condicion) ? (valor para true) : (valor para false)
     * que se llama "ternary operator" (operador ternario)*/
    printf("El Stack está lleno?: %s\n\n" , isFull(stack1)?"Si":"No");

    /*Intentar agregar otro número a propósito*/
    push(stack1, 33); //esto soltará un mensaje de alerta al final de la consola

    /*Ver que hay en la cima del stack sin sacarlo*/
    printf("En la cima de stack1 está el número %d\n\n",peak(stack1));

    /*Copiar los 5 numeros superiores a otro Stack*/
    printf("Copiar los 5 numeros superiores a otro Stack\n");
    for(int i=0; i<5; i++){
        int temp = pop(stack1);
        printf("Se sacó el numero: %d\n", temp);
        push(stack2, temp);
    }
    printf("\n");

    /*Resetear el Stack1 y agregar nuevos números*/
    stackPrint(stack1);
    stackReset(stack1);
    push(stack1,3);
    push(stack1,5);
    stackPrint(stack1);
    printf("\n");

    /*Realizar 3 + 5 y almacenar resultado en lista1*/
    printf("Realizar 3 + 5 del stack1: \n");
    pushSumOf2Pops(stack1);
    stackPrint(stack1);
    printf("\n");

    /*Vaciar el stack1 y a la vez imprimir sus contenidos*/
    printf("Vaciar el stack1 y a la vez imprimir sus contenidos\n");
    stackPrint(stack1);
    stackPrint(stack2);
    while(!isEmpty(stack1)){
        printf("Se sacó el número %d\n", pop(stack1));
    }

    /*Vaciar el stack2 y a la vez imprimir sus contenidos*/
    while(!isEmpty(stack2)){
        printf("Se sacó el número %d\n", pop(stack2));
    }

    /*probar las funciones que quedan por escribir*/


    /*Eliminar ambos stack*/
    stackDelete(&stack1);
    stackDelete(&stack2);

    return EXIT_SUCCESS;
}

/**
 * @brief       : genera un número entero aleatorio en el rango de 1 a limit 
 * @param limit : el limite superior del rango de valores a crear aleatoriamente
 * @return      : retorna el número aleatorio en el rango especificado, o 0 si limit = 0
 */
static unsigned int getRandomNumber(unsigned int limit){
    int result = 0;
    if (limit != 0){
        result = rand() % limit + 1;
    }
    return result;
}