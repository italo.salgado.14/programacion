/*
 * @file    : stack.c
 * @author	: Ioannis Vourkas
 * @date	: 28/05/2020
 * @brief   : Template de código para Taller 3 - ELO 320
 */

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

#include "stack.h"

//indica si el stack st está lleno
int isFull (Stack* st){
    int result = 0;
    // Control de puntero NULL
    if(st == NULL){
        fprintf (stderr, "NULL pointer passed to isFull().\n");
    }
    else{
        if((st->top + 1) == st->capacity){ //si sobrepasamos la capacidad de el stack
            result = 1; //esta lleno
        }
    }
    return result;
}

//indica si el stack st está vacio
int isEmpty(Stack* st){
    int result = 0;
    if(st == NULL){ //control
        fprintf (stderr, "NULL pointer passed to isEmpty().\n");
        result = 1;
    }
    else{
        //como en la teoria, se hace apuntar a -1 el puntero si no tenemos ningún
        //elemento
        if(st->top == -1){
            result = 1; //avisamos que esta vacio el stack
        }
    }
    return result;
}

//genera un nuevo stack de capacidad size
int sizeOfStack(Stack* st){
    int result = 0;
    if(st == NULL){ //control del puntero
        fprintf (stderr, "NULL pointer passed to sizeOfStack().\n");
    }
    else{
        result = st->top + 1; //retornar el size
    }
    return result;
}

//resetea el stack st al estado inicial
Stack* getNewStack (unsigned int size){
    Stack* newStack = NULL; //instanciamos un puntero para el nuevo stack
    if (size > 0){ //Procedemos solo si size > 0
        newStack = (Stack*) malloc(sizeof(Stack) + size*sizeof(int));

        if(newStack == NULL){ //Control si malloc no funcionó
            fprintf (stderr, "Error creating a new Stack with malloc.\n");
        }
        else{
            newStack->top = -1; //inicializamos el stack sin que tenga nada -> -1
            newStack->capacity = size; //la capacidad será size, para siempre
        }
    }

    return newStack; //retornamos el puntero al stack con la memoria ya reservada
}

//resetea el stack st al estado inicial
int stackReset(Stack* st){
    int status = 0;
    if(st == NULL){ //control
        fprintf (stderr, "NULL pointer passed to stackReset().\n");
    }
    else{ //no necesitamos eliminar los datos del array, simplemente movemos nuestro top
        st->top = -1; //apuntamos a sin datos
        status = 1; //todo ok
    }

    return status;
}

//elimina el stack y libera la memoria reservada
int stackDelete(Stack** st){
    int status = 0;
    if(*st == NULL){ //control
        fprintf (stderr, "NULL pointer passed to stackDelete().\n");
    }
    else{
        free(*st); //liberamos lo que reservamos para el stack
        *st = NULL; //hacemos null el puntero casteado
        status = 1; //todo ok. Puede reutilizar/elimnar el puntero
    }
    return status;
}

//indica el valor almacenado en la cima del stack sin eliminarlo
int peak(Stack* st){
    int result;
    /*Si el stack está vacio, retornamos a propósito
     * el mínimo número negativo que se puede representar
     * con una variable int*/
    if(isEmpty(st)){
        result = INT_MIN; //esto es arbitrario
    }
    else {
        result = st->array[st->top]; //retornamos el valor del array que apunta top
    }
    return result;
}

//agrega el elemento item al stack st
int push(Stack* st, int item){
    int status = 0;
    if(isFull(st)){ //corroborar que no este lleno
        //expulsaremos un mensaje de error (fprintf) que dira que no se logro
        fprintf (stderr, "Could not push item %d. Stack is Full.\n", item);
    }
    else{
        st->top++; //avanzamos nuestro puntero
        st->array[st->top] = item; //guardamos e item en el puntero actualizado
        status = 1; //todo salio ok
    }
    return status;
}

//quita el primero elemento del stack
int pop(Stack* st){
    int result;
    if(isEmpty(st)){ //si esta vacio el stack retornamos
        result = INT_MIN; //un numero gigante negativo predefinido
    }
    else{
        result = st->array[st->top]; //retornamos el valor actual del top
        st->top--; //hacemos retroceder el top
    }
    return result;
}

//imprime los contenidos del stack completo sin modificarlo
int stackPrint(Stack* st){
    //rellenar
    //para no modificarlo debemos recrear el mismo stack mientras vamos
    //printeando los valores
    Stack* auxStack = NULL;
    int auxStackTop = -1;
    int auxValue = 0;

    if(isEmpty(st)){ //esta vacio?
        printf("Se ha intentado imprimir un stack vacio/n");
        return 0;
    }
    else{ //si no está vacio
        //printf("Formato: (Valor,Pos) del stack Printeado:\n");
        auxStack = getNewStack(st->capacity); //creamos el stack con la misma capacidad
        while (!isEmpty(st)){ //mientras no este vacio
            auxStackTop = st->top; //guardamos la posicion actual del top
            auxValue = pop(st); //guardamos el valor asociado a este top
            push(auxStack, auxValue); //pusheamos el valor al nuevo stack
            printf("(%d,%d)", auxValue, auxStackTop);
        }
        printf("\n");

        //nuetro auxiliar queda con el stack ingresado al revés
        //ahora que ya operamos sobre el original ingresamos los valores
        //de vuelta al stack
        while(!isEmpty(auxStack)){ //mientras el stack auxiliar no este vacio
            auxValue = pop(auxStack); //extraigo los datos de mi stack auxiliar
            push(st, auxValue); //y los ingreso a mi stack original
        }

        return 1;
    }


}

//genera una copia del stack st
Stack* stackCopy(Stack* st){
    //rellenar
    Stack* result = NULL; //la copia solicitada
    Stack* aux = NULL; //el stack auxiliar para reintegrar el stack original

    //PLAN: leer el stack original. Generar dos copias al unísono
    //Invertir ambas copias
    result = getNewStack(st->capacity);
    while(!isEmpty(st)){

    }

}

//invierte los contenidos del stack
int stackInvert(Stack* st){
    //rellenar
    /*HOMEWORK: Traten de implementarla también de forma recursiva!
     * SIN CREAR COPIA del stack */
    Stack* aux = NULL;
    aux = getNewStack(st->capacity);




    push(aux, pop(st));

}

//realiza "pop() - pop()" y hace push() el resultado
int pushSumOf2Pops (Stack* st){
    //Ejemplo a seguir
    int result = 0;
    if(st == NULL){
        fprintf (stderr, "NULL pointer passed to pushSumOf2Pops().\n");
    }
    else{
        /*Procedemos si el stack tiene al menos 2 elementos*/
        if((!isEmpty(st)) && (sizeOfStack(st) >= 2)){
            int temp = pop(st) + pop(st); //2 pops con el resultado
            push(st, temp); //push al resultado
            result = 1; //todo ok
        }
        else{ //mensaje de alerta de que es imposible
            fprintf (stderr, "No hay suficientes elementos para completar pushSumOf2Pops().\n");
        }
    }

    return result;
}

int pushSubOf2Pops (Stack* st){
    //rellenar
}

int pushMulOf2Pops (Stack* st){
    //rellenar
}

int pushDivOf2Pops (Stack* st){
    //rellenar
}