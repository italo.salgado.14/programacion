/* 
 * @file    : structStudent.h
 * @author  : Ioannis Vourkas
 * @date    : 29/04/2020
 * @brief   : Ejemplos de uso de struct y strings para Clase 04 - ELO 320
 */
 
#ifndef STRUCT_STUDENT_H
#define STRUCT_STUDENT_H

#define NUMCOURSES 5

/*Definición de una estructura con datos de contacto*/
struct Contact {
    char phone[15];
    char streetName[40];
    char streetNumber[10];
};

struct Courses {
    char* courseName[NUMCOURSES];
    char* grade[NUMCOURSES];
};

/*Definición de un nuevo tipo de datos que se llama "Student" */
typedef struct {
    char name[20];
    char surname[40];
    char rol[12];
    struct Contact contactInfo; //una estructura puede tener otras estructuras
    struct Courses selectedCourses;
} Student;

/**
 * @name         : int enrolStudent (Student *s, char* name, char* surname, char* rol)
 * @brief        : inscribe un nuevo estudiante "Student" al curso
 * @param s      : Dirección de una estructura de tipo "Student"
 * @param name   : string que representa el nombre a asignar
 * @param surname: string que representa el apellido a asignar
 * @param rol    : string que representa el ROL a asignar
 * @return       : retorna -1 si s es NULL, o 0 en c.o.c.
 */
int enrolStudent (Student *s, char* name, char* surname, char* rol);

/**
 * @name         : int updateStudentContactInfo (Student *s, char* phone, char* street, char* num)
 * @brief        : Actualiza la información de contacto de un estudiante "Student"
 * @param s      : Dirección de una estructura de tipo "Student"
 * @param phone  : string que representa el num de telefono a asignar
 * @param street : string que representa la calle de la direccion a asignar
 * @param num    : string que representa el numero de la direccion a asignar
 * @return       : retorna -1 si s es NULL, o 0 en c.o.c.
 */
int updateStudentContactInfo (Student *s, char* phone, char* street, char* num);

/**
 * @name          : int printStudentInfo(Student students[], unsigned int id)
 * @brief         : imprime la informacion relevante al estudiante en la posición id
 * @param students: arreglo de tipo "Student"
 * @param id      : indice en el arreglo students de donde imprimir informacion
 * @return        : al imprimir los datos, retorna siempre 0
 */
int printStudentInfo(Student students[], unsigned int id);

/**
 * @name    : unsigned int getNumOfStudents(void)
 * @brief   : entrega el numero de estudiantes inscritos
 * @param   : no recibe argumentos
 * @return  : retorna el valor actual de la variable "numOfStudents"
 */
unsigned int getNumOfStudents(void);

/**
 * @name            : int printAllStudentInfo(Student students[], unsigned int size)
 * @brief           : imprime la informacion de todos los estudiantes
 * @param students  : arreglo de tipo "Student"
 * @param size      : número de estudiantes a imprimir
 * @return          : al imprimir los datos, retorna siempre 0
 */
int printAllStudentInfo(Student students[], unsigned int size);

/**
 * @name                : int enrolCourses (Student *s, unsigned int numOfCourses)
 * @brief               : pide al usuario nombres de asignatura y actualiza la informacion
 * @param s             :
 * @param numOfCourses  :
 * @return              :
 */
int enrolCourses (Student *s, unsigned int numOfCourses);

#endif	// STRUCT_STUDENT_H