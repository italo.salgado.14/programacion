/*
 * @file    : structStudent.c
 * @author	: Ioannis Vourkas
 * @date	: 29/04/2020
 * @brief   : Ejemplos de uso de struct y strings para Clase 04 - ELO 320
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "structStudent.h"

#define MAX_STR_LEN 256

const char* defaultInfo = "N/A";
static unsigned int numOfStudents = 0;

int enrolStudent (Student *s, char* name, char* surname, char* rol){
  if(s == NULL)
    return -1;
  
  numOfStudents++;
  strcpy(s->name, name);
  strcpy(s->surname, surname);
  strcpy(s->rol, rol);
  
  strcpy(s->contactInfo.phone, defaultInfo);
  strcpy(s->contactInfo.streetName, defaultInfo);
  strcpy(s->contactInfo.streetNumber, defaultInfo);
	
	return 0;
}

int updateStudentContactInfo (Student *s, char* phone, char* street, char* num) {
  if(s == NULL)
    return -1;
  
  strcpy(s->contactInfo.phone, phone);
  strcpy(s->contactInfo.streetName, street);
  strcpy(s->contactInfo.streetNumber, num);
  
  return 0;
}

int printStudentInfo(Student students[], unsigned int id){
  
  printf("Student %s %s with rol: %s \n", students[id].name, students[id].surname, students[id].rol);
  printf("Streer Address: %s %s\n", students[id].contactInfo.streetName, students[id].contactInfo.streetNumber);
  printf("Phone number: %s\n", students[id].contactInfo.phone);
  
  return 0;
}

unsigned int getNumOfStudents() {
  
  return numOfStudents;
}

int printAllStudentInfo(Student students[], unsigned int size){
  /*Rellenar con su código*/
  
  return 0;
}

int enrolCourses (Student *s, unsigned int numOfCourses){
  if(s == NULL)
    return -1;
  
  if(numOfCourses > NUMCOURSES){
    fputs ("Numero de cursos supera el maximo permitido\n", stderr);
    return -1;
  }
  
  char textBuffer [MAX_STR_LEN];
  fputs("Ingresar nueva asignatura para Student %s :", stdout);
  if(scanf("%s",textBuffer) != EOF){
    int largo = strlen(textBuffer);
    s->selectedCourses.courseName[0] = (char*) malloc((largo+1)*sizeof(char));
    if(s->selectedCourses.courseName[0] != NULL){
      for (int i=0; i < largo; i++){
        s->selectedCourses.courseName[0][i] = textBuffer[i];
      }
      s->selectedCourses.courseName[0][largo] = '\0';
    }
  }
  
  return 0;
}