/*
 * @file    : structStudent.c
 * @author	: Ioannis Vourkas
 * @date	: 29/04/2020
 * @brief   : Ejemplos de uso de struct y strings para Clase 04 - ELO 320
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "structStudent.h"

#define MAX_STR_LEN 256

const char* defaultInfo = "N/A"; //NULL particular
static unsigned int numOfStudents = 0; //0 inicializador estandar

int initArrayStudents(Student s[], unsigned int num_stud){
    //parametros del estudiante generados por defecto
    for(int i=0; i < num_stud; i++){
        strcpy(s[i].name, defaultInfo);
        strcpy(s[i].surname, defaultInfo);
        strcpy(s[i].rol, defaultInfo);

        strcpy(s[i].contactInfo.phone, defaultInfo);
        strcpy(s[i].contactInfo.streetName, defaultInfo);
        strcpy(s[i].contactInfo.streetNumber, defaultInfo);


        //parametros de courses generados por defectos. aqui debemos inicializar el
        //struct para reservar la memoria correspondiente al parametro que se desea setear
        for (int j=0; j < NUMCOURSES; j++){
            int largo = (int) strlen(defaultInfo);

            //rellenamos el nombre de curso con un N/A
            s[j].selectedCourses.courseName[j] = (char*) malloc((largo+1)*sizeof(char));
            strcpy(s[i].selectedCourses.courseName[j], defaultInfo);
            //relleno el grado con un N/A
            s[j].selectedCourses.grade[j] = (char*) malloc((largo+1)*sizeof(char));
            strcpy(s[i].selectedCourses.grade[j], defaultInfo);
        }
    }

    return 1;
}

int enrolStudent (Student *s, char* name, char* surname, char* rol){
    if(s == NULL) return -1;

    numOfStudents++;
    strcpy(s->name, name);
    strcpy(s->surname, surname);
    strcpy(s->rol, rol);

    strcpy(s->contactInfo.phone, defaultInfo);
    strcpy(s->contactInfo.streetName, defaultInfo);
    strcpy(s->contactInfo.streetNumber, defaultInfo);

    return 0;
}

int updateStudentContactInfo (Student *s, char* phone, char* street, char* num) {
    if(s == NULL)
        return -1;

    strcpy(s->contactInfo.phone, phone);
    strcpy(s->contactInfo.streetName, street);
    strcpy(s->contactInfo.streetNumber, num);

    return 0;
}

int printStudentInfo(Student students[], unsigned int id){
    printf("Student %s %s with rol: %s \n", students[id].name, students[id].surname, students[id].rol);
    printf("Streer Address: %s %s\n", students[id].contactInfo.streetName, students[id].contactInfo.streetNumber);
    printf("Phone number: %s\n", students[id].contactInfo.phone);

    return 0;
}

unsigned int getNumOfStudents() {

    return numOfStudents;
}

int printAllStudentInfo(Student students[], unsigned int size){
    /*Rellenar con su código*/

    return 0;
}

int enrolCourses (Student *s, unsigned int numOfCourses){
    if(s == NULL)
        return -1;

    if(numOfCourses > NUMCOURSES){
        fputs ("Numero de cursos supera el maximo permitido\n", stderr);
        return -1;
    }

    char textBuffer [MAX_STR_LEN];
    //escribir por consola (stdout) el mensaje ingresado en el primer parametro
    fputs("Ingresar nueva asignatura para Student %s :", stdout);
    //
    if(scanf("%s",textBuffer) != EOF){
        unsigned int largo = strlen(textBuffer);
        //solicitamos memoria para guardar el texto ingresado + el \0
        s->selectedCourses.courseName[0] = (char*) malloc((largo+1)*sizeof(char));
        // ->Student-SelectedCourses->Courses-Coursename
        if(s->selectedCourses.courseName[0] != NULL){
            for (int i=0; i < largo; i++){
                //
                s->selectedCourses.courseName[0][i] = textBuffer[i];
            }
            s->selectedCourses.courseName[0][largo] = '\0';
        }
    }

    return 0;
}