/*
 * @file    : main_structStudent.c
 * @author	: Ioannis Vourkas
 * @date	: 29/04/2020
 * @brief   : Ejemplos de uso de struct y strings para Clase 04 - ELO 320
 */

#define STUDENTS 10

#include <stdio.h>
#include <stdlib.h>
#include "student.h"

int main() {
    /*Un arreglo de estructuras de tipo Student*/
    Student ELO320_students[STUDENTS]; //10 estudiantes

    /* Actualizo la información en el arreglo llamando las funciones que he creado
    Recomiendo buscar estas personas, reconocidas por su contribución científica :) */
    enrolStudent(&ELO320_students[0], "Ioannis", "Vourkas", "12345678-9");
    enrolStudent(&ELO320_students[1], "Georgios", "Nikolaou Papanikolaou", "18830513-0");
    enrolStudent(&ELO320_students[2], "Dimitri", "Nanopoulos", "19480913-K");
    enrolStudent(&ELO320_students[3], "Constantinos", "Daskalakis", "19810429-0");

    /*Averiguar el tamaño de las estructuras y tipos de datos utilizados*/
    printf("La estructura \"Struct Contact\" ocupa %d bytes\n", sizeof(struct Contact));
    printf("porque es la suma del tamaño de las variables internas que incluye.\n\n");

    /* Recuerden que un puntero ocupa siempre la misma cantidad de bytes, independientemente
      del tipo de variable a la que apunta; pues tiene el tamaño que ocupa una dirección
      en su computador (pueden ver 4 bytes o 8 bytes, dependiendo del sistema utilizado) */
    printf("Un puntero a \"Struct Contact\" ocupa %d bytes\n\n", sizeof(struct Contact *));

    printf("El tipo de datos \"Student\" ocupa %d bytes\n", sizeof(Student));
    printf("porque es la suma del tamaño de las variables internas que incluye.\n\n");

    printf("Un puntero al tipo de datos \"Student\" ocupa %d bytes\n\n", sizeof(Student *));

    printf("El arreglo \"ELO320_students\" ocupa %d bytes\n", sizeof(ELO320_students));
    printf("porque es la suma de %d variables de tipo Student.\n\n", STUDENTS);
    /* Fíjense que el arreglo ya ocupa el total de bytes que requiere para
     * almacenar en nuestra memoria la cantidad de variables Student que lleva;
     * Esto no depende de si nosotros hemos actualizado la información dentro
     * del arreglo o no. Si definimos el arreglo, entonces este ya existe en la memoria.
     */

    // Actualizar información en los registros del arreglo, del primer estudiante
    updateStudentContactInfo(&ELO320_students[0], "+56912345678", "Av. Espana", "1680");

    /*Imprimir información de forma selectiva*/
    int totalStudents = getNumOfStudents();
    for (int i = 0; i < totalStudents; i++) {
        printStudentInfo(ELO320_students, i);
    }

    /* Taller 2 2021
     * --> Usar malloc() para reservar memoria 10 estructuras Student dinamicamente.
     * --> Revisen el nuevo struct Courses en el archivo .h y generar
     *     funcion para inicializar batch de estudiantes.
     * --> Actualicen la estructura Student y todas las funciones para incorporarla
     * --> Generar una función que imprime la informacion de todos los estudiantes
     * --> Generen una función para la inscripción de asignaturas
     * --> Generen una función que ingrese notas a las asignaturas
     * --> Generen una función que retorna el promedio de las notas de un estudiante
     */

    /* 1 */
    Student *ELO320_newStudents = (Student *) malloc(10 * sizeof(Student));

    /* 2 */
    initArrayStudents(ELO320_students, 10);

    enrolStudent(&ELO320_newStudents[0], "Ioannis", "Vourkas", "12345678-9");
    updateStudentContactInfo(&ELO320_newStudents[0], "+56912345678", "Av. Espana", "1680");
    printStudentInfo(ELO320_newStudents, 0);
    enrolCourses(&ELO320_newStudents[0], 1);

    getchar(); //waiting
    return 0;

}