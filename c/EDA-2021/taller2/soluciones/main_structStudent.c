/*
 * @file    : main_structStudent.c
 * @author	: Ioannis Vourkas
 * @date	: 23/05/2021
 * @brief   : Soluciones propuestas para Taller 2 2021 - ELO 320
 */

#define STUDENTS 10

#include <stdio.h>
#include <stdlib.h>
#include "structStudent.h"

int main(){
	/*Un arreglo estático de estructuras de tipo Student*/
    Student ELO320_students[STUDENTS];
    
    /*Se inicializa el arreglo con informacion por defecto*/
    initArrayOfStudents(ELO320_students, STUDENTS);
	
    /*Actualizo la información en el arreglo inscribiendo estudiantes*/
	enrolStudent(&ELO320_students[0], "Ioannis", "Vourkas", "12345678-9");
    enrolStudent(&ELO320_students[1], "Georgios", "Nikolaou Papanikolaou", "18830513-0");
    enrolStudent(&ELO320_students[2], "Dimitri", "Nanopoulos", "19480913-K");
    enrolStudent(&ELO320_students[3], "Constantinos", "Daskalakis", "19810429-0");
    
    // Actualizo la información de contacto de los estudiantes
    updateStudentContactInfo(&ELO320_students[0], "+56912345678", "Av. Espana", "1680");
    
    /*Imprimo información de forma selectiva*/
    int totalStudents = getNumOfStudents();
    for(int i=0; i<totalStudents; i++){
      printStudentInfo(ELO320_students, i);
    }
    
    /* Puntos a completar para Taller 2 2021
     *1 --> Usar malloc() para reservar memoria para 10 estructuras Student dinamicamente
     *2 --> Revisen el nuevo struct Courses en el archivo .h
     *3 --> Actualicen la estructura Student y todas las funciones para incorporarla
     *4 --> Generen una función que imprime la informacion de todos los estudiantes
     *5 --> Generen una función para la inscripción de asignaturas
     *6 --> Generen una función que ingrese notas a las asignaturas
     *7 --> Generen una función que retorne el promedio de las notas de un estudiante
     */
	
    //Eliminar la info de los estudiantes en el arreglo
    totalStudents = getNumOfStudents();
    for(int i=0; i<totalStudents; i++){
      eraseStudent(&ELO320_students[i]);
    }
    
    /*1*/
    Student* ELO320_newStudents = (Student *) malloc(STUDENTS * sizeof(Student));
    
    /*Se inicializa el nuevo arreglo con informacion por defecto*/
    initArrayOfStudents(ELO320_newStudents, STUDENTS);

    /*3*/
    enrolStudent(&ELO320_newStudents[0], "Ioannis", "Vourkas", "12345678-9");
    updateStudentContactInfo(&ELO320_newStudents[0], "+56912345678", "Av. Espana", "1680");
    
    enrolStudent(&ELO320_newStudents[1], "John", "Ioannidis", "32165498-7");
    updateStudentContactInfo(&ELO320_newStudents[1], "N/A", "Welch Rd, Stanford", "1265");
    
    eraseStudent(&ELO320_newStudents[0]);
    
    enrolStudent(&ELO320_newStudents[0], "Melina", "Merkouri", "31245769-K");
    updateStudentContactInfo(&ELO320_newStudents[0], "+302233445588", "Panepistimiou", "33-B");
    
    /*4*/
    totalStudents = getNumOfStudents();
    printAllStudentsInfo(ELO320_newStudents, totalStudents);
    
    /*5*/
    //Inscribo 2 nuevas asignaturas al estudiante en posición [0]
    enrolCourses(&ELO320_newStudents[0], 3);
    
    /*6*/
    // Ingreso notas a las asignaturas, asumiendo que "DSP" y "EDA" son inscritas
    insertCourseGrade(&ELO320_newStudents[0], "DSP", "100");
    insertCourseGrade(&ELO320_newStudents[0], "EDA", "95");
    
    /*7*/
    float avg = getGradesAvg(&ELO320_newStudents[0]);
    fprintf(stdout, "El promedio de notas del estudiante %s %s es: %4.2f. \n",ELO320_newStudents[0].name, ELO320_newStudents[0].surname, avg);
    
    //Eliminar la info de los estudiantes en el arreglo
    totalStudents = getNumOfStudents();
    for(int i=0; i<totalStudents; i++){
      eraseStudent(&ELO320_newStudents[i]);
    }
    free(ELO320_newStudents);
    
    getchar();
	return 0;
}