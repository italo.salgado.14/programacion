/* 
 * @file    : structStudent.h
 * @author  : Ioannis Vourkas
 * @date    : 23/05/2021
 * @brief   : Soluciones propuestas para Taller 2 2021 - ELO 320
 */
 
#ifndef STRUCT_STUDENT_H
#define STRUCT_STUDENT_H

#define NUMCOURSES 5

/*Definición de una estructura con datos de contacto*/
struct Contact {
    char phone[15];
    char streetName[40];
    char streetNumber[10];
};

struct Courses {
    char* courseName[NUMCOURSES];
    char* grade[NUMCOURSES];
};

/*Definición de un nuevo tipo de datos que se llama "Student" */
typedef struct {
    char name[20];
    char surname[40];
    char rol[12];
    struct Contact contactInfo; 
    struct Courses selectedCourses;
} Student;

/**
 * @name            : int initArrayOfStudents(Student students[], unsigned int size)
 * @brief           : inicializa el arreglo con información por defecto
 * @param students  : arreglo de tipo "Student"
 * @param size      : número de estudiantes (largo del arreglo students[])
 * @return          : al inicializar los datos retorna siempre 0
 */
int initArrayOfStudents(Student students[], unsigned int size);

/**
 * @name         : int enrolStudent (Student *s, char* name, char* surname, char* rol)
 * @brief        : inscribe un nuevo estudiante "Student" al curso
 * @param s      : Dirección de una estructura de tipo "Student"
 * @param name   : string que representa el nombre a asignar
 * @param surname: string que representa el apellido a asignar
 * @param rol    : string que representa el ROL a asignar
 * @return       : retorna -1 si s es NULL, o 0 en c.o.c.
 */
int enrolStudent (Student *s, char* name, char* surname, char* rol);

/**
 * @name         : int eraseStudent (Student *s);
 * @brief        : resetea los datos de un estudiante "Student"
 * @param s      : Dirección de una estructura de tipo "Student"
 * @return       : retorna -1 si s es NULL, o 0 en c.o.c.
 */
int eraseStudent (Student *s);

/**
 * @name         : int updateStudentContactInfo (Student *s, char* phone, char* street, char* num)
 * @brief        : Actualiza la información de contacto de un estudiante "Student"
 * @param s      : Dirección de una estructura de tipo "Student"
 * @param phone  : string que representa el num de telefono a asignar
 * @param street : string que representa la calle de la direccion a asignar
 * @param num    : string que representa el numero de la direccion a asignar
 * @return       : retorna -1 si s es NULL, o 0 en c.o.c.
 */
int updateStudentContactInfo (Student *s, char* phone, char* street, char* num);

/**
 * @name          : int printStudentInfo(Student students[], unsigned int id)
 * @brief         : imprime la informacion relevante al estudiante en la posición id
 * @param students: arreglo de tipo "Student"
 * @param id      : indice en el arreglo students de donde imprimir informacion
 * @return        : al imprimir los datos, retorna siempre 0
 */
int printStudentInfo(Student students[], unsigned int id);

/**
 * @name    : unsigned int getNumOfStudents(void)
 * @brief   : entrega el numero de estudiantes inscritos
 * @param   : no recibe argumentos
 * @return  : retorna el valor actual de la variable "numOfStudents"
 */
unsigned int getNumOfStudents(void);

/**
 * @name            : int printAllStudentInfo(Student students[], unsigned int size)
 * @brief           : imprime la informacion de todos los estudiantes
 * @param students  : arreglo de tipo "Student"
 * @param size      : número de estudiantes a imprimir (largo del arreglo students[])
 * @return          : al imprimir los datos retorna 0, o -1 si hubo error alguno
 */
int printAllStudentsInfo(Student students[], unsigned int size);

/**
 * @name                : int enrolCourses (Student *s, unsigned int numOfCourses)
 * @brief               : pide al usuario nombres de asignaturas y actualiza la informacion
 * @param s             : Dirección de una estructura de tipo "Student"
 * @param numOfCourses  : Numero de asignaturas a inscribir
 * @return              : retorna -1 si hubo algún error, o 0 en c.o.c.
 */
int enrolCourses (Student *s, unsigned int numOfCourses);

/**
 * @name                : int insertCourseGrade (Student *s, char* course, char* grade)
 * @brief               : ingresa una nota "grade" a la asignatura "course" inscrita
 * @param s             : Dirección de una estructura de tipo "Student"
 * @param course        : Nombre de la asignatura cuya nota se ingresara
 * @param grade         : Nota a ingresar
 * @return              : retorna -1 si hubo algún error, o 0 en c.o.c.
 */
int insertCourseGrade (Student *s, char* course, char* grade);

/**
 * @name                : float getGradesAvg (Student *s)
 * @brief               : calcula el promedio de las asignaturas inscritas que tienen nota valida
 * @param s             : Dirección de una estructura de tipo "Student"
 * @return              : el promedio de las asignaturas, o -1 si hubo algún error
 */
float getGradesAvg (Student *s);

#endif	// STRUCT_STUDENT_H