/*
 * @file    : structStudent.c
 * @author	: Ioannis Vourkas
 * @date	: 23/05/2021
 * @brief   : Soluciones propuestas para Taller 2 2021 - ELO 320
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "structStudent.h"

#define MAX_STR_LEN 256

const char* defaultInfo = "N/A";
static unsigned int numOfStudents = 0;

int initArrayOfStudents(Student students[], unsigned int size) {
  
  numOfStudents = 0;
  
  //el argumento size tiene que tener el tamaño del arreglo
  for(int i=0; i<size; i++){
    strcpy(students[i].name, defaultInfo);
    strcpy(students[i].surname, defaultInfo);
    strcpy(students[i].rol, defaultInfo);
  
    strcpy(students[i].contactInfo.phone, defaultInfo);
    strcpy(students[i].contactInfo.streetName, defaultInfo);
    strcpy(students[i].contactInfo.streetNumber, defaultInfo);
  
    /*Inicializamos la informacion de asignaturas inscritas con info por defecto*/
    for(int c=0; c < NUMCOURSES; c++){
      int largo = strlen(defaultInfo);
    
      students[i].selectedCourses.courseName[c] = (char*) malloc((largo+1)*sizeof(char));
      strcpy(students[i].selectedCourses.courseName[c], defaultInfo);
    
      students[i].selectedCourses.grade[c] = (char*) malloc((largo+1)*sizeof(char));
      strcpy(students[i].selectedCourses.grade[c], defaultInfo);
    }
  }
  
  return 0;
}

int enrolStudent (Student *s, char* name, char* surname, char* rol){
  if(s == NULL)
    return -1;
  
  /* OJO que, nunOfStudents incrementará si hacemos  
   * enrolStudent() en la misma posición del arreglo 
   * más de una vez sin aliminar previamente el contenido
   */
  numOfStudents++;
  
  strcpy(s->name, name);
  strcpy(s->surname, surname);
  strcpy(s->rol, rol);
  
  /*Inicializamos la informacion de asignaturas inscritas con info por defecto*/
  for(int c=0; c < NUMCOURSES; c++){
    int largo = strlen(defaultInfo);
    
    s->selectedCourses.courseName[c] = (char*) malloc((largo+1)*sizeof(char));
    strcpy(s->selectedCourses.courseName[c], defaultInfo);
    
    s->selectedCourses.grade[c] = (char*) malloc((largo+1)*sizeof(char));
    strcpy(s->selectedCourses.grade[c], defaultInfo);
  }
	
  return 0;
}

int eraseStudent (Student *s) {
  if(s == NULL)
    return -1;
  
  //chequeo si efectivamente hay informacion valida de un estudiante
  if (strcmp(s->name, defaultInfo) == 0) {
    return -1;
  }
  
  numOfStudents--;
  
  strcpy(s->name, defaultInfo);
  strcpy(s->surname, defaultInfo);
  strcpy(s->rol, defaultInfo);
  
  strcpy(s->contactInfo.phone, defaultInfo);
  strcpy(s->contactInfo.streetName, defaultInfo);
  strcpy(s->contactInfo.streetNumber, defaultInfo);
  
  /*Dejamos la informacion de asignaturas inscritas con info por defecto*/
  for(int c=0; c < NUMCOURSES; c++){
    free(s->selectedCourses.courseName[c]);
    free(s->selectedCourses.grade[c]);
  }
	
  return 0;
}

int updateStudentContactInfo (Student *s, char* phone, char* street, char* num) {
  if(s == NULL)
    return -1;
  
  strcpy(s->contactInfo.phone, phone);
  strcpy(s->contactInfo.streetName, street);
  strcpy(s->contactInfo.streetNumber, num);
  
  return 0;
}

int printStudentInfo(Student students[], unsigned int id){
  
  printf("Student %s %s with rol: %s \n", students[id].name, students[id].surname, students[id].rol);
  printf("Streer Address: %s %s\n", students[id].contactInfo.streetName, students[id].contactInfo.streetNumber);
  printf("Phone number: %s\n", students[id].contactInfo.phone);
  
  return 0;
}

unsigned int getNumOfStudents() {
  
  return numOfStudents;
}

int printAllStudentsInfo(Student students[], unsigned int size){
  if(size > numOfStudents){
    fputs ("Numero de estudiantes indicado supera el numero de estudiantes inscritos\n", stderr);
    return -1;
  }
  
  printf("Siguen los datos de %u estudiantes, de un total de %u inscritos \n", size, numOfStudents);
  for(int i=0; i<size; i++){
    printStudentInfo(students, i);
  }
  
  return 0;
}

int enrolCourses (Student *s, unsigned int numOfCourses){
  if(s == NULL)
    return -1;
  
  if(numOfCourses > NUMCOURSES){
    fputs ("Numero de cursos supera el maximo permitido\n", stderr);
    return -1;
  }
  
  // arreglo temporal donde almacenamos la información ingresada por el teclado
  char textBuffer [MAX_STR_LEN];
  
  for (int c = 0; c < numOfCourses; c++){
    fputs("Ingresar nueva asignatura para Student: ", stdout);
    /*OJO!!! scanf lee solo una palabra, mientras que fgets lee también el ENTER (\n) al final*/
    //if(scanf("%s", textBuffer) != EOF){
    if(fgets(textBuffer, sizeof(textBuffer), stdin) != NULL){
      int largo = strlen(textBuffer);
      
      //reservamos memoria dinamicamente segun la info ingresada por el teclado
      free(s->selectedCourses.courseName[c]);
      s->selectedCourses.courseName[c] = (char*) malloc((largo+1)*sizeof(char));
      if(s->selectedCourses.courseName[c] != NULL){
        for (int i=0; i < largo; i++){
          s->selectedCourses.courseName[c][i] = textBuffer[i];
        }
        s->selectedCourses.courseName[c][largo-1] = '\0';
      }
    }
    else {
      fprintf (stderr, "No se pudo leer el nombre del courso no. %d \n", c);
      return -1;
    }
  }
  
  return 0;
}

int insertCourseGrade (Student *s, char* course, char* grade){
  if(s == NULL)
    return -1;
  
  int i, result;
  //Buscamos si se encuentra la asignatura con este nombre entre las inscritas
  for(i=0; i < NUMCOURSES; i++){
    if (strcmp(course, s->selectedCourses.courseName[i]) == 0)
      break;
  }
  //Si se encuentra, se actualiza la nota
  if(i < NUMCOURSES){
    int largo = strlen(grade);
    free(s->selectedCourses.grade[i]);
    s->selectedCourses.grade[i] = (char*) malloc((largo+1)*sizeof(char));
    strcpy(s->selectedCourses.grade[i], grade);
    result = 0;
  }
  else {
    fprintf(stderr, "El curso %s no está inscrito por el estudiante. \n", course);
    result = -1;
  }
  
  return result;
}

float getGradesAvg (Student *s) {
  if(s == NULL)
    return -1;
  
  //chequeo si efectivamente hay informacion valida de un estudiante
  if (strcmp(s->name, defaultInfo) == 0) {
    return -1;
  }
  
  int sum = 0;
  int validCourses = 0;
  float avg = -1;
  // para cada asignatura valida, si tiene nota valida, se incluye en el promedio
  for(int i=0; i < NUMCOURSES; i++){
    if (strcmp(defaultInfo, s->selectedCourses.courseName[i]) != 0)
      if(strcmp(defaultInfo, s->selectedCourses.grade[i]) != 0) {
        sum += atoi(s->selectedCourses.grade[i]);
        validCourses++;
      }
  }
  
  if (validCourses != 0)
    avg = ((float) sum)/validCourses;
            
  return avg;
}