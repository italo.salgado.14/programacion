#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>

struct record {
    int data;
    struct record *next;
};

//en esta función debo recibir un punteroA de un punteroB, para poder modificar el
//puntero B de acuerdo a la nueva dirección de memoria que este debe apuntar
void insert_First (struct record **rootRef, int x0){
    struct record *nuevo = (struct record *) malloc (sizeof(struct record));
    if (nuevo == NULL){
        printf("No he podido crear el nuevo elemento \n");
        exit(0);
    }
    nuevo->data = x0;
    nuevo->next =  (*rootRef); // la dirección de la dirección de rootRef, ahora sera nuestra nueva boca
    *rootRef = nuevo;

    //return;
}


int main() {

    printf("Test con Listas\n");

    //instanciar dos punteros tipo record
    struct record *root_1 = NULL;
    struct record *root_2 = NULL;

    //le entregamos a la funcion la dirección de memoria de mis dos punteros creados.
    insert_First(&root_1, 10);
    insert_First(&root_2, 20);

    //printiemos asegurandoq ue llenamos bien la lista
    printf("----------------------\n");
    int resul = root_1->data;
    printf("%i /n", resul);

    //int resul2 = root_1->next->data;
    //printf("%i /n", resul2);

    return 1;
}
