#include <stdio.h>

void swap_addresses(int **x, int **y){
    int* temp; //guardar un tipo puntero
    temp = *x; //guardar direccion de x
    *x = *y; //igualar la direccion de x con la de y
    *y = temp; //guardar la direccion que guarda temp en y, listo
}

int main() {

    printf("--------------Antes---------------\n");

    int x=3, y=5;
    int *px = &x;
    int *py = &y;
    printf("px = %u, y = %u \n", px, py);
    printf("-------------Despues---------------\n");

    int **ppx = &px;
    int **ppy = &py;
    swap_addresses(ppx, ppy);
    printf("px = %u, y = %u \n", px, py);
    printf("-------------------------------\n");

    return 0;
}
