#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include "mySortAlg.c"

/*
 * Italo Saqlgado
 * V1.0
 * 24/jul/21
 *
 * Nota 1: el programa no tiene ennes ni tildes a drede.
 *
 */

/*Auxiliares*/
void reset(struct node* myPointer) {
    if (myPointer) {
        free(myPointer);
        myPointer = NULL;
    }
}



int main() {
    /*--------------------- ETAPA 1-------------------- */
    printf("\n-----------------InicioPrograma--------------------\n");
    //reservar la memoria dinamicamente
    int number_of_numbers = 1000;
    struct node* myArray = malloc(number_of_numbers * sizeof(struct node));

    //generacion del numero aleatorio
    srand (time(NULL));
    int N = 1000000; //desde 0 hasta N se generara el numero
    int random_number = rand() % (N+1);

    //rellenar los datos correspondientes
    int counter = 0;
    for (counter = 0; counter < number_of_numbers; counter++){
        myArray[counter].data = random_number;
        myArray[counter].left = -1;
        myArray[counter].right = -1;
        random_number = rand() % (N+1);
    }

    /*-------------------ETAPA2-------------------*/
    //captura de los subelementos de interés,
    int min1 = 0, max1 = 6, min2 = 0, max2 = 6, out = 0;

    printf("Ingrese los 4 numeros separados por espacios:");
    scanf( "%i %i %i %i", &min1, &max1, &min2, &max2);

    //identificar 2 zonas dentro del arreglo original
    while(out == 0){
        if( (0 <= min1) && min1 < max1 && max1 < min2 && min2 < max2 && (max2 <= 1000)){
            out = 1;
        }
        else{
            printf("No ha ingresado correctamente los parámetros. Reintentelo:");
            scanf( "%i %i %i %i", &min1, &max1, &min2, &max2);
        }
    }

    printf("\n-----------------El heap min---------------------\n");

    key_type* recArray = recortar_arreglo(myArray, min1, max1);
    array_print(recArray, max1 - min1);

    heapsort(recArray, max1 - min1, MINHEAP);
    array_print_withChilds(recArray, max1 - min1);
    array_copy(recArray, myArray, min1, max1);

    printf("\n-----------------El heap max---------------------\n");

    key_type* recArray2 = recortar_arreglo(myArray, min2, max2);
    array_print(recArray2, max2 - min2);

    compute_childs(recArray2, max2 - min2, ARRAY_FORMAT );
    array_print_withChilds(recArray2, max2 - min2);

    heapsort(recArray2, max2 - min2 , MAXHEAP);
    array_print(recArray2, max2 - min2);
    array_copy(recArray2, myArray, min2, max2);

    printf("\n-------------Merge Array----------------\n");

    key_type* united_array = merge_arrays(recArray, recArray2, max1 - min1, max2 - min2);
    array_print(united_array, max1 - min1 + max2 - min2);

    heapsort(united_array, max1 - min1 + max2 - min2, MINHEAP);
    array_print(united_array, max1 - min1 + max2 - min2);

    /*---------------ETAPA3--------------------*/
    printf("\n-------------La Busqueda----------------\n");
    //el calculo del tiempo de implementación
    struct timeval stop, start;

    int cicles_per_number = 100, to_search = 10000; //100 o mas, con "varios" ciclos
    unsigned long total_time = 0, actual_time = 0;

    //buscamos aleatoriamente un numero que ya esté en el arreglo para realizar la busqueda
    random_number = rand() % (max1 - min1 + max2 - min2);
    random_number = united_array[random_number].data;

    while( to_search > 0 ){
        gettimeofday(&start, NULL);

        while( cicles_per_number > 0 ){
            binarySearch(united_array, 0, max1 - min1 + max2 - min2, random_number);
            cicles_per_number--;
        }
        gettimeofday(&stop, NULL);

        //reiniciar los counters
        cicles_per_number = 100;

        actual_time = (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec;
        actual_time = actual_time/(cicles_per_number+1);
        printf("took-time-> Buscar %i demoro: %lu us\n", random_number, actual_time);

        total_time = total_time + (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec;

        //cambio en los contadores y el indice del numero a buscar
        to_search--;
        random_number = rand() % (max1 - min1 + max2 - min2);
    }
    printf("took-time-> La operación tomó: %lu us\n", total_time);

    /*---------------ETAPA4--------------------*/
    printf("\n-------------El arbol----------------\n");

    //key_type* ABB_array = ABB_build(united_array, max1 - min1 + max2 - min2);
    //array_print(ABB_array, max1 - min1 + max2 - min2);

    //borrar la memoria reservada
    reset(myArray);
    reset(recArray2);
    reset(recArray);
    reset(united_array);
    return (0);
}
