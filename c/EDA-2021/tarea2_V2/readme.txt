
 Tarea 2
 Italo Salgado
 201473051-9
 - EJECUCIÓN
 $gcc main.c -o tarea2
 $./tarea2

 - LAS ETAPAS
 * Las 3 primeras etapas estan bien implementadas.
 * La 4 se traduce en una función llamada ABB_build. No se esta ejecutando
 ya que no hubo suficiente tiempo para eliminar un bug de implementación al
 realizar la recursión hacia la derecha. El objetivo es dimidiar el arreglo ordenado
 para así anexar esos indices en el ABB.

 - ERRATAS
 * No estoy seguro si la intención de la tarea es que left/right guarde los índices del hijo isquierdo
  y derecho, o los valores de el hijo isquierdo y el derecho. Como no ocupe estos datos, para mi
  no fue crítico averiguar esto. Se entiende que basta con:
  array[i - 1].left = array[2*i - 1].data; ---> array[i - 1].left = 2*i-1;
  para materializar el cambio, si fuese necesario, en las secciones de computo de los hijos.
 * El tiempo de medición es insignificanete para un solo factor. Con ello, reporto la suma de todo
 el proceso para tener una idea de el cómputo. Esto funcionaria de la misma manera a compararlo con la
 ejecución con el ABB ejecutado (si hubiese funcionado -_-).

 - LAS REFERENCIAS

ETAPA 1
Arreglos dinamicos en C:
https://www.delftstack.com/es/howto/c/c-array-of-structs/
http://www.mathcs.emory.edu/~cheung/Courses/255/Syllabus/2-C-adv-data/dyn-array.html

Los numeros aleatorios en C:
https://www.aprenderaprogramar.com/index.php?option=com_content&view=article&id=917:generar-numeros-o-secuencias-aleatorios-en-c-intervalos-srand-y-rand-time-null-randmax-cu00525f&catid=82&Itemid=210

ETAPA2:
Uso de scanf para obtener mas de 1 parametro
https://stackoverflow.com/questions/1412513/getting-multiple-values-with-scanf

Referencia para Heap:
https://gist.github.com/nachinius/490c1561a5bfe015ff18d8d4ac22cb84
https://d-michail.github.io/assets/teaching/data-structures/033_BinaryHeapImplementation.en.pdf

ETAPA3:
https://www.geeksforgeeks.org/binary-search/

Para el timer:
https://stackoverflow.com/questions/10192903/time-in-milliseconds-in-c

