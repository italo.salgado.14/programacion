cmake_minimum_required(VERSION 3.19)
project(tarea2_V2 C)

set(CMAKE_C_STANDARD 99)

add_executable(tarea2_V2 main.c)