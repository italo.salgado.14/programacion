//
// Created by laila on 25-07-21.
//

#ifndef TAREA2_V2_MYSORTALG_H_H
#define TAREA2_V2_MYSORTALG_H_H

/* Definicion de los Tipos */
typedef struct node key_type;

/* El tipo de dato minheap, que contiene los parametros y la informacion del heap para ambas estructuras
 * cur_size = el tamaño actual de mi arreglo dentro de la
 * max_size = tamaño máximo actual del arreglo
 * key_type = dato a guardar de manera generica, predefinindo en el .h, aqui es donde
 *            guardamos el struct definido
 * */
typedef struct minheap* minheap;
typedef struct maxheap* maxheap;

/*
 *---------------------------------------
 * --------------MINHEAP-----------------
 *---------------------------------------
 */

/**
 * @brief  crea un minheap, de tamaño inicial 32, que solicita memoria automaticamente segun se le insertan datos.
 * @return retorna un minheap
 */
minheap minheap_create();

/**
 * @brief   Libera la memoria de un minheap.
 * @param h El minheap a destruir.
 */
void minheap_destroy(minheap);

/**
 * @brief Aumenta la memoria del minheap de manera dinámica. Esto se realiza internamente por
 * la bilbioteca.
 * @param minheap a aumentar la memoria
 */
static void minheap_double_capacity(minheap);

/**
 * @brief Realiza un swap entre las varlables indicadas por los dos indices.
 * @param h El minheap donde se operará el swap.
 * @param i El primero elemento a realizar swap.
 * @param j El segundo elemento donde realizar swap.
 */
static void minheap_swap(minheap h, int i, int j);

/**
 * @brief Empuja el nodo k del minheap entregado, hasta que alcanza su posición respectiva respetando el formato del
 * heap. Especialmente util cuando se agrega un elemento nuevo.
 * @param h Minheap de entrada
 * @param k Nodo del minheap dado que debe ser empujado superior.
 */
static void minheap_fixup(minheap h, int k);

/**
 * @brief Funcion que fuerza el formato minheap en una seccion del array del minheap entregado. Util al
 * utilizar un nuevo minheap.
 * @param h Minheap de entrada.
 * @param k nodo raiz del subarbol del minheap donde se desea reordenar los nodos.
 */
static void minheap_fixdown(minheap h, int k);

/**
 * @brief Agregar un nodo al minheap de manera dinámica.
 * @param h Minheap donde se desea insertar el nuevo parametro.
 * @param key El nuevo nodo a ser ingresado al minheap.
 */
void minheap_insert(minheap h, key_type key);

/**
 * @brief Encuentra el menor valor en un minheap que tenga el formato característico de un minheap.
 * @return Retorna el nodo que tiene el menor valor.
 */
key_type minheap_findmin(minheap);

/**
 * @brief Elimina el minimo presente en el minheap de manera segura.
 * @param h El minheap donde se desea eliminar el minimo
 */
void minheap_deletemin(minheap);

/**
 * @brief Obtener el tamaño del arreglo actual que contiene el tipo de dato minheap.
 * @param h Minheap donde se desea averiguar el tamaño.
 * @return Retorna el tamaño del arreglo como int.
 */
int minheap_size(minheap h);

/**
 * @brief Retorna False o True en un int, si es que el minheap esta vacio.
 * @param h Un minheap
 * @return Retorna un int que indica en un entero si esta o no vacio el minheap (su arreglo).
 */
int minheap_is_empty(minheap h);

/**
 * @brief Resetea el contador interno del tamañó del minheap a 0.
 * @param h Un minheap
 */
void minheap_clear(minheap h);

/**
 * @brief A partir de un arreglo y su tamaño, crea un arreglo de manera dinámica que respeta
 * el formato de de minheap. Setea los hijos de los objetos del array.
 * @param array Arreglo fuente que es utilizado como fuente.
 * @param n El tamañó del arreglo usado como fuente.
 * @return retorna un minheap con formato correspondiente a los datos del array introducido. Setea los hijos.
 */
minheap minheap_heapify(const key_type* array, int n);

/**
 * @brief Print sencillo de un minheap. Entregara uno a uno sus elementos en una linea.
 * @param h Un minheap a ser printeado.
 */
void minheap_print(minheap h);

/*
 *---------------------------------------
 * --------------HEAPSORT-----------------
 *---------------------------------------
 */

/**
 * @brief Algoritmo de ordenamiento, que trabaja tanto sobre un minheap como un maxheap.
 * Reordena el arreglo entregado y computa sus hijos.
 * @param array El array que debe ser ordenado.
 * @param size El tamaño del arreglo que debe ser ordenado.
 * @param op Tipo de Heap que se desea computar. Use MINHEAP o MAXHEAP en este parámetro según
 * corresponda.
 */
void heapsort(key_type* array, int size, int op);

/*
 *---------------------------------------
 * --------------MAXHEAP-----------------
 *---------------------------------------
 */

/**
 * @brief  crea un maxheap, de tamaño inicial 32, que solicita memoria automaticamente segun crece.
 * @return retorna un maxheap
 */
maxheap maxheap_create();

/**
 * @brief   Libera la memoria de un maxheap.
 * @param h El maxheap a destruir.
 */
void maxheap_destroy(maxheap);

/**
 * @brief Aumenta la memoria del maxheap de manera dinámica. Esto se realiza internamente por
 * la bilbioteca.
 * @param maxheap a aumentar la memoria
 */
static void maxheap_double_capacity(maxheap);

/**
 * @brief Realiza un swap entre las varlables indicadas por los dos indices.
 * @param h El maxheap donde se operará el swap.
 * @param i El primero elemento a realizar swap.
 * @param j El segundo elemento donde realizar swap.
 */
static void maxheap_swap(maxheap h, int i, int j);

/**
 * @brief Empuja el nodo k del maxheap entregado, hasta que alcanza su posición respectiva respetando el formato del
 * heap. Especialmente util cuando se agrega un elemento nuevo.
 * @param h Maxheap de entrada
 * @param k Nodo del maxheap dado que debe ser empujado superior.
 */
static void maxheap_fixup(maxheap h, int k);

/**
 * @brief Funcion que fuerza el formato maxheap en una sección del array del maxheap entregado. Util al
 * utilizar un nuevo maxheap.
 * @param h Maxheap de entrada.
 * @param k nodo raiz del subarbol del maxheap donde se desea reordenar los nodos.
 */
static void maxheap_fixdown(maxheap h, int k);

/**
 * @brief Agregar un nodo al maxheap de manera dinámica.
 * @param h Maxheap donde se desea insertar el nuevo parametro.
 * @param key El nuevo nodo a ser ingresado al maxheap.
 */
void maxheap_insert(maxheap h, key_type key);

/**
 * @brief Encuentra el mayor valor en un maxheap que tenga el formato característico de un maxheap.
 * @return Retorna el nodo que tiene el mayor valor.
 */
key_type maxheap_findmax(maxheap);

/**
 * @brief Elimina el maximo presente en el maxheap de manera segura.
 * @param h El maxheap donde se desea eliminar el maximo
 */
void maxheap_deletemax(maxheap);

/**
 * @brief Obtener el tamaño del arreglo actual que contiene el tipo de dato maxheap.
 * @param h Maxheap donde se desea averiguar el tamaño.
 * @return Retorna el tamaño del arreglo como int.
 */
int maxheap_size(maxheap h);

/**
 * @brief Retorna False o True en un int, si es que el maxheap esta vacio.
 * @param h Un maxheap
 * @return Retorna un int que indica en un entero si esta o no vacio el maxheap (su arreglo).
 */
int maxheap_is_empty(maxheap h);

/**
 * @brief Resetea el contador interno del tamañó del maxheap a 0.
 * @param h Un maxheap
 */
void maxheap_clear(maxheap h);

/**
 * @brief A partir de un arreglo y su tamaño, crea un arreglo de manera dinámica que respeta
 * el formato de de maxheap. Setea los hijos de los objetos del array.
 * @param array Arreglo fuente que es utilizado como fuente.
 * @param n El tamaño del arreglo usado como fuente.
 * @return retorna un heapmap ordenado en formato correspondiente a el array introducidoy con hijos seteados.
 */
maxheap maxheap_heapify(const key_type* array, int n);


/**
 * @brief Printea en orden los elementos del maxheap actuales separados por espacios.
 * @param h maxheap a printear
 */
void maxheap_print(maxheap h);

/*
 *---------------------------------------
 * --------------SEARCH-----------------
 *---------------------------------------
 */

/**
 * @brief Algoritmo de busqueda de un dato en especifico.
 * @param arr Arreglo donde se desea buscar un valor especifico.
 * @param l int desde donde iniciar la busqueda.
 * @param r int hasta donde terminar l busqueda.
 * @param number_to_search Numero a buscar dentro del arreglo dado.
 * @return Retorna la posicion del dato a buscar. Retorna -1 s no lo encuentra.
 */
int binarySearch(key_type* arr, int l, int r, int number_to_search);

/*
 *--------------------------------------------
 * ---------OperacionesSobreArreglos----------
 *--------------------------------------------
 */

/**
 * @brief Printea cualquier array en la consola, separados sus parametros en espacios.
 * @param array El array a printear
 * @param size El tamañó del arreglo a mostrar por pantalla.
 */
void array_print(key_type* array, int size);

/**
 * @brief Printea cualquier array en la consola, separados sus parametros en espacios. Además, acompaña cada
 * parametro impreso con sus respectivos hijos en formato (nodo,hijo1,hijo2).
 * @param recArray El array a ser printeado
 * @param size El tamañó del arreglo a ser printeado.
 */
void array_print_withChilds(key_type* recArray, int size);

/**
 * @brief Recorta el arrelo dado y entrega uno nuevo en el puntero dado.
 * @param myarreglo Arreglo a ser copiado en una sección
 * @param begin Donde comenzar la copia.
 * @param end Donde terminar la copia.
 * @return Retorna puntero key_type* hacia el nuevo arreglo.
 */
key_type* recortar_arreglo(key_type* myarreglo, int begin, int end);

/**
 * @brief Funcion que une dos arreglos en uno. Une la primera seccion con la segunda, en ese orden.
 * @param array1 Primer seccion a unir
 * @param array2 Segunda seccion a unir.
 * @param size1 Tamaño primer arreglo.
 * @param size2 Tamaño segundo arreglo.
 * @return Retorna un puntero key_type* al nuevo objeto unido
 */
key_type* merge_arrays (key_type* array1, key_type* array2, int size1, int size2);

/**
 * @brief Computa los hijos de un arreglo segun el formato heap o un arreglo común.
 * @param array Array al qe deben setearles sus hijos
 * @param array_size El tamaño del array a setearle hijos
 * @param format Formato del array. Si es ARRAY_FORMAT O HEAP_FORMAT debe especificarlo.
 */
void compute_childs(key_type* array, int array_size, int format);

/**
 * @brief Al realizar un swap, la siguiente función mantiene correctos los hijos para el padre y los dos nodos que
 * realizaron el swap.
 * @param array El array en cuestion donde se desea mantener bien configurados los hijos
 * @param cursor_size El tamaño del array en cuestion.
 * @param node1_up El nodo que subirá/sera rotado.
 * @param node2 El segundo nodo a ser rotado
 * @param father La posición del padre
 */
void reformate_childs(key_type* array, int cursor_size, int node1_up, int node2, int father);

/**
 * @brief Funcion para copiar desde la fuente 1 (array1) hacia los intervalos (begin1-end1) de la fuente 2 (array2)
 * @param array1 Array fuente a copiar
 * @param array2 Array donde se desea copiar
 * @param begin1 Seccion de inicio de la copia en el Array2
 * @param end1 Seccion de fin de la copia en el Array2
 */
void array_copy(key_type* array1, key_type* array2, int begin1, int end1);

/*
 *--------------------------------------------
 * ------------------ABB----------------------
 *--------------------------------------------
 */

/**
 * @brief Función que crea un arbol balanceado de un heap seteado. La información debe estar previamente ordenada.
 * @param array El array que contiene la información ordenada.
 * @param ABB_array Arreglo de salida
 * @param begin Donde comenzar a operar
 * @param end Donde terminar de operar
 * @param abb_indice Indice actual a operar dentro de ABB.
 */
void ABB_recurrent(key_type* array, key_type* ABB_array, int begin, int end, int abb_indice);

/**
 * @brief Funcion auxiliar para setear el ABB
 * @param array El array con la información ordenada (ejecute un heapsort sobre él)
 * @param size Tamañó del arreglo de entrada
 * @return retornamos el ABB balanceado como salida, en un key_tupe*.
 */
key_type* ABB_build(key_type* array, int size);


#endif //TAREA2_V2_MYSORTALG_H_H
