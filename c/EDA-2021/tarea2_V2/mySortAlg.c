//
// Created by laila on 25-07-21.
//
/*
 * Estrategia:
 * Funciones separadas para minheap y maxheap respetando los tipos
 * los arreglos parten desde 1 hasta n. El valor aaray[0] no se utilizará con
 * motivos pedagogicos, para poder dividir y multiplicar comodamente y llegar a los hijos y padres.
 */

#include "mySortAlg.h"
#include <assert.h>
#define MINHEAP 1
#define MAXHEAP 0
#define HEAP_FORMAT 1
#define ARRAY_FORMAT 0


struct node {
    int data;
    int left;
    int right;
};
struct minheap {
    key_type* array; //array is the array for the keys, un (int*)
    int max_size; //max_size+1 is the array size
    int cur_size; //cur_size is the position of the last array element which is used
};
struct maxheap {
    key_type* array; //array is the array for the keys, un (int*)
    int max_size; //max_size+1 is the array size
    int cur_size; //cur_size is the position of the last array element which is used
};

/*
 *---------------------------------------
 * --------------MINHEAP-----------------
 *---------------------------------------
 */

minheap minheap_create() {
    minheap h = (minheap) malloc(sizeof(struct minheap));
    if (h == NULL) { //existe suficiente memoria?
        fprintf(stderr, "Not enough memory!\n");
        abort();
    }
    h->max_size = 32;
    h->cur_size = 0; //posicion del ultimo elemento usado, es un cursor
    h->array = (key_type*) malloc( sizeof(key_type)*(h->max_size+1)); //el tamano es (num_entradas + 1)
    if (h->array == NULL) { //existe suficiente memoria?
        fprintf(stderr, "Not enough memory!\n");
        abort();
    }

    return h;
}


void minheap_destroy(minheap h) {
    assert(h); //control
    free(h->array);
    free(h);
}

static void minheap_double_capacity(minheap h){
    // create double the array
    int new_max_size = 2 * h->max_size;
    key_type* new_array = (key_type*) malloc( sizeof(key_type)*(new_max_size+1)); //crear nuevo array
    if (new_array == NULL) { //existe suficiente memoria?
        fprintf(stderr, "Not enough memory!\n");
        abort();
    }
    /* copy old elements to new array */
    int i;
    for(i = 1; i <= h->cur_size; i++) {
        new_array[i] = h->array[i];
    }
    /* free old array and place new in position */
    free(h->array);
    h->array = new_array;
    h->max_size = new_max_size;
}


/*
 * el clasico swap de toda la vida
 */
static void minheap_swap(minheap h, int i, int j) {
    assert (h && i >=1 && i <= h->cur_size && j >= 1 && j <= h->cur_size); //todo ok?
    key_type tmp = h->array[i];
    h->array[i] = h->array[j]; //todo ok, estos son punteros
    h->array[j] = tmp;
}

/*
 * Las dos funciones principales para realizar un minHeapify
 *
 * RECUERDE: if "yo soy k"
 * my parent is k/2,
 * nodo de la isquierda es 2k,
 * nodo de la derecha es 2k+1
 */
static void minheap_fixup(minheap h, int k) {
    assert(h && k >= 1 && k <= h->cur_size);
    while (k>1 && h->array[k].data < h->array[k/2].data) { //mientras yo(k) <= mi padre(k)
        minheap_swap(h, k/2, k); //lo intercambiamos por el padre
        k /= 2; //k = k/2 : ahora lo ejecutamos en el padre(k)
    }
}
static void minheap_fixdown(minheap h, int k) {
    assert(h);
    while (2*k <= h->cur_size) { //mientras mi nodo isq (2k) <= cursorHeap
        int j = 2*k; //guardamos al nodo isquierdo (2k) en j
        if (j < h->cur_size && h->array[j+1].data < h->array[j].data) //si hijoisq < cursorH y hijoder < hijoisq (o sea si el arbol esta mal como minHeap)
            j++; //voy al hijo de la derecha
        if (h->array[k].data <= h->array[j].data) //si yo nodo(k) <= hijoder nodo(2k +1)
            break; //salimos del while
        minheap_swap(h, k, j);
        k = j; //ahora "yo" soy el nodo hijoder (2k+1)
    }
}


void minheap_insert(minheap h, key_type key) {
    assert(h);
    // make sure there is space
    if (h->cur_size == h->max_size) //si no existe capacidad, doblamos el largo del array dinámicamente
        minheap_double_capacity(h);
    // add at the bottom, as a leaf
    h->array[++h->cur_size] = key;
    // fix its position
    minheap_fixup(h, h->cur_size);
}


key_type minheap_findmin(minheap h) {
    if (minheap_is_empty(h)) {
        fprintf(stderr, "Heap is empty!\n");
        abort();
    }
// min is always in first position
    return h->array[1];
}


void minheap_deletemin(minheap h) {
    if (minheap_is_empty(h)) {
        fprintf(stderr, "Heap is empty!\n");
        abort();
    }
    // swap first with last
    minheap_swap(h, 1, h->cur_size);
    // delete last
    h->cur_size--;
    // fixdown first
    minheap_fixdown(h, 1);
}


int minheap_size(minheap h) {
    assert(h);
    return h->cur_size;
}

int minheap_is_empty(minheap h) {
    assert(h);
    return h->cur_size <= 0;
}

void minheap_clear(minheap h) {
    assert(h);
    h->cur_size = 0;
}

minheap minheap_heapify(const key_type* array, int n) {
    assert(array && n > 0);
    minheap h = (minheap) malloc(sizeof(struct minheap)); //reservar memoria para el heap
    if (h == NULL) { //memoria suficiente?
        fprintf(stderr, "Not enough memory!\n");
        abort();
    }
    h->max_size = n; //tamaño del heap creado
    h->cur_size = 0; //el cursor parte en el primer elemento
    h->array = (key_type*) malloc(sizeof(key_type)*(h->max_size+1)); //reservar mem para n elementos
    if (h->array == NULL) {
        fprintf(stderr, "Not enough memory!\n");
        abort();
    }
    h->cur_size = n; //ponemos el cursor en el elemento n (el ultimo)
    int k = 0;
    for(k = 0; k < n; k++) //recorremos desde el 0 hasta el ultimo
        h->array[k+1] = array[k]; //guardar el elemento k+1 en mi array k (una copia total)

    compute_childs(h->array, h->cur_size, HEAP_FORMAT);
    for(k = (h->max_size+1)/2; k > 0; k--) //procesar desde la mitad hacia adelante, o sea los nodos finales
        minheap_fixdown(h, k); //realizar fixdown, que realiza el foramto del minHeap para el nodo dado (tira hacia arriba los menores)
    return h;
}



void minheap_print(minheap h){
    int k =1;
    for (k=1; k < h->cur_size + 1; k++ ){
        printf("%i ",h->array[k].data);
    }
    printf("\n");
}

/*
 *---------------------------------------
 * --------------HEAPSORT-----------------
 *---------------------------------------
 */

void heapsort(key_type* array, int size, int op) {
    if (op == MINHEAP){
        minheap h = minheap_heapify(array, size);
        int i = 0;
        while(!minheap_is_empty(h)) {
            array[i++] = minheap_findmin(h); //suma i++ altiro en esta zona
            minheap_deletemin(h);
        }
        compute_childs(array, size, ARRAY_FORMAT);
    }
    if (op == MAXHEAP){
        maxheap h = maxheap_heapify(array, size);
        int i = 0;
        while(!maxheap_is_empty(h)) {
            array[i++] = maxheap_findmax(h);
            maxheap_deletemax(h);

        }
        compute_childs(array, size, ARRAY_FORMAT);
    }
}


/*
 *---------------------------------------
 * --------------MAXHEAP-----------------
 *---------------------------------------
 */

maxheap maxheap_create() {
    maxheap h = (maxheap) malloc(sizeof(struct maxheap));
    if (h == NULL) { //existe suficiente memoria?
        fprintf(stderr, "Not enough memory!\n");
        abort();
    }
    h->max_size = 32;
    h->cur_size = 0; //posicion del ultimo elemento usado, es un cursor
    h->array = (key_type*) malloc( sizeof(key_type)*(h->max_size+1)); //el tamano es (num_entradas + 1)
    if (h->array == NULL) { //existe suficiente memoria?
        fprintf(stderr, "Not enough memory!\n");
        abort();
    }
    int aux = 0;
    while( aux < h->max_size){
        h->array[aux].data = -1;
        aux++;
    }
    return h;
}

void maxheap_destroy(maxheap h) {
    assert(h); //control
    free(h->array);
    free(h);
}

static void maxheap_double_capacity(maxheap h) {
    // create double the array
    int new_max_size = 2 * h->max_size;
    key_type* new_array = (key_type*) malloc( sizeof(key_type)*(new_max_size+1)); //crear nuevo array
    if (new_array == NULL) { //existe suficiente memoria?
        fprintf(stderr, "Not enough memory!\n");
        abort();
    }

    int i = 0;
    for(i = 0; i <= h->cur_size; i++) {
        new_array[i] = h->array[i];
    }
    for (i = h->max_size; i <= h->max_size * 2; i++ ){ //rellenar con datos muertos
        new_array[i].data = -1;
        new_array[i].right = -1;
        new_array[i].left = -1;
    }

    free(h->array);
    h->array = new_array;
    h->max_size = new_max_size;
}

static void maxheap_swap(maxheap h, int i, int j) {
    assert (h && i >=1 && j >= 1  && i <= h->cur_size  && j <= h->cur_size); //todo ok?
    key_type tmp = h->array[i];
    h->array[i] = h->array[j]; //todo ok, estos son punteros
    h->array[j] = tmp;
}

static void maxheap_fixup(maxheap h, int k) {
    assert(h && k >= 1 && k >= h->cur_size); // <=
    while (k>1 && h->array[k].data > h->array[k/2].data) { //mientras yo(k) > mi padre(k) <

        if (h->array[k].data == -1){ //no debo hacer nada si estoy parado en un -1
            break;
        }

        maxheap_swap(h, k/2, k); //lo intercambiamos por el padre
        k /= 2; //k = k/2 : ahora lo ejecutamos en el padre(k)
    }
}

static void maxheap_fixdown(maxheap h, int k) {
    assert(h);
    while (2*k <= h->cur_size) { //mientras mi nodo isq (2k) >= cursorHeap (*) >=

        int j = 2*k; //guardamos al nodo isquierdo (2k) en j
        int father = k/2;

        //printf("-%i:(%i,%i)", h->array[k].data, h->array[k].left, h->array[k].right);

        //---------------------evitar los -1--------------
        if (j < h->cur_size &&  h->array[j].data == -1 ){ //si mi hijoisq es -1, no tengo nada que hacer
            //printf("%i:(%i,%i) ", h->array[k].data, h->array[j].data, h->array[j+1].data);
            break;
        }
        if (j < h->cur_size && h->array[j].data != -1 && h->array[j+1].data == -1 ){ //64 a la isq y -1 a la der
            if (h->array[k].data <= h->array[j].data){ //si yo nodo(k) <= hijoisq nodo(2k) (*)
                maxheap_swap(h, k, j);
                reformate_childs(h->array, h->cur_size, k, j, father);
                //printf("%i:(%i,%i)- ", h->array[k].data, h->array[k].left, h->array[k].right);
                break; //salimos del while
            }
            break;
        }
        //-----------------------------------------

        //si   hijoder > hijoisq (o sea si el arbol esta mal como minHeap)
        if (j < h->cur_size && h->array[j+1].data > h->array[j].data){ //hijder > hijoisq
            j++; //voy al hijo de la derecha

        }

        if (h->array[k].data >= h->array[j].data)  {
            break; //salimos del while
        }//si yo nodo(k) >= hijoisq nodo(2k) (*) >=

        maxheap_swap(h, k, j);
        //compute_childs(h->array, h->cur_size, HEAP_FORMAT);
        reformate_childs(h->array, h->cur_size, k, j, father);
        //printf("%i:(%i,%i)- ", h->array[k].data, h->array[k].left, h->array[k].right);

        k = j; //ahora "yo" soy el nodo hijoder (2k+1)
    }
}


void maxheap_insert(maxheap h, key_type key) {
    assert(h);
    // make sure there is space
    if (h->cur_size == h->max_size) //si no existe capacidad, doblamos el largo del array dinámicamente
        maxheap_double_capacity(h);
    // add at the bottom, as a leaf
    h->array[++h->cur_size] = key;
    // fix its position
    maxheap_fixup(h, h->cur_size);
}


key_type maxheap_findmax(maxheap h) {
    if (maxheap_is_empty(h)) {
        fprintf(stderr, "Heap is empty!\n");
        abort();
    }
// maxxx is always in first position
    return h->array[1];
}


void maxheap_deletemax(maxheap h) {
    if (maxheap_is_empty(h)) {
        fprintf(stderr, "Heap is empty!\n");
        abort();
    }
    // swap first with last
    maxheap_swap(h, 1, h->cur_size);
    // delete last
    h->cur_size--;
    // fixdown first
    maxheap_fixdown(h, 1);
}


int maxheap_size(maxheap h) {
    assert(h);
    return h->cur_size;
}

int maxheap_is_empty(maxheap h) {
    assert(h);
    return h->cur_size <= 0;
}


void maxheap_clear(maxheap h) {
    assert(h);
    h->cur_size = 0;
}


maxheap maxheap_heapify(const key_type* array, int n) {
    assert(array && n > 0);
    maxheap h = (maxheap) malloc(sizeof(struct maxheap)); //reservar memoria para el heap
    if (h == NULL) { //memoria suficiente?
        fprintf(stderr, "Not enough memory!\n");
        abort();
    }
    h->max_size = n; //tamaño del heap creado
    h->cur_size = 0; //el cursor parte en el primer elemento
    h->array = (key_type*) malloc(sizeof(key_type)*(h->max_size+1)); //reservar mem para n+1 elementos
    if (h->array == NULL) {
        fprintf(stderr, "Not enough memory!\n");
        abort();
    }
    h->cur_size = n; //ponemos el cursor en el elemento n (el ultimo)
    int k = 0;
    for(k = 0; k < n ; k++){ //recorremos desde el 0 hasta n-1
        h->array[k+1] = array[k]; //guardar el elemento k+1 en mi array k (una copia total, dejamos el h->array[0] sin utilizar)
        //printf("<%i,%i> ", h->array[k].data, array[k].data);
    }
    compute_childs(h->array, h->cur_size, HEAP_FORMAT);

    for(k = (h->max_size+1)/2; k > 0; k--){ //procesar desde la mitad hacia atras, o sea los nodos iniciales
        maxheap_fixdown(h, k); //realizar fixdown, que realiza el foramto del minHeap para el nodo dado (tira hacia arriba los menores)
    }

    //en este punto, el algoritmo tiene todos los left y los rights de todas las estructuras actualizados, como se pidio
    return h;
}

void maxheap_heapify_instance(maxheap h) {
    assert(h);
    int k = 0;
    for(k = (h->max_size+1)/2; k > 0; k--){ //procesar desde la mitad hacia atras, o sea los nodos iniciales
        maxheap_fixdown(h, k-1); //realizar fixdown, que realiza el foramto del minHeap para el nodo dado (tira hacia arriba los menores)
    }

    //return;
}

void maxheap_print(maxheap h){
    int k;
    for (k=1; k < h->cur_size + 1; k++ ){
        printf("%i ",h->array[k].data);
    }
    printf("\n");
}



/*
 *---------------------------------------
 * --------------SEARCH-----------------
 *---------------------------------------
 */

// A recursive binary search function. It returns
// location of number_to_search in given array arr[l..r] is present,
// otherwise -1
int binarySearch(key_type* arr, int l, int r, int number_to_search)
{
    if (r >= l) {
        int mid = l + (r - l) / 2;

        // If the element is present at the middle
        // itself
        if (arr[mid].data == number_to_search)
            return mid;

        // If element is smaller than mid, then
        // it can only be present in left subarray
        if (arr[mid].data > number_to_search)
            return binarySearch(arr, l, mid - 1, number_to_search);

        // Else the element can only be present
        // in right subarray
        return binarySearch(arr, mid + 1, r, number_to_search);
    }

    // We reach here when element is not
    // present in array
    return -1;
}

/*
 *--------------------------------------------
 * ---------OperacionesSobreArreglos----------
 *--------------------------------------------
 */

void array_print(key_type* array, int size){
    int i = 0;
    for(i = 0 ; i < size; i++){ //recuerde que el arreglo va de 0 a size-1
        printf("%i ", array[i].data);
    }
    printf("\n");
}

void array_print_withChilds(key_type* recArray, int size){
    int i = 0;
    for(i = 0; i < size; i++){ //recuerde que el arreglo va de 0 a size-1
        printf("%i:(%i,%i) ", recArray[i].data, recArray[i].left, recArray[i].right);
    }
    printf("\n");
}

key_type* recortar_arreglo(key_type* myarreglo, int begin, int end){
    key_type* outArray = malloc(( end-begin + 1) * sizeof(key_type));

    int aux = 0;
    while ( aux + begin <= end ){
        //printf("|%i, %i|", aux, begin + aux);
        outArray[aux] = myarreglo[begin + aux];
        aux++;
    }
    //printf("\n");
    return outArray;
}

key_type* merge_arrays (key_type* array1, key_type* array2, int size1, int size2){
    int totalsize = size1 + size2;
    key_type* outArray = malloc((totalsize) * sizeof(key_type));

    int i = 0;
    for (i = 0; i < size1; i++){
        outArray[i] = array1[i];
        //printf("%i", i);
    }
    for (i = size1; i < size1 + size2; ++i) {
        outArray[i] = array2[ i -size1];
        //printf("%i", i);
    }
    //printf("\n");
    return outArray;
}


void compute_childs(key_type* array, int array_size, int format){
    int i = 0;
    if (format == ARRAY_FORMAT){
        for (i = 1; i < array_size ; i++){

            array[i - 1].right = array[2*i + 1 -1].data;
            if (2*i + 1 -1 > array_size - 1){
                array[i - 1].right = -1;
            }

            array[i - 1].left = array[2*i - 1].data;
            if ( 2*i - 1 > array_size -1){
                array[i - 1].left = -1;
            }
        }
    }
    if (format == HEAP_FORMAT){
        for (i = 1; i <= array_size; i++){

            array[i].right = array[2*i + 1].data;
            if (2*i + 1 > array_size){
                array[i].right = -1;
            }

            array[i].left = array[2*i].data;
            if ( 2*i  > array_size){
                array[i].left = -1;
            }
        }
    }
}

void reformate_childs(key_type* array, int cursor_size, int node1_up, int node2, int father){

    if (2 * node1_up <= cursor_size) array[node1_up].left =  array[2 * node1_up].data;
    if (2 * node1_up > cursor_size) array[node1_up].left = -1;

    if (2 * node1_up + 1 <= cursor_size) array[node1_up].right = array[2 * node1_up + 1].data;
    if (2 * node1_up + 1 > cursor_size) array[node1_up].right = -1;

    if (2 * node2 <= cursor_size ) array[node2].left = array[2 * node2].data;
    if (2 * node2 > cursor_size ) array[node2].left = -1;

    if (2 * node2 + 1 <= cursor_size ) array[node2].right = array[2 * node2 + 1].data;
    if (2 * node2 > cursor_size ) array[node2].left = -1;

    if (2 * father <= cursor_size ) array[father].left = array[2 * father].data;
    if (2 * father > cursor_size ) array[father].left = -1;

    if (2 * father + 1 <= cursor_size ) array[father].right = array[2 * father + 1].data;
    if (2 * father + 1 > cursor_size ) array[father].right = -1;

}

void array_copy(key_type* array1, key_type* array2, int begin1, int end1){
    int i = 0;
    for(i = 0 ; i < end1 - begin1; i++){ //recuerde que el arreglo va de 0 a size-1
        array2[begin1 + i] = array1[i];
    }
}


/*
 *--------------------------------------------
 * ------------------ABB----------------------
 *--------------------------------------------
 */

//begin y end deben ser ingresados como en un array, o sea, 0 hasta size-1
void ABB_recurrent(key_type* array, key_type* ABB_array, int begin, int end, int abb_indice){
    printf("%i-%i ", begin, end);

    if ( (begin == -1) || (end == -1) || end < begin){
        return;
    }
    else if (end-begin == 1){
        ABB_array[2*abb_indice - 1] = array[end];
        ABB_array[2*abb_indice - 1 + 1] = array[end - 1];
    }
    else if ( (end - begin +1) % 2 == 0){ //si es par
        printf("P(%i) \n", array[(end - begin)/2 + 1 + begin].data);
        ABB_array[abb_indice - 1] = array[(end - begin)/2 + 1 + begin];
        ABB_recurrent(array, ABB_array, begin, end/2, 2*abb_indice); //el hijo isq
        ABB_recurrent(array, ABB_array, end/2 + 1 , end, 2*abb_indice + 1); //el hijo der
    }
    else { //si es impar
        printf("I(%i) \n", array[(end - begin + 1)/2].data);
        ABB_array[abb_indice - 1] = array[(end - begin)/2 + begin];
        ABB_recurrent(array, ABB_array, begin, end/2 - 1, 2*abb_indice); //el hijo isq
        ABB_recurrent(array, ABB_array, end/2 + 1, end, 2*abb_indice + 1); //el hijo der
    }

}

key_type* ABB_build(key_type* array, int size){
    key_type* ABB_array  = (key_type*) malloc(sizeof(key_type)*size); //reservar mem para n+1 elementos
    ABB_recurrent(array, ABB_array, 0, size-1, 1);
    return ABB_array;
}
