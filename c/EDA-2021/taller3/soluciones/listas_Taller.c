/*
 * @file    : listas_Taller2.c
 * @author	: Ioannis Vourkas
 * @date	: 14/05/2020
 * @brief   : Soluciones de código para Taller 2 - ELO 320
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "listas_Taller2.h"

/**
 * @brief       : genera un número entero aleatorio en el rango de 1 a limit 
 * @param limit : el limite superior del rango de valores a crear aleatoriamente
 * @return      : retorna el número aleatorio en el rango especificado, o 0 si limit = 0
 */
static unsigned int getRandomNumber(unsigned int limit){
  int result = 0;
  if (limit != 0){
    result = rand() % limit + 1;
  }
  return result;
}

unsigned int listLength(Object** headRef){
  int numOfElements = 0;
  /*Cuento solo si la lista no está vacia*/
  if (*headRef != NULL){
    Object* cursor = *headRef;
    while(cursor != NULL){
      numOfElements++;
      cursor = cursor->next;
    }
  }
  return numOfElements;
}

Object* createObject (int num, Object* nextObject) {
  Object* new_object = (Object*) malloc(sizeof (Object));
  if (new_object == NULL) {
    fprintf (stderr, "Error creating a new object with malloc.\n");
    return new_object; 
  }
  /*Actualizo el dato y el puntero next del nuevo elemento*/
  new_object->number = num;
  new_object->next = nextObject;
 
  return new_object;
}

Object* insertFirst (Object** headRef, int num) {
  /*El puntero next del nuevo objeto apuntará a *headRef*/
  Object* new_object = createObject(num, *headRef);
  if (new_object == NULL) {
    return new_object;
  }
  /* Actualizo el valor del puntero head de la lista
   * para apuntar al objeto recientemente ingresado
   */
  *headRef = new_object;
  return new_object;
}

Object* insertLast (Object** headRef, int num) {
  /* Si la lista aún esta vacia, agregar al final de la lista 
   * es igual que agregar al principio de la lista*/
  if(*headRef == NULL){
    Object* new_object = insertFirst(headRef, num);
    return new_object;
  }
  else{
    /*Creo el nuevo objeto y fijo su puntero next a NULL*/
    Object* new_object = createObject(num, NULL);
    if (new_object == NULL) {
      return new_object;
    }
    /*Uso un puntero cursor para navegar hasta el final de la lista*/
    Object* cursor = *headRef;
    while(cursor->next != NULL){
      cursor = cursor->next;
    }
    /* Al llegar al último objeto, actualizamos su puntero next
     * para hacerlo apuntar al objeto recientemente creado
     */
    cursor->next = new_object;
  
    return new_object;
  } 
}

void deleteFirst (Object** headRef){
  /*Si *headRef es NULL, significa que la lista ya esta vacia*/
  if(*headRef != NULL){
    Object* temp = *headRef;
    *headRef = (*headRef)->next;
    /*Eliminamos el elemento que antes era el primero de la lista*/
    free(temp);
  }
  return;
}

void deleteLast (Object** headRef){
  /*Si *headRef es NULL, significa que la lista ya esta vacia*/
  if(*headRef != NULL){
    /* Uso un puntero cursor para navegar hasta el final 
     * de la lista y un puntero previous que indica el 
     * objeto anterior de donde esta el cursor
     */
    Object* cursor = *headRef;
    Object* previous = NULL;
    
    while(cursor->next != NULL){
      previous = cursor;
      cursor = cursor->next;
    }
    /* Existe la posibilidad de que mi lista tenga solo 1 objeto
     * que es cuando necesito actualizar el *headRef también
     */
    if(cursor == *headRef){
      *headRef = (*headRef)->next;
      free(cursor);
    }
    else{
      /* En cualquier otro caso, procedo así, haciendo que el puntero
       * next del penúltimo elemento, ahora a punte a NULL*/
      previous->next = NULL;
      free(cursor);
    }
  }
  return;
}

Object* removeFirst (Object** headRef){
  Object* temp = *headRef;
  /*Si *headRef es NULL, significa que la lista ya esta vacia*/
  if(*headRef != NULL){
    *headRef = (*headRef)->next;
  }
  
  return temp;
}

Object* removeLast (Object** headRef){
  Object* cursor = *headRef;
  /*Si *headRef es NULL, significa que la lista ya esta vacia*/
  if(*headRef != NULL){
    /* Uso un puntero cursor para navegar hasta el final 
     * de la lista y un puntero previous que indica el 
     * objeto anterior de donde esta el cursor
     */
    Object* previous = NULL;
    while(cursor->next != NULL){
      previous = cursor;
      cursor = cursor->next;
    }
    /* Existe la posibilidad de que mi lista tenga solo 1 objeto
     * que es cuando necesito actualizar el *headRef también
     */
    if(cursor == *headRef){
      *headRef = (*headRef)->next;
    }
    else{
      /*En cualquier otro caso, procedo así*/
      previous->next = NULL;
    }
  }
  return cursor;
}

Object* initList (Object** headRef, unsigned int size, unsigned int limit){
  Object* temp = *headRef;
  /*La siguiente linea solo inicializa el seed del generador de números pseudo-aleatorios*/
  srand (time(NULL));
  int i;
  for(i=0; i<size; i++){
    int x = getRandomNumber(limit);
    Object* temp = insertFirst(headRef, x);
    /*Si insertFirst falló, termino el loop aquí con break*/
    if(temp == NULL){
      break;
    }
  }
  /*retorno *headRef o NULL si es que algo falló*/
  return temp;
}

int copyList(Object** headRefSrc, Object** headRefDst, unsigned int size){
  int result = -1;
  /*calculo el largo de la lista de donde quiero copiar*/
  int largoListaSrc = listLength(headRefSrc);
  /* para seguir con la copia, primero la lista de donde copiar tiene que tener elementos
   * De ser así, luego el numero de elementos a copiar tiene que ser menor
   * que el largo de la lista, y además, la lista destino tiene que estar vacia
   * para almacenar correctamente los elementos que le ingresamos*/
  if(largoListaSrc > 0){
    if((size <= largoListaSrc) && (*headRefDst == NULL)){
      Object* cursor =  *headRefSrc;
      int i;
      for(i=0;i<size;i++){
        /* Uso insertLast() para que la nueva lista quede igual que la anterior
         * Si usan insertFirst(), la nueva lista será la anterior pero invertida*/
        insertLast(headRefDst, cursor->number);
        cursor = cursor->next;
      }
      result = 0;
    }
  }
  return result;
}

void deleteList(Object** headRef){
  /*Procedo solo si la lista no está ya vacía*/
  if(*headRef != NULL){
    /*Repito mientras la lista aún tiene elementos*/
    while(*headRef != NULL){
      deleteFirst(headRef);
    }
  }
  
  return;
}

void printList(Object** headRef){
  /*Procedo solo si la lista tiene elementos*/
  if(*headRef == NULL){
    puts("La lista está vacía\n");
  }
  else{
    int largoListaSrc = listLength(headRef);
    printf("La lista tiene los siguientes %d objetos:\n", largoListaSrc);
    Object* cursor = *headRef;
    int i;
    for(i=1; i<= largoListaSrc; i++){
      printf("%d", cursor->number);
      cursor = cursor->next;
      if(cursor != NULL){
        printf(", ");
      }
    }
    puts("\n");
  }
  return;
}

Object* sortList(Object** headRef){
  /*HOMEWORK*/
  return *headRef;
}

Object* searchList(Object** headRef, int num){
  Object* cursor = *headRef;
  /*Busco solo si la lista no está vacía*/
  if(*headRef != NULL){
    while(cursor != NULL){
      /*Al encontrar la primera instancia de num, dejo de buscar más con break*/
      if(cursor->number == num){
        break;
      }
      cursor = cursor->next;
    }
  }
  /* En este punto, si num existe en la lista, cursor tendrá la dirección del 
   * objeto que tiene este valor. Si recorimos la lista completa sin encontrarlo,
   * cursor tendrá valor NULL y la función correctamente retornará NULL*/
  return cursor;
}
