/*
 * @file    : listas_Taller2.h
 * @author  : Ioannis Vourkas
 * @date    : 14/05/2020
 * @brief   : Soluciones de código para Taller 2 - ELO 320
 */

#ifndef LISTAS_TALLER2_H
#define LISTAS_TALLER2_H

typedef struct Node{
  int number;
  struct Node* next;
}Object;

/**
 * @brief        : calcula el largo de la lista (número de objetos)
 * @param headRef: la dirección del puntero al primer objeto de la lista
 * @return       : retorna el largo de la lista al que apunta *headRef
 */
unsigned int listLength(Object** headRef);

/**
 * @brief           : crea un nuevo objeto con los datos pasados como argumentos
 * @param num       : número a ingresar a la estructura
 * @param nextObject: el valor a asignar al puntero next del nuevo objeto
 * @return          : retorna un puntero al nuevo objeto creado o NULL si no se pudo crear
 */
Object* createObject (int num, Object* nextObject);

/**
 * @brief         : agrega un nuevo objeto al principio de la lista
 * @param headRef : la dirección del puntero al primer objeto de la lista
 * @param num     : el número a ingresar al nuevo elemento que entra a la lista
 * @return        : retorna el puntero al nuevo objeto, o NULL si no se pudo crear
 */
Object* insertFirst (Object** headRef, int num);

/**
 * @brief         : agrega un nuevo objeto al final de la lista
 * @param headRef : dirección del primer elemento de la lista
 * @param num     : el número a ingresar al nuevo elemento que entra a la lista
 * @return        : retorna el puntero al nuevo objeto, o NULL si no se pudo crear
 */
Object* insertLast (Object** headRef, int num);

/**
 * @brief         : elimina el primer objeto de la lista y de la memoria
 * @param headRef : la dirección del puntero al primer objeto de la lista 
 */
void deleteFirst (Object** headRef);

/**
 * @brief         : elimina el último objeto de la lista y de la memoria
 * @param headRef : la dirección del puntero al primer objeto de la lista
 */
void deleteLast (Object** headRef);

/**
 * @brief         : quita (desvincula) el primer objeto de la lista
 * @param headRef : la dirección del puntero al primer objeto de la lista
 * @return        : retorna el puntero al objeto removido o NULL si la lista está vacía
 */
Object* removeFirst (Object** headRef);

/**
 * @brief         : quita (desvincula) el último objeto de la lista
 * @param headRef : la dirección del puntero al primer objeto de la lista
 * @return        : retorna el puntero al objeto removido o NULL si la lista está vacía
 */
Object* removeLast (Object** headRef);

/**
 * @brief        : agrega a la lista numeros aleatorios en el rango de 1 a limit
 * @param headRef: la dirección del puntero al primer objeto de la lista
 * @param size   : el numero de objetos a ingresar a la lista
 * @param limit  : el limite superior del rango de valores a crear aleatoriamente
 * @return       : retorna siempre *headRef, o NULL si hubo algún problema
 */
Object* initList (Object** headRef, unsigned int size, unsigned int limit);

/**
 * @brief           : copia size elementos de la lista Src a la lista Dst
 * @param headRefSrc: la dirección del puntero al primer objeto de la lista Src
 * @param headRefDst: la dirección del puntero al primer objeto de la lista Dst
 * @param size      : el número de elementos a copiar
 * @return          : retorna -1 si size es mayor que el largo de la lista Src, 
 *                    o si *headRefDst es NULL, y 0 en c.o.c.
 */
int copyList(Object** headRefSrc, Object** headRefDst, unsigned int size);

/**
 * @brief        : elimina los elementos de la lista completa liberando la memoria
 * @param headRef: la dirección del puntero al primer objeto de la lista
 */
void deleteList(Object** headRef);

/**
 * @brief        : imprime por pantalla el largo de la lista y después todos los 
 *                 números de los elementos de la lista separados por coma 
 * @param headRef: la dirección del puntero al primer objeto de la lista
 */
void printList(Object** headRef);

/**
 * @brief        : ordena los elementos de la lista
 * @param headRef: la dirección del puntero al primer objeto de la lista
 * @return       : retorna siempre *headRef
 */
Object* sortList(Object** headRef);

/**
 * @brief        : Busca en la lista para encontrar el primer objeto con el número num
 * @param headRef: la dirección del puntero al primer objeto de la lista
 * @param num    : el número a buscar en los objetos de la lista
 * @return       : retorna la dirección del objeto de la lista con el número num,
 *                 o NULL si no existe el número num en la lista
 */
Object* searchList(Object** headRef, int num);

#endif	// LISTAS_TALLER2_H
