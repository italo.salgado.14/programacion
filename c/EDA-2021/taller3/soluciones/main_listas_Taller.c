/*
 * @file    : main_listas_Taller2.c
 * @author	: Ioannis Vourkas
 * @date	: 14/05/2020
 * @brief   : Soluciones de código para Taller 2 - ELO 320
 */

#include <stdlib.h>
#include <stdio.h>
#include "listas_Taller2.h"

/*Largo de lista a crear*/
#define SIZE 20
/*Intentos para adivinar números en la lista*/
#define INTENTOS 5

int main (){
  
  //Mensaje de bienvenida
  puts("Inicio del programa de Taller 2\n");
  
  /*Dos punteros a listas*/
  Object* list1 = NULL; //head/cabeza de la lista 1
  Object* list2 = NULL; //head/cabeza de la lista 2
  Object* temp = NULL;   
  
  /*Imprimimos la lista 1 (que aún no tiene nada)*/
  printList(&list1);
  
  /*Agregar elementos a la lista1: un 0, SIZE numeros aleatorios, y otro 0*/
  initList(&list1,SIZE,100);
  insertFirst(&list1, 0);
  insertLast(&list1, 0);
  
  /*Imprimimos la lista 1*/
  printList(&list1);
  
  /*Eliminamos los ceros de los 2 extremos de la lista, y la imprimimos*/
  temp = removeFirst(&list1);
  free(temp);
  temp = removeLast(&list1);
  free(temp);
  printList(&list1);
  
  /*Copiamos algunos elementos de la lista 1 a la lista 2 y la imprimimos*/
  int elementsToCopy = 5;
  copyList(&list1, &list2, elementsToCopy);
  printList(&list2);
  
  /*Eliminamos los elementos restantes en la lista 1*/
  deleteList(&list1);
  
  /*Buscamos en la lista 2 un número durante */
  int num = 0;
  int intentos = 0;
  temp = NULL;
  do{
    puts("Ingrese un número a buscar entre 1 y 100:");
    scanf("%d", &num);
    temp = searchList(&list2, num);
    if(temp != NULL){
      printf("Encontró el No. %d. Tuvo suerte...\n", num);
    }
    intentos++;
    /*repetir si num no existe en la lista y si aún quedan intentos*/
  }while((temp == NULL) && (intentos < INTENTOS));
  
  /*Eliminamos la lista 2*/
  deleteList(&list2);
  
  //Mensaje de salida
  puts("Termino del programa de Taller 2");
  
  return EXIT_SUCCESS;
}