cmake_minimum_required(VERSION 3.19)
project(taller3 C)

set(CMAKE_C_STANDARD 99)

add_executable(taller3 main.c listas_Taller.c listas_Taller.h)