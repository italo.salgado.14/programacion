/*
 * @file    : listas_Taller2.c
 * @author	: Ioannis Vourkas
 * @date	: 14/05/2020
 * @brief   : Template de código para Taller 2 - ELO 320
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "listas_Taller.h"

/**
 * @brief       : genera un número entero aleatorio en el rango de 1 a limit
 * @param limit : el limite superior del rango de valores a crear aleatoriamente
 * @return      : retorna el número aleatorio, o 0 si limit = 0
 */
static unsigned int getRandomNumber(unsigned int limit){
    return (rand() % limit + 1);
}


//calcula el largo de la lista (número de objetos)
unsigned int listLength(Object** headRef){
    int numOfElements = 0;
    if (*headRef != NULL){
        /*LLENAR con su código*/
        Object* HEADaux = *headRef; //instanciar puntero en HEAD de la lista (para no mover el HEAD)

        //recorremos la lista hasta encontrar el ultimo elemento
        while (HEADaux != NULL){
            numOfElements++;
            HEADaux = HEADaux->next;
        }
    }
    return numOfElements;
}

//crea un nuevo objeto con los datos pasados como argumentos
Object* createObject (int num, Object* nextObject) {
    Object* new_object = (Object*) malloc(sizeof (Object)); //creo el obj en el puntero nextobject

    //verificamos que lo que creamos este ok
    if (new_object == NULL) {
        fprintf (stderr, "Error creating a new object with malloc.\n");
        return new_object;
    }
    //Actualizo el dato y el puntero next del nuevo element
    new_object->number = num;
    new_object->next = nextObject;

    return new_object;
}

//agrega un nuevo objeto al principio de la lista
Object* insertFirst (Object** headRef, int num) {
    Object* new_object = createObject(num, *headRef); //creo un objeto tipo object

    //verifico que este bien creado
    if (new_object == NULL) {
        return new_object;
    }
    //Actualizo el valor del puntero head de la lista
    //para apuntar al objeto recientemente ingresado
    *headRef = new_object; //el puntero que apunta al valor headRef ahora apunta al nuevo obj creado
    return new_object;
}

//agrega un nuevo objeto al final de la lista
Object* insertLast (Object** headRef, int num) {
    // Si la lista aún esta vacia, agregar al final de la lista
    //es igual que agregar al principio de la lista
    if(*headRef == NULL){
        Object* new_object = insertFirst(headRef, num); //crear un
        return new_object;
    }
    else{
        Object* new_object = createObject(num, NULL); //crear un objeto

        //verificar que este correctamente creado
        if (new_object == NULL) {
            return new_object;
        }

        //Uso un puntero cursor para navegar hasta el final de la lista
        Object* cursor = *headRef; //instanciar un puntero, que apunta a el HEAD de la lista
        while(cursor->next != NULL){ //mientras no encuentre el ultimo elemento de la lista
            cursor = cursor->next; //avanzo al siguiente elemento de la lista
        }

        // Al llegar al último objeto, actualizamos su puntero next
        //para hacerlo apuntar al objeto recientemente creado
        cursor->next = new_object;

        return new_object;
    }
}

//elimina el último objeto de la lista
void deleteFirst (Object** headRef){
    // Si *headRef es NULL, significa que la lista ya esta vacia
    if(*headRef != NULL){
        Object* temp = *headRef; //instanciamos un puntero, luego lo hacemos apuntar al HEAD de la lista
        *headRef = (*headRef)->next; //el HEAD de la lista será ahora el puntero del siguiente

        free(temp); //liberar memoria del elemento que antes era el primero
    }
    else{
        //no hacemos nada :)
    }
    return;
}

// elimina el último objeto de la lista
void deleteLast (Object** headRef){
    /*Si *headRef es NULL, significa que la lista ya esta vacia*/
    if(*headRef != NULL){
        /* Uso un puntero cursor para navegar hasta el final
         * de la lista y un puntero previous que indica el
         * objeto anterior de donde esta el cursor
         */
        Object* cursor = *headRef; //instanciar puntero al HEAD
        Object* previous = NULL;

        while(cursor->next != NULL){ //mientras no lleguemos al final de la lista
            previous = cursor; //guardamos el actual cursor
            cursor = cursor->next; //avanzamos un elemento en la lista
        }

        /* CASO ESPECIAL
         * Existe la posibilidad de que mi lista tenga solo 1 objeto
         * que es cuando necesito actualizar el *headRef también
         */
        if(cursor == *headRef){ //si mi cursor termino en el HEAD
            *headRef = (*headRef)->next; //actualizo el HEAD
            free(cursor); //libero el cursor
        }
        else{
            /*En cualquier otro caso, procedo así*/
            previous->next = NULL; //declaro al previo como el ultimo
            free(cursor); //y me desago del último
        }
    }
    return;
}

Object* removeFirst (Object** headRef){
    Object* temp = *headRef;
    /*LLENAR con su código*/

    return temp;
}

Object* removeLast (Object** headRef){
    Object* cursor = *headRef;
    /*LLENAR con su código*/

    return cursor;
}

//llena la lista con numeros aleatorios en el rango de 1 a limit
Object* initList (Object** headRef, unsigned int size, unsigned int limit){
    Object* temp = *headRef;
    /*LLENAR con su código*/

    for (int i=0; i<size; i++){
        int aux = (int) getRandomNumber(limit);//obtenemos un numero aleatorio
        Object* temp = insertFirst(headRef, aux);
    }

    return temp;
}

int copyList(Object** headRefSrc, Object** headRefDst, unsigned int size){
    int result = -1;

    //recuperar el largo de la lista. Esto es mas facil que escribir el codigo para recorrer
    //la lista, pero tecnicamente no es lo mas rapido llamar esto y recorrer la lista de nuevo para
    //operar sobre ella
    int unsigned largoListaSrc = listLength(headRefSrc);

    //primero no aseguramos que el largo a extraer sea menor que el tamaño de la de la lista
    //ademas nos aseguramos que que el HEAD de destino esté vacio (ese requisito lo puso vourkas)
    //ademas debemos asegurarnos que al menos la lista tenga 1 elemento
    if((size <= largoListaSrc) && (*headRefDst == NULL) && (largoListaSrc >0)){
        /*LLENAR con su código*/
        Object* auxHEAD = (*headRefSrc);

        //insertamos interativamente cada uno de los elementos
        for (unsigned int i=0; i<size; i++){ //0,1,2...,size-2,size-1
            //insertLast solo requiere el dato del nuevo nodo a agregar a la lista con el HEAD dado
            //en el segundo parametro
            insertLast(headRefDst, auxHEAD->number);
            auxHEAD = auxHEAD->next;
        }
        result = 0; //si pase por todo el proceso sin detenerme, entonces puedo retornar 0, etoc=-1
    }
    return result;
}

void deleteList(Object** headRef){
    /*LLENAR con su código*/
    //borrado interativo, esto es, borrar hasta que nos econtremos apuntando a NULL en el HEAD de la lista
    //como no nos importa mover el HEAD en este caso no nos molestamos en crear un a copia ni ninguna
    //huea así
    while (*headRef != NULL){ //mientras no apunte a NULL en el HEAD
        deleteFirst(headRef); //elimino el que acabo de recorrer
    }

    return; //chao pescao
}

void printList(Object** headRef){
    /*LLENAR con su código*/
    int acum = 0;

    //primero verificar que no este vacia la lista
    if ( (*headRef) == NULL){
        printf("Lista: empty");
    }
    else{
        Object* auxHEAD = *headRef; //creamos un puntero nuevo, que este apuntando a HEAD
        if (*headRef != NULL){
            while (auxHEAD != NULL){
                printf("%d", auxHEAD->number); //printeo el numero del actual elemento
                auxHEAD = auxHEAD->next; //avanzo al siguiente
                acum++; //mi numero acumulado aumenta en 1
                if (auxHEAD != NULL){ //mientras no sea el ultimo elemento lo separamos con un -
                    printf("-");
                }
            }
        }
    }
    printf("\n"); //sea lo que sea que se imprima, hacemos salto de linea
    return;
}

Object* searchList(Object** headRef, int num){
    /*LLENAR con su código*/
    Object* auxHEAD = (*headRef);
    if ( (*headRef) != NULL){ //pa que buscar si esta vacio
        while (auxHEAD != NULL){ //mientras no llegue al final lo seguire intentado
            if (auxHEAD->number == num){
                return auxHEAD;
            }
            auxHEAD = auxHEAD->next; //avanzar al siguiente elemento
        }
        //si llegamos hasta acá no encontramos lo que buscabamos. auxHEAD será NULL
    }

    //return *headRef; //no cacho porque vourkas queria devolver esto aca
    return auxHEAD;
}

/* PREFUNCIONES PARA APLICAR BUBBLESORT EN LA LISTA */
void swap(struct Node *a, struct Node *b){
    int temp = a->number;
    a->number = b->number;
    b->number = temp;
}

Object* sortList(Object** start){
    /*LLENAR con su código*/
    int swapped, i;
    struct Node *ptr1;
    struct Node *lptr = NULL;

    if ( (*start) == NULL)
        return NULL;
    do
    {
        swapped = 0;
        ptr1 = (*start);

        while (ptr1->next != lptr)
        {
            if (ptr1->number > ptr1->next->number)
            {
                swap(ptr1, ptr1->next);
                swapped = 1;
            }
            ptr1 = ptr1->next;
        }
        lptr = ptr1;
    }
    while (swapped);

    return *start;
}


