/*
 * @file    : main_listas_Taller2.c
 * @author	: Ioannis Vourkas
 * @date	: 14/05/2020
 * @brief   : Template de código para Taller 2 - ELO 320
 */

#include <stdlib.h>
#include <stdio.h>
#include "listas_Taller.h"

/*Largo de lista a crear*/
#define SIZE 10



int main (){

    //Mensaje de bienvenida
    puts("Inicio del programa de Taller 2\n");

    /*Dos punteros a listas*/
    Object* list1 = NULL; //head/cabeza de la lista 1
    Object* list2 = NULL; //head/cabeza de la lista 2

    /*Inicializar la lista1 con SIZE elementos con números aleatorios*/
    printf("-------------------------\n");
    initList(&list1, SIZE, 20);
    printList(&list1);

    /*Copiar la lista1 a la lista2*/
    printf("-------------------------\n");
    int elem_to_copy = 5;
    copyList(&list1, &list2, elem_to_copy);
    printList(&list2);

    /*Eliminar la lista 1 completa*/
    printf("-------------------------\n");
    deleteList(&list1);
    printList(&list1);

    /*Imprimir la lista 2*/
    //ya esta hecho mas arriba

    /*Buscar en la lista un número específico*/
    //defino una nueva funcion artificial de busqueda al inicio del main
    int num_search = 2;
    Object* auxP = NULL;
    auxP = searchList(&list2, num_search);
    if (auxP == NULL){
        printf("No se encontró en la lista el número %d\n", num_search);
    }
    else {
        printf("Se encontró en la lista el número %d\n", num_search);
    }

    /*Ordenar la lista 2*/
    printf("-------------------------\n");
    sortList(&list2);
    printList(&list2);

    /*Eliminar la lista 2 completa*/
    printf("-------------------------\n");
    deleteList(&list2);
    printList(&list2);

    //Mensaje de salida
    printf("-------------------------\n");
    puts("Termino del programa de Taller 1");
    getchar(); //waiting
    return EXIT_SUCCESS;
}