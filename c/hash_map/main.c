/*
 * A unit test and example of how to use the simple C hashmap
 */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "hash_map.c"

#define KEY_MAXLENGTH (64)
#define KEY_PREFIX ("somekey")

enum node_can{ 
    MOTOR_CURRENT, MOTOR_VOLTAGE,
    SCREEN_MOTOR, SCREEN_CONTROL, SCREEN_CONTROL,
    CONTROL_MOVE};
#define KEY_INIT MOTOR_CURRENT    
#define KEY_COUNT (CONTROL_MOVE + 1)

typedef struct _data
{
    size_t len;
    void* data;
} data;

//------------------PROGRAM------------------------//

int main(char* argv, int argc)
{
    int error;
    map_t mydates;  //map_t = void* ,abstraccion valida para cualquier tipo de dato
    char key_string[KEY_MAXLENGTH];
    data* value;

    mydates = hashmap_new();

    /* First, populate the hash map with the enum    */
    int index;
    for (index=KEY_INIT; index<KEY_COUNT; index+=1)
    {
         //creamos el puntero tipo nodes, y llenamos el arreglo con -1 (no se ha tomado el dato)
        value = malloc(sizeof(mydates));
        value->data = NULL;

        error = hashmap_put(mydates, index, value);
        assert(error==MAP_OK);
    }

    /* Now, check all of the expected values are there 
    for (index=KEY_INIT; index<KEY_COUNT; index+=1)
    {
        snprintf(key_string, KEY_MAXLENGTH, "%s: %d", KEY_PREFIX, index);
        error = hashmap_get(mymap, key_string, (void**)(&value));
        
        // Make sure the value was both found and the correct number 
        assert(error==MAP_OK);
        assert(value->number==index);
    }
    */

    /* Make sure that a value that wasn't in the map can't be found 
       Asegurese de que no se pueda encontrar un valor que no estaba en el mapa */
      // snprintf(key_string, KEY_MAXLENGTH, "%s: %d", KEY_PREFIX, KEY_COUNT);
    

    error = hashmap_get(mydates, SCREEN_MOTOR, (void**)(&value));
    printf ("Characters: %c \n", error);    
    /* Make sure the value was not found */
    assert(error==MAP_MISSING);



    /* Free all of the values we allocated and remove them from the map */
    for (index=0; index<KEY_COUNT; index+=1)
    {
        error = hashmap_get(mydates, index, (void**)(&value));
        assert(error==MAP_OK);

        error = hashmap_remove(mydates, key_string);
        assert(error==MAP_OK);

        free(value);        
    }
    
    /* Now, destroy the map */
    hashmap_free(mydates);

    return 1;
}